/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import ar.com.singleton.fiscalizar.libs.jpa.controllers.entities.CategoriesJpaController;
import ar.com.singleton.fiscalizar.libs.jpa.controllers.entities.CircuitsJpaController;
import ar.com.singleton.fiscalizar.libs.jpa.controllers.entities.ElectionsJpaController;
import ar.com.singleton.fiscalizar.libs.jpa.controllers.entities.InstitutionsJpaController;
import ar.com.singleton.fiscalizar.libs.jpa.controllers.entities.PartiesJpaController;
import ar.com.singleton.fiscalizar.libs.jpa.controllers.entities.PollsFilesJpaController;
import ar.com.singleton.fiscalizar.libs.jpa.controllers.entities.PollsJpaController;
import ar.com.singleton.fiscalizar.libs.jpa.controllers.entities.RegionsJpaController;
import ar.com.singleton.fiscalizar.libs.jpa.controllers.entities.SectionsJpaController;
import ar.com.singleton.fiscalizar.libs.jpa.controllers.entities.UsersJpaController;
import ar.com.singleton.fiscalizar.libs.jpa.controllers.entities.VotingTablesJpaController;
import ar.com.singleton.fiscalizar.libs.jpa.controllers.entities.relations.ElectionsCategoriesJpaController;
import ar.com.singleton.fiscalizar.libs.jpa.controllers.entities.relations.ElectionsPartiesJpaController;
import ar.com.singleton.fiscalizar.libs.jpa.controllers.entities.relations.ElectionsRegionsJpaController;
import ar.com.singleton.fiscalizar.libs.jpa.controllers.entities.relations.ElectionsSectionsJpaController;
import ar.com.singleton.fiscalizar.libs.jpa.controllers.entities.relations.UserCircuitsJpaController;
import ar.com.singleton.fiscalizar.libs.jpa.controllers.entities.relations.UserElectionsJpaController;
import ar.com.singleton.fiscalizar.libs.jpa.controllers.entities.relations.UserInstitutionsJpaController;
import ar.com.singleton.fiscalizar.libs.jpa.controllers.entities.relations.UserRegionsJpaController;
import ar.com.singleton.fiscalizar.libs.jpa.controllers.entities.relations.UserSectionsJpaController;
import ar.com.singleton.fiscalizar.libs.jpa.controllers.entities.relations.UserVotingTablesJpaController;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Rule;
import org.junit.rules.TestRule;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;

/**
 *
 * @author pato
 */
public class TestConnection {
    
    private final EntityManagerFactory emf;
    private final CategoriesJpaController categoriesJpaController;
    private final CircuitsJpaController circuitsJpaController;
    private final ElectionsJpaController electionsJpaController;
    private final InstitutionsJpaController institutionsJpaController;
    private final PartiesJpaController partiesJpaController;
    private final PollsJpaController pollsJpaController;
    private final PollsFilesJpaController pollsFilesJpaController;
    private final RegionsJpaController regionsJpaController;
    private final SectionsJpaController sectionsJpaController;
    private final UsersJpaController usersJpaController;
    private final VotingTablesJpaController votingTablesJpaController;
    private final ElectionsCategoriesJpaController electionsCategoriesJpaController;
    private final ElectionsPartiesJpaController electionsPartiesJpaController;
    private final ElectionsRegionsJpaController electionsRegionsJpaController;
    private final ElectionsSectionsJpaController electionsSectionsJpaController;
    private final UserCircuitsJpaController userCircuitsJpaController;
    private final UserElectionsJpaController userElectionsJpaController; 
    private final UserInstitutionsJpaController userInstitutionsJpaController;
    private final UserRegionsJpaController userRegionsJpaController;
    private final UserSectionsJpaController userSectionsJpaController;
    private final UserVotingTablesJpaController userVotingTablesJpaController;
    
    public TestConnection() {
        emf = Persistence.createEntityManagerFactory("ar.com.singleton.fiscalizar");
        
        categoriesJpaController = new CategoriesJpaController(emf);
        circuitsJpaController = new CircuitsJpaController(emf);
        electionsCategoriesJpaController = new ElectionsCategoriesJpaController(emf);
        electionsJpaController = new ElectionsJpaController(emf);
        electionsPartiesJpaController = new ElectionsPartiesJpaController(emf);
        electionsRegionsJpaController = new ElectionsRegionsJpaController(emf);
        electionsSectionsJpaController = new ElectionsSectionsJpaController(emf);
        institutionsJpaController = new InstitutionsJpaController(emf);
        partiesJpaController = new PartiesJpaController(emf);
        pollsFilesJpaController = new PollsFilesJpaController(emf);
        pollsJpaController = new PollsJpaController(emf);
        regionsJpaController = new RegionsJpaController(emf);
        sectionsJpaController = new SectionsJpaController(emf);
        userCircuitsJpaController = new UserCircuitsJpaController(emf);
        userElectionsJpaController = new UserElectionsJpaController(emf);
        userInstitutionsJpaController = new UserInstitutionsJpaController(emf);
        userRegionsJpaController = new UserRegionsJpaController(emf);
        userSectionsJpaController = new UserSectionsJpaController(emf);
        userVotingTablesJpaController = new UserVotingTablesJpaController(emf);
        usersJpaController = new UsersJpaController(emf);
        votingTablesJpaController = new VotingTablesJpaController(emf);
    }
    
    @Rule
    public TestRule watcher = new TestWatcher() {
       @Override
       protected void starting(Description description) {
          System.out.println("Starting test: " + description.getMethodName());
       }

        @Override
        protected void finished(Description description) {
            System.out.println("Finished test: " + description.getMethodName());
        }
       
    };
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void connection() {
        assertNotNull(categoriesJpaController);
        assertNotNull(circuitsJpaController);
        assertNotNull(electionsJpaController);
        assertNotNull(institutionsJpaController);
        assertNotNull(partiesJpaController);
        assertNotNull(pollsJpaController);
        assertNotNull(pollsFilesJpaController);
        assertNotNull(regionsJpaController);
        assertNotNull(sectionsJpaController);
        assertNotNull(usersJpaController);
        assertNotNull(votingTablesJpaController);
        assertNotNull(electionsCategoriesJpaController);
        assertNotNull(electionsPartiesJpaController);
        assertNotNull(electionsRegionsJpaController);
        assertNotNull(electionsSectionsJpaController);
        assertNotNull(userCircuitsJpaController);
        assertNotNull(userElectionsJpaController);
        assertNotNull(userInstitutionsJpaController);
        assertNotNull(userRegionsJpaController);
        assertNotNull(userSectionsJpaController);
        assertNotNull(userVotingTablesJpaController);
        assertNotNull(votingTablesJpaController);
    }
}
