/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ar.com.singleton.fiscalizar.libs.jpa.entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author pato
 */
@Entity
@Table(name = "Polls", catalog = "", schema = "")
@XmlRootElement
public class Polls implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "votes")
    private int votes;
    @Basic(optional = false)
    @Column(name = "createdAt")
    private String createdAt;
    @Column(name = "updatedAt")
    private String updatedAt;
    @Basic(optional = false)
    @Column(name = "deleted")
    private int deleted;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idPoll")
    private List<PollsFiles> pollsFilesList;
    @JoinColumn(name = "idParty", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Parties idParty;
    @JoinColumn(name = "idElection", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Elections idElection;
    @JoinColumn(name = "idUser", referencedColumnName = "id")
    @ManyToOne
    private Users idUser;
    @JoinColumn(name = "idVotingTable", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private VotingTables idVotingTable;
    @JoinColumn(name = "idCategory", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Categories idCategory;

    public Polls() {
    }

    public Polls(Integer id) {
        this.id = id;
    }

    public Polls(Integer id, int votes, String createdAt, int deleted) {
        this.id = id;
        this.votes = votes;
        this.createdAt = createdAt;
        this.deleted = deleted;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public int getVotes() {
        return votes;
    }

    public void setVotes(int votes) {
        this.votes = votes;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public int getDeleted() {
        return deleted;
    }

    public void setDeleted(int deleted) {
        this.deleted = deleted;
    }

    @XmlTransient
    public List<PollsFiles> getPollsFilesList() {
        return pollsFilesList;
    }

    public void setPollsFilesList(List<PollsFiles> pollsFilesList) {
        this.pollsFilesList = pollsFilesList;
    }

    public Parties getIdParty() {
        return idParty;
    }

    public void setIdParty(Parties idParty) {
        this.idParty = idParty;
    }

    public Elections getIdElection() {
        return idElection;
    }

    public void setIdElection(Elections idElection) {
        this.idElection = idElection;
    }

    public Users getIdUser() {
        return idUser;
    }

    public void setIdUser(Users idUser) {
        this.idUser = idUser;
    }

    public VotingTables getIdVotingTable() {
        return idVotingTable;
    }

    public void setIdVotingTable(VotingTables idVotingTable) {
        this.idVotingTable = idVotingTable;
    }

    public Categories getIdCategory() {
        return idCategory;
    }

    public void setIdCategory(Categories idCategory) {
        this.idCategory = idCategory;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Polls)) {
            return false;
        }
        Polls other = (Polls) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ar.com.singleton.app.fiscalizar.database.Polls[ id=" + id + " ]";
    }
    
}
