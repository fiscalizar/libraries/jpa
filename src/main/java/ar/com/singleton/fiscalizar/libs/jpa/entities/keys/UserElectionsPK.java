/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ar.com.singleton.fiscalizar.libs.jpa.entities.keys;

import ar.com.singleton.fiscalizar.libs.jpa.entities.Elections;
import ar.com.singleton.fiscalizar.libs.jpa.entities.Users;
import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Basic;
import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 *
 * @author pato
 */
@Embeddable
public class UserElectionsPK implements Serializable {

    @Basic
    @ManyToOne
    @JoinColumn(name = "idUser", insertable = false, updatable = false)
    private Users idUser;
    
    @Basic
    @ManyToOne
    @JoinColumn(name = "idElection",insertable = false, updatable = false)
    private Elections idElection;
    
    public UserElectionsPK() {
    }

    public UserElectionsPK(Elections idElection, Users idUser) {
        this.idElection = idElection;
        this.idUser = idUser;
    }

    public Users getIdUser() {
        return idUser;
    }

    public void setIdUser(Users idUser) {
        this.idUser = idUser;
    }

    public Elections getIdElection() {
        return idElection;
    }

    public void setIdElection(Elections idElection) {
        this.idElection = idElection;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 67 * hash + Objects.hashCode(this.idUser);
        hash = 67 * hash + Objects.hashCode(this.idElection);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final UserElectionsPK other = (UserElectionsPK) obj;
        if (!Objects.equals(this.idUser, other.idUser)) {
            return false;
        }
        return Objects.equals(this.idElection, other.idElection);
    }
    
}
