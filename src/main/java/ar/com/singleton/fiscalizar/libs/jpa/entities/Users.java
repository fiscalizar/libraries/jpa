/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ar.com.singleton.fiscalizar.libs.jpa.entities;

import ar.com.singleton.fiscalizar.libs.jpa.entities.relations.UserVotingTables;
import ar.com.singleton.fiscalizar.libs.jpa.entities.relations.UserElections;
import ar.com.singleton.fiscalizar.libs.jpa.entities.relations.UserCircuits;
import ar.com.singleton.fiscalizar.libs.jpa.entities.relations.UserSections;
import ar.com.singleton.fiscalizar.libs.jpa.entities.relations.UserInstitutions;
import ar.com.singleton.fiscalizar.libs.jpa.entities.relations.UserRegions;
import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author pato
 */
@Entity
@Table(name = "Users", catalog = "", schema = "")
@XmlRootElement
public class Users implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "name")
    private String name;
    @Basic(optional = false)
    @Column(name = "email")
    private String email;
    @Column(name = "identification")
    private String identification;
    @Column(name = "type")
    private String type;
    @Column(name = "birth")
    private String birth;
    @Column(name = "passwordSalt")
    private String passwordSalt;
    @Column(name = "passwordHash")
    private String passwordHash;
    @Column(name = "subscribePush")
    private String subscribePush;
    @Column(name = "subscribeEmail")
    private String subscribeEmail;
    @Column(name = "externalId")
    private String externalId;
    @Column(name = "facebookId")
    private String facebookId;
    @Column(name = "twitterId")
    private String twitterId;
    @Column(name = "googleId")
    private String googleId;
    @Basic(optional = false)
    @Column(name = "emailValid")
    private int emailValid;
    @Basic(optional = false)
    @Column(name = "passwordReset")
    private int passwordReset;
    @Basic(optional = false)
    @Column(name = "createdAt")
    private String createdAt;
    @Column(name = "updatedAt")
    private String updatedAt;
    @Basic(optional = false)
    @Column(name = "deleted")
    private int deleted;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idUser")
    private List<UserCircuits> userCircuitsList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idUser")
    private List<UserVotingTables> userVotingTablesList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idUser")
    private List<UserRegions> userRegionsList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idUser")
    private List<UserSections> userSectionsList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idUser")
    private List<UserElections> userElectionsList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idUser")
    private List<UserInstitutions> userInstitutionsList;
    @OneToMany(mappedBy = "idUser")
    private List<Polls> pollsList;

    public Users() {
    }

    public Users(Integer id) {
        this.id = id;
    }

    public Users(Integer id, String name, String email, int emailValid, int passwordReset, String createdAt, int deleted) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.emailValid = emailValid;
        this.passwordReset = passwordReset;
        this.createdAt = createdAt;
        this.deleted = deleted;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getIdentification() {
        return identification;
    }

    public void setIdentification(String identification) {
        this.identification = identification;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getBirth() {
        return birth;
    }

    public void setBirth(String birth) {
        this.birth = birth;
    }

    public String getPasswordSalt() {
        return passwordSalt;
    }

    public void setPasswordSalt(String passwordSalt) {
        this.passwordSalt = passwordSalt;
    }

    public String getPasswordHash() {
        return passwordHash;
    }

    public void setPasswordHash(String passwordHash) {
        this.passwordHash = passwordHash;
    }

    public String getSubscribePush() {
        return subscribePush;
    }

    public void setSubscribePush(String subscribePush) {
        this.subscribePush = subscribePush;
    }

    public String getSubscribeEmail() {
        return subscribeEmail;
    }

    public void setSubscribeEmail(String subscribeEmail) {
        this.subscribeEmail = subscribeEmail;
    }

    public String getExternalId() {
        return externalId;
    }

    public void setExternalId(String externalId) {
        this.externalId = externalId;
    }

    public String getFacebookId() {
        return facebookId;
    }

    public void setFacebookId(String facebookId) {
        this.facebookId = facebookId;
    }

    public String getTwitterId() {
        return twitterId;
    }

    public void setTwitterId(String twitterId) {
        this.twitterId = twitterId;
    }

    public String getGoogleId() {
        return googleId;
    }

    public void setGoogleId(String googleId) {
        this.googleId = googleId;
    }

    public int getEmailValid() {
        return emailValid;
    }

    public void setEmailValid(int emailValid) {
        this.emailValid = emailValid;
    }

    public int getPasswordReset() {
        return passwordReset;
    }

    public void setPasswordReset(int passwordReset) {
        this.passwordReset = passwordReset;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public int getDeleted() {
        return deleted;
    }

    public void setDeleted(int deleted) {
        this.deleted = deleted;
    }

    @XmlTransient
    public List<UserCircuits> getUserCircuitsList() {
        return userCircuitsList;
    }

    public void setUserCircuitsList(List<UserCircuits> userCircuitsList) {
        this.userCircuitsList = userCircuitsList;
    }

    @XmlTransient
    public List<UserVotingTables> getUserVotingTablesList() {
        return userVotingTablesList;
    }

    public void setUserVotingTablesList(List<UserVotingTables> userVotingTablesList) {
        this.userVotingTablesList = userVotingTablesList;
    }

    @XmlTransient
    public List<UserRegions> getUserRegionsList() {
        return userRegionsList;
    }

    public void setUserRegionsList(List<UserRegions> userRegionsList) {
        this.userRegionsList = userRegionsList;
    }

    @XmlTransient
    public List<UserSections> getUserSectionsList() {
        return userSectionsList;
    }

    public void setUserSectionsList(List<UserSections> userSectionsList) {
        this.userSectionsList = userSectionsList;
    }

    @XmlTransient
    public List<UserElections> getUserElectionsList() {
        return userElectionsList;
    }

    public void setUserElectionsList(List<UserElections> userElectionsList) {
        this.userElectionsList = userElectionsList;
    }

    @XmlTransient
    public List<UserInstitutions> getUserInstitutionsList() {
        return userInstitutionsList;
    }

    public void setUserInstitutionsList(List<UserInstitutions> userInstitutionsList) {
        this.userInstitutionsList = userInstitutionsList;
    }

    @XmlTransient
    public List<Polls> getPollsList() {
        return pollsList;
    }

    public void setPollsList(List<Polls> pollsList) {
        this.pollsList = pollsList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Users)) {
            return false;
        }
        Users other = (Users) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ar.com.singleton.app.fiscalizar.database.Users[ id=" + id + " ]";
    }
    
}
