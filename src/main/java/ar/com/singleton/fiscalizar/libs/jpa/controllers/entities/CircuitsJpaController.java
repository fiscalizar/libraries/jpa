/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ar.com.singleton.fiscalizar.libs.jpa.controllers.entities;

import ar.com.singleton.fiscalizar.libs.jpa.controllers.exceptions.IllegalOrphanException;
import ar.com.singleton.fiscalizar.libs.jpa.controllers.exceptions.NonexistentEntityException;
import ar.com.singleton.fiscalizar.libs.jpa.controllers.exceptions.PreexistingEntityException;
import ar.com.singleton.fiscalizar.libs.jpa.entities.Circuits;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import ar.com.singleton.fiscalizar.libs.jpa.entities.Sections;
import ar.com.singleton.fiscalizar.libs.jpa.entities.Institutions;
import java.util.ArrayList;
import java.util.List;
import ar.com.singleton.fiscalizar.libs.jpa.entities.relations.UserCircuits;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author pato
 */
public class CircuitsJpaController implements Serializable {

    public CircuitsJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Circuits circuits) throws PreexistingEntityException, Exception {
        if (circuits.getInstitutionsList() == null) {
            circuits.setInstitutionsList(new ArrayList<Institutions>());
        }
        if (circuits.getUserCircuitsList() == null) {
            circuits.setUserCircuitsList(new ArrayList<UserCircuits>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Sections idSection = circuits.getIdSection();
            if (idSection != null) {
                idSection = em.getReference(idSection.getClass(), idSection.getId());
                circuits.setIdSection(idSection);
            }
            List<Institutions> attachedInstitutionsList = new ArrayList<Institutions>();
            for (Institutions institutionsListInstitutionsToAttach : circuits.getInstitutionsList()) {
                institutionsListInstitutionsToAttach = em.getReference(institutionsListInstitutionsToAttach.getClass(), institutionsListInstitutionsToAttach.getId());
                attachedInstitutionsList.add(institutionsListInstitutionsToAttach);
            }
            circuits.setInstitutionsList(attachedInstitutionsList);
            List<UserCircuits> attachedUserCircuitsList = new ArrayList<UserCircuits>();
            for (UserCircuits userCircuitsListUserCircuitsToAttach : circuits.getUserCircuitsList()) {
                userCircuitsListUserCircuitsToAttach = em.getReference(userCircuitsListUserCircuitsToAttach.getClass(), userCircuitsListUserCircuitsToAttach.getUserCircuitsPK());
                attachedUserCircuitsList.add(userCircuitsListUserCircuitsToAttach);
            }
            circuits.setUserCircuitsList(attachedUserCircuitsList);
            em.persist(circuits);
            if (idSection != null) {
                idSection.getCircuitsList().add(circuits);
                idSection = em.merge(idSection);
            }
            for (Institutions institutionsListInstitutions : circuits.getInstitutionsList()) {
                Circuits oldIdCircuitOfInstitutionsListInstitutions = institutionsListInstitutions.getIdCircuit();
                institutionsListInstitutions.setIdCircuit(circuits);
                institutionsListInstitutions = em.merge(institutionsListInstitutions);
                if (oldIdCircuitOfInstitutionsListInstitutions != null) {
                    oldIdCircuitOfInstitutionsListInstitutions.getInstitutionsList().remove(institutionsListInstitutions);
                    oldIdCircuitOfInstitutionsListInstitutions = em.merge(oldIdCircuitOfInstitutionsListInstitutions);
                }
            }
            for (UserCircuits userCircuitsListUserCircuits : circuits.getUserCircuitsList()) {
                Circuits oldIdCircuitOfUserCircuitsListUserCircuits = userCircuitsListUserCircuits.getIdCircuit();
                userCircuitsListUserCircuits.setIdCircuit(circuits);
                userCircuitsListUserCircuits = em.merge(userCircuitsListUserCircuits);
                if (oldIdCircuitOfUserCircuitsListUserCircuits != null) {
                    oldIdCircuitOfUserCircuitsListUserCircuits.getUserCircuitsList().remove(userCircuitsListUserCircuits);
                    oldIdCircuitOfUserCircuitsListUserCircuits = em.merge(oldIdCircuitOfUserCircuitsListUserCircuits);
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findCircuits(circuits.getId()) != null) {
                throw new PreexistingEntityException("Circuits " + circuits + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Circuits circuits) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Circuits persistentCircuits = em.find(Circuits.class, circuits.getId());
            Sections idSectionOld = persistentCircuits.getIdSection();
            Sections idSectionNew = circuits.getIdSection();
            List<Institutions> institutionsListOld = persistentCircuits.getInstitutionsList();
            List<Institutions> institutionsListNew = circuits.getInstitutionsList();
            List<UserCircuits> userCircuitsListOld = persistentCircuits.getUserCircuitsList();
            List<UserCircuits> userCircuitsListNew = circuits.getUserCircuitsList();
            List<String> illegalOrphanMessages = null;
            for (Institutions institutionsListOldInstitutions : institutionsListOld) {
                if (!institutionsListNew.contains(institutionsListOldInstitutions)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Institutions " + institutionsListOldInstitutions + " since its idCircuit field is not nullable.");
                }
            }
            for (UserCircuits userCircuitsListOldUserCircuits : userCircuitsListOld) {
                if (!userCircuitsListNew.contains(userCircuitsListOldUserCircuits)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain UserCircuits " + userCircuitsListOldUserCircuits + " since its idCircuit field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            if (idSectionNew != null) {
                idSectionNew = em.getReference(idSectionNew.getClass(), idSectionNew.getId());
                circuits.setIdSection(idSectionNew);
            }
            List<Institutions> attachedInstitutionsListNew = new ArrayList<Institutions>();
            for (Institutions institutionsListNewInstitutionsToAttach : institutionsListNew) {
                institutionsListNewInstitutionsToAttach = em.getReference(institutionsListNewInstitutionsToAttach.getClass(), institutionsListNewInstitutionsToAttach.getId());
                attachedInstitutionsListNew.add(institutionsListNewInstitutionsToAttach);
            }
            institutionsListNew = attachedInstitutionsListNew;
            circuits.setInstitutionsList(institutionsListNew);
            List<UserCircuits> attachedUserCircuitsListNew = new ArrayList<UserCircuits>();
            for (UserCircuits userCircuitsListNewUserCircuitsToAttach : userCircuitsListNew) {
                userCircuitsListNewUserCircuitsToAttach = em.getReference(userCircuitsListNewUserCircuitsToAttach.getClass(), userCircuitsListNewUserCircuitsToAttach.getUserCircuitsPK());
                attachedUserCircuitsListNew.add(userCircuitsListNewUserCircuitsToAttach);
            }
            userCircuitsListNew = attachedUserCircuitsListNew;
            circuits.setUserCircuitsList(userCircuitsListNew);
            circuits = em.merge(circuits);
            if (idSectionOld != null && !idSectionOld.equals(idSectionNew)) {
                idSectionOld.getCircuitsList().remove(circuits);
                idSectionOld = em.merge(idSectionOld);
            }
            if (idSectionNew != null && !idSectionNew.equals(idSectionOld)) {
                idSectionNew.getCircuitsList().add(circuits);
                idSectionNew = em.merge(idSectionNew);
            }
            for (Institutions institutionsListNewInstitutions : institutionsListNew) {
                if (!institutionsListOld.contains(institutionsListNewInstitutions)) {
                    Circuits oldIdCircuitOfInstitutionsListNewInstitutions = institutionsListNewInstitutions.getIdCircuit();
                    institutionsListNewInstitutions.setIdCircuit(circuits);
                    institutionsListNewInstitutions = em.merge(institutionsListNewInstitutions);
                    if (oldIdCircuitOfInstitutionsListNewInstitutions != null && !oldIdCircuitOfInstitutionsListNewInstitutions.equals(circuits)) {
                        oldIdCircuitOfInstitutionsListNewInstitutions.getInstitutionsList().remove(institutionsListNewInstitutions);
                        oldIdCircuitOfInstitutionsListNewInstitutions = em.merge(oldIdCircuitOfInstitutionsListNewInstitutions);
                    }
                }
            }
            for (UserCircuits userCircuitsListNewUserCircuits : userCircuitsListNew) {
                if (!userCircuitsListOld.contains(userCircuitsListNewUserCircuits)) {
                    Circuits oldIdCircuitOfUserCircuitsListNewUserCircuits = userCircuitsListNewUserCircuits.getIdCircuit();
                    userCircuitsListNewUserCircuits.setIdCircuit(circuits);
                    userCircuitsListNewUserCircuits = em.merge(userCircuitsListNewUserCircuits);
                    if (oldIdCircuitOfUserCircuitsListNewUserCircuits != null && !oldIdCircuitOfUserCircuitsListNewUserCircuits.equals(circuits)) {
                        oldIdCircuitOfUserCircuitsListNewUserCircuits.getUserCircuitsList().remove(userCircuitsListNewUserCircuits);
                        oldIdCircuitOfUserCircuitsListNewUserCircuits = em.merge(oldIdCircuitOfUserCircuitsListNewUserCircuits);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = circuits.getId();
                if (findCircuits(id) == null) {
                    throw new NonexistentEntityException("The circuits with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Circuits circuits;
            try {
                circuits = em.getReference(Circuits.class, id);
                circuits.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The circuits with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            List<Institutions> institutionsListOrphanCheck = circuits.getInstitutionsList();
            for (Institutions institutionsListOrphanCheckInstitutions : institutionsListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Circuits (" + circuits + ") cannot be destroyed since the Institutions " + institutionsListOrphanCheckInstitutions + " in its institutionsList field has a non-nullable idCircuit field.");
            }
            List<UserCircuits> userCircuitsListOrphanCheck = circuits.getUserCircuitsList();
            for (UserCircuits userCircuitsListOrphanCheckUserCircuits : userCircuitsListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Circuits (" + circuits + ") cannot be destroyed since the UserCircuits " + userCircuitsListOrphanCheckUserCircuits + " in its userCircuitsList field has a non-nullable idCircuit field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            Sections idSection = circuits.getIdSection();
            if (idSection != null) {
                idSection.getCircuitsList().remove(circuits);
                idSection = em.merge(idSection);
            }
            em.remove(circuits);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Circuits> findCircuitsEntities() {
        return findCircuitsEntities(true, -1, -1);
    }

    public List<Circuits> findCircuitsEntities(int maxResults, int firstResult) {
        return findCircuitsEntities(false, maxResults, firstResult);
    }

    private List<Circuits> findCircuitsEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Circuits.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Circuits findCircuits(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Circuits.class, id);
        } finally {
            em.close();
        }
    }

    public int getCircuitsCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Circuits> rt = cq.from(Circuits.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
