/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ar.com.singleton.fiscalizar.libs.jpa.controllers.entities.relations;

import ar.com.singleton.fiscalizar.libs.jpa.controllers.exceptions.NonexistentEntityException;
import ar.com.singleton.fiscalizar.libs.jpa.controllers.exceptions.PreexistingEntityException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import ar.com.singleton.fiscalizar.libs.jpa.entities.Elections;
import ar.com.singleton.fiscalizar.libs.jpa.entities.Users;
import ar.com.singleton.fiscalizar.libs.jpa.entities.keys.UserElectionsPK;
import ar.com.singleton.fiscalizar.libs.jpa.entities.relations.UserElections;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author pato
 */
public class UserElectionsJpaController implements Serializable {

    public UserElectionsJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(UserElections userElections) throws PreexistingEntityException, Exception {
        if (userElections.getUserElectionsPK() == null) {
            userElections.setUserElectionsPK(new UserElectionsPK());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Elections idElection = userElections.getIdElection();
            if (idElection != null) {
                idElection = em.getReference(idElection.getClass(), idElection.getId());
                userElections.setIdElection(idElection);
            }
            Users idUser = userElections.getIdUser();
            if (idUser != null) {
                idUser = em.getReference(idUser.getClass(), idUser.getId());
                userElections.setIdUser(idUser);
            }
            em.persist(userElections);
            if (idElection != null) {
                idElection.getUserElectionsList().add(userElections);
                idElection = em.merge(idElection);
            }
            if (idUser != null) {
                idUser.getUserElectionsList().add(userElections);
                idUser = em.merge(idUser);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findUserElections(userElections.getUserElectionsPK()) != null) {
                throw new PreexistingEntityException("UserElections " + userElections + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(UserElections userElections) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            UserElections persistentUserElections = em.find(UserElections.class, userElections.getUserElectionsPK());
            Elections idElectionOld = persistentUserElections.getIdElection();
            Elections idElectionNew = userElections.getIdElection();
            Users idUserOld = persistentUserElections.getIdUser();
            Users idUserNew = userElections.getIdUser();
            if (idElectionNew != null) {
                idElectionNew = em.getReference(idElectionNew.getClass(), idElectionNew.getId());
                userElections.setIdElection(idElectionNew);
            }
            if (idUserNew != null) {
                idUserNew = em.getReference(idUserNew.getClass(), idUserNew.getId());
                userElections.setIdUser(idUserNew);
            }
            userElections = em.merge(userElections);
            if (idElectionOld != null && !idElectionOld.equals(idElectionNew)) {
                idElectionOld.getUserElectionsList().remove(userElections);
                idElectionOld = em.merge(idElectionOld);
            }
            if (idElectionNew != null && !idElectionNew.equals(idElectionOld)) {
                idElectionNew.getUserElectionsList().add(userElections);
                idElectionNew = em.merge(idElectionNew);
            }
            if (idUserOld != null && !idUserOld.equals(idUserNew)) {
                idUserOld.getUserElectionsList().remove(userElections);
                idUserOld = em.merge(idUserOld);
            }
            if (idUserNew != null && !idUserNew.equals(idUserOld)) {
                idUserNew.getUserElectionsList().add(userElections);
                idUserNew = em.merge(idUserNew);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                UserElectionsPK id = userElections.getUserElectionsPK();
                if (findUserElections(id) == null) {
                    throw new NonexistentEntityException("The userElections with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(UserElectionsPK id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            UserElections userElections;
            try {
                userElections = em.getReference(UserElections.class, id);
                userElections.getUserElectionsPK();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The userElections with id " + id + " no longer exists.", enfe);
            }
            Elections idElection = userElections.getIdElection();
            if (idElection != null) {
                idElection.getUserElectionsList().remove(userElections);
                idElection = em.merge(idElection);
            }
            Users idUser = userElections.getIdUser();
            if (idUser != null) {
                idUser.getUserElectionsList().remove(userElections);
                idUser = em.merge(idUser);
            }
            em.remove(userElections);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<UserElections> findUserElectionsEntities() {
        return findUserElectionsEntities(true, -1, -1);
    }

    public List<UserElections> findUserElectionsEntities(int maxResults, int firstResult) {
        return findUserElectionsEntities(false, maxResults, firstResult);
    }

    private List<UserElections> findUserElectionsEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(UserElections.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public UserElections findUserElections(UserElectionsPK id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(UserElections.class, id);
        } finally {
            em.close();
        }
    }

    public int getUserElectionsCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<UserElections> rt = cq.from(UserElections.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
