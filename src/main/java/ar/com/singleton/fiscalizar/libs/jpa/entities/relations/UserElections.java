/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ar.com.singleton.fiscalizar.libs.jpa.entities.relations;

import ar.com.singleton.fiscalizar.libs.jpa.entities.Elections;
import ar.com.singleton.fiscalizar.libs.jpa.entities.Users;
import ar.com.singleton.fiscalizar.libs.jpa.entities.keys.UserElectionsPK;
import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author pato
 */
@Entity
@Table(name = "UserElections", catalog = "", schema = "")
@XmlRootElement
public class UserElections implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected UserElectionsPK userElectionsPK;
    @Basic(optional = false)
    @Column(name = "createdAt")
    private String createdAt;
    @Column(name = "updatedAt")
    private String updatedAt;
    @Basic(optional = false)
    @Column(name = "deleted")
    private int deleted;
    @PrimaryKeyJoinColumn(name = "idElection", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Elections idElection;
    @PrimaryKeyJoinColumn(name = "idUser", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Users idUser;

    public UserElections() {
    }

    public UserElections(UserElectionsPK userElectionsPK) {
        this.userElectionsPK = userElectionsPK;
    }

    public UserElections(UserElectionsPK userElectionsPK, String createdAt, int deleted) {
        this.userElectionsPK = userElectionsPK;
        this.createdAt = createdAt;
        this.deleted = deleted;
    }

    public UserElectionsPK getUserElectionsPK() {
        return userElectionsPK;
    }

    public void setUserElectionsPK(UserElectionsPK userElectionsPK) {
        this.userElectionsPK = userElectionsPK;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public int getDeleted() {
        return deleted;
    }

    public void setDeleted(int deleted) {
        this.deleted = deleted;
    }

    public Elections getIdElection() {
        return idElection;
    }

    public void setIdElection(Elections idElection) {
        this.idElection = idElection;
    }

    public Users getIdUser() {
        return idUser;
    }

    public void setIdUser(Users idUser) {
        this.idUser = idUser;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (userElectionsPK != null ? userElectionsPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UserElections)) {
            return false;
        }
        UserElections other = (UserElections) object;
        if ((this.userElectionsPK == null && other.userElectionsPK != null) || (this.userElectionsPK != null && !this.userElectionsPK.equals(other.userElectionsPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ar.com.singleton.app.fiscalizar.database.UserElections[ userElectionsPK=" + userElectionsPK + " ]";
    }
    
}
