/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ar.com.singleton.fiscalizar.libs.jpa.controllers.entities;

import ar.com.singleton.fiscalizar.libs.jpa.controllers.exceptions.IllegalOrphanException;
import ar.com.singleton.fiscalizar.libs.jpa.controllers.exceptions.NonexistentEntityException;
import ar.com.singleton.fiscalizar.libs.jpa.controllers.exceptions.PreexistingEntityException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import ar.com.singleton.fiscalizar.libs.jpa.entities.Circuits;
import ar.com.singleton.fiscalizar.libs.jpa.entities.Institutions;
import ar.com.singleton.fiscalizar.libs.jpa.entities.VotingTables;
import java.util.ArrayList;
import java.util.List;
import ar.com.singleton.fiscalizar.libs.jpa.entities.relations.UserInstitutions;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author pato
 */
public class InstitutionsJpaController implements Serializable {

    public InstitutionsJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Institutions institutions) throws PreexistingEntityException, Exception {
        if (institutions.getVotingTablesList() == null) {
            institutions.setVotingTablesList(new ArrayList<VotingTables>());
        }
        if (institutions.getUserInstitutionsList() == null) {
            institutions.setUserInstitutionsList(new ArrayList<UserInstitutions>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Circuits idCircuit = institutions.getIdCircuit();
            if (idCircuit != null) {
                idCircuit = em.getReference(idCircuit.getClass(), idCircuit.getId());
                institutions.setIdCircuit(idCircuit);
            }
            List<VotingTables> attachedVotingTablesList = new ArrayList<VotingTables>();
            for (VotingTables votingTablesListVotingTablesToAttach : institutions.getVotingTablesList()) {
                votingTablesListVotingTablesToAttach = em.getReference(votingTablesListVotingTablesToAttach.getClass(), votingTablesListVotingTablesToAttach.getId());
                attachedVotingTablesList.add(votingTablesListVotingTablesToAttach);
            }
            institutions.setVotingTablesList(attachedVotingTablesList);
            List<UserInstitutions> attachedUserInstitutionsList = new ArrayList<UserInstitutions>();
            for (UserInstitutions userInstitutionsListUserInstitutionsToAttach : institutions.getUserInstitutionsList()) {
                userInstitutionsListUserInstitutionsToAttach = em.getReference(userInstitutionsListUserInstitutionsToAttach.getClass(), userInstitutionsListUserInstitutionsToAttach.getUserInstitutionsPK());
                attachedUserInstitutionsList.add(userInstitutionsListUserInstitutionsToAttach);
            }
            institutions.setUserInstitutionsList(attachedUserInstitutionsList);
            em.persist(institutions);
            if (idCircuit != null) {
                idCircuit.getInstitutionsList().add(institutions);
                idCircuit = em.merge(idCircuit);
            }
            for (VotingTables votingTablesListVotingTables : institutions.getVotingTablesList()) {
                Institutions oldIdInstitutionOfVotingTablesListVotingTables = votingTablesListVotingTables.getIdInstitution();
                votingTablesListVotingTables.setIdInstitution(institutions);
                votingTablesListVotingTables = em.merge(votingTablesListVotingTables);
                if (oldIdInstitutionOfVotingTablesListVotingTables != null) {
                    oldIdInstitutionOfVotingTablesListVotingTables.getVotingTablesList().remove(votingTablesListVotingTables);
                    oldIdInstitutionOfVotingTablesListVotingTables = em.merge(oldIdInstitutionOfVotingTablesListVotingTables);
                }
            }
            for (UserInstitutions userInstitutionsListUserInstitutions : institutions.getUserInstitutionsList()) {
                Institutions oldIdUserInstitutionOfUserInstitutionsListUserInstitutions = userInstitutionsListUserInstitutions.getIdUserInstitution();
                userInstitutionsListUserInstitutions.setIdUserInstitution(institutions);
                userInstitutionsListUserInstitutions = em.merge(userInstitutionsListUserInstitutions);
                if (oldIdUserInstitutionOfUserInstitutionsListUserInstitutions != null) {
                    oldIdUserInstitutionOfUserInstitutionsListUserInstitutions.getUserInstitutionsList().remove(userInstitutionsListUserInstitutions);
                    oldIdUserInstitutionOfUserInstitutionsListUserInstitutions = em.merge(oldIdUserInstitutionOfUserInstitutionsListUserInstitutions);
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findInstitutions(institutions.getId()) != null) {
                throw new PreexistingEntityException("Institutions " + institutions + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Institutions institutions) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Institutions persistentInstitutions = em.find(Institutions.class, institutions.getId());
            Circuits idCircuitOld = persistentInstitutions.getIdCircuit();
            Circuits idCircuitNew = institutions.getIdCircuit();
            List<VotingTables> votingTablesListOld = persistentInstitutions.getVotingTablesList();
            List<VotingTables> votingTablesListNew = institutions.getVotingTablesList();
            List<UserInstitutions> userInstitutionsListOld = persistentInstitutions.getUserInstitutionsList();
            List<UserInstitutions> userInstitutionsListNew = institutions.getUserInstitutionsList();
            List<String> illegalOrphanMessages = null;
            for (VotingTables votingTablesListOldVotingTables : votingTablesListOld) {
                if (!votingTablesListNew.contains(votingTablesListOldVotingTables)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain VotingTables " + votingTablesListOldVotingTables + " since its idInstitution field is not nullable.");
                }
            }
            for (UserInstitutions userInstitutionsListOldUserInstitutions : userInstitutionsListOld) {
                if (!userInstitutionsListNew.contains(userInstitutionsListOldUserInstitutions)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain UserInstitutions " + userInstitutionsListOldUserInstitutions + " since its idUserInstitution field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            if (idCircuitNew != null) {
                idCircuitNew = em.getReference(idCircuitNew.getClass(), idCircuitNew.getId());
                institutions.setIdCircuit(idCircuitNew);
            }
            List<VotingTables> attachedVotingTablesListNew = new ArrayList<VotingTables>();
            for (VotingTables votingTablesListNewVotingTablesToAttach : votingTablesListNew) {
                votingTablesListNewVotingTablesToAttach = em.getReference(votingTablesListNewVotingTablesToAttach.getClass(), votingTablesListNewVotingTablesToAttach.getId());
                attachedVotingTablesListNew.add(votingTablesListNewVotingTablesToAttach);
            }
            votingTablesListNew = attachedVotingTablesListNew;
            institutions.setVotingTablesList(votingTablesListNew);
            List<UserInstitutions> attachedUserInstitutionsListNew = new ArrayList<UserInstitutions>();
            for (UserInstitutions userInstitutionsListNewUserInstitutionsToAttach : userInstitutionsListNew) {
                userInstitutionsListNewUserInstitutionsToAttach = em.getReference(userInstitutionsListNewUserInstitutionsToAttach.getClass(), userInstitutionsListNewUserInstitutionsToAttach.getUserInstitutionsPK());
                attachedUserInstitutionsListNew.add(userInstitutionsListNewUserInstitutionsToAttach);
            }
            userInstitutionsListNew = attachedUserInstitutionsListNew;
            institutions.setUserInstitutionsList(userInstitutionsListNew);
            institutions = em.merge(institutions);
            if (idCircuitOld != null && !idCircuitOld.equals(idCircuitNew)) {
                idCircuitOld.getInstitutionsList().remove(institutions);
                idCircuitOld = em.merge(idCircuitOld);
            }
            if (idCircuitNew != null && !idCircuitNew.equals(idCircuitOld)) {
                idCircuitNew.getInstitutionsList().add(institutions);
                idCircuitNew = em.merge(idCircuitNew);
            }
            for (VotingTables votingTablesListNewVotingTables : votingTablesListNew) {
                if (!votingTablesListOld.contains(votingTablesListNewVotingTables)) {
                    Institutions oldIdInstitutionOfVotingTablesListNewVotingTables = votingTablesListNewVotingTables.getIdInstitution();
                    votingTablesListNewVotingTables.setIdInstitution(institutions);
                    votingTablesListNewVotingTables = em.merge(votingTablesListNewVotingTables);
                    if (oldIdInstitutionOfVotingTablesListNewVotingTables != null && !oldIdInstitutionOfVotingTablesListNewVotingTables.equals(institutions)) {
                        oldIdInstitutionOfVotingTablesListNewVotingTables.getVotingTablesList().remove(votingTablesListNewVotingTables);
                        oldIdInstitutionOfVotingTablesListNewVotingTables = em.merge(oldIdInstitutionOfVotingTablesListNewVotingTables);
                    }
                }
            }
            for (UserInstitutions userInstitutionsListNewUserInstitutions : userInstitutionsListNew) {
                if (!userInstitutionsListOld.contains(userInstitutionsListNewUserInstitutions)) {
                    Institutions oldIdUserInstitutionOfUserInstitutionsListNewUserInstitutions = userInstitutionsListNewUserInstitutions.getIdUserInstitution();
                    userInstitutionsListNewUserInstitutions.setIdUserInstitution(institutions);
                    userInstitutionsListNewUserInstitutions = em.merge(userInstitutionsListNewUserInstitutions);
                    if (oldIdUserInstitutionOfUserInstitutionsListNewUserInstitutions != null && !oldIdUserInstitutionOfUserInstitutionsListNewUserInstitutions.equals(institutions)) {
                        oldIdUserInstitutionOfUserInstitutionsListNewUserInstitutions.getUserInstitutionsList().remove(userInstitutionsListNewUserInstitutions);
                        oldIdUserInstitutionOfUserInstitutionsListNewUserInstitutions = em.merge(oldIdUserInstitutionOfUserInstitutionsListNewUserInstitutions);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = institutions.getId();
                if (findInstitutions(id) == null) {
                    throw new NonexistentEntityException("The institutions with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Institutions institutions;
            try {
                institutions = em.getReference(Institutions.class, id);
                institutions.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The institutions with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            List<VotingTables> votingTablesListOrphanCheck = institutions.getVotingTablesList();
            for (VotingTables votingTablesListOrphanCheckVotingTables : votingTablesListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Institutions (" + institutions + ") cannot be destroyed since the VotingTables " + votingTablesListOrphanCheckVotingTables + " in its votingTablesList field has a non-nullable idInstitution field.");
            }
            List<UserInstitutions> userInstitutionsListOrphanCheck = institutions.getUserInstitutionsList();
            for (UserInstitutions userInstitutionsListOrphanCheckUserInstitutions : userInstitutionsListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Institutions (" + institutions + ") cannot be destroyed since the UserInstitutions " + userInstitutionsListOrphanCheckUserInstitutions + " in its userInstitutionsList field has a non-nullable idUserInstitution field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            Circuits idCircuit = institutions.getIdCircuit();
            if (idCircuit != null) {
                idCircuit.getInstitutionsList().remove(institutions);
                idCircuit = em.merge(idCircuit);
            }
            em.remove(institutions);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Institutions> findInstitutionsEntities() {
        return findInstitutionsEntities(true, -1, -1);
    }

    public List<Institutions> findInstitutionsEntities(int maxResults, int firstResult) {
        return findInstitutionsEntities(false, maxResults, firstResult);
    }

    private List<Institutions> findInstitutionsEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Institutions.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Institutions findInstitutions(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Institutions.class, id);
        } finally {
            em.close();
        }
    }

    public int getInstitutionsCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Institutions> rt = cq.from(Institutions.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
