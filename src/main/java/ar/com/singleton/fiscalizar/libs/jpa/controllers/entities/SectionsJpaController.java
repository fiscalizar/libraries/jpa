/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ar.com.singleton.fiscalizar.libs.jpa.controllers.entities;

import ar.com.singleton.fiscalizar.libs.jpa.controllers.exceptions.IllegalOrphanException;
import ar.com.singleton.fiscalizar.libs.jpa.controllers.exceptions.NonexistentEntityException;
import ar.com.singleton.fiscalizar.libs.jpa.controllers.exceptions.PreexistingEntityException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import ar.com.singleton.fiscalizar.libs.jpa.entities.Regions;
import ar.com.singleton.fiscalizar.libs.jpa.entities.relations.ElectionsSections;
import java.util.ArrayList;
import java.util.List;
import ar.com.singleton.fiscalizar.libs.jpa.entities.relations.UserSections;
import ar.com.singleton.fiscalizar.libs.jpa.entities.Circuits;
import ar.com.singleton.fiscalizar.libs.jpa.entities.Sections;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author pato
 */
public class SectionsJpaController implements Serializable {

    public SectionsJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Sections sections) throws PreexistingEntityException, Exception {
        if (sections.getElectionsSectionsList() == null) {
            sections.setElectionsSectionsList(new ArrayList<ElectionsSections>());
        }
        if (sections.getUserSectionsList() == null) {
            sections.setUserSectionsList(new ArrayList<UserSections>());
        }
        if (sections.getCircuitsList() == null) {
            sections.setCircuitsList(new ArrayList<Circuits>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Regions idRegion = sections.getIdRegion();
            if (idRegion != null) {
                idRegion = em.getReference(idRegion.getClass(), idRegion.getId());
                sections.setIdRegion(idRegion);
            }
            List<ElectionsSections> attachedElectionsSectionsList = new ArrayList<ElectionsSections>();
            for (ElectionsSections electionsSectionsListElectionsSectionsToAttach : sections.getElectionsSectionsList()) {
                electionsSectionsListElectionsSectionsToAttach = em.getReference(electionsSectionsListElectionsSectionsToAttach.getClass(), electionsSectionsListElectionsSectionsToAttach.getElectionsSectionsPK());
                attachedElectionsSectionsList.add(electionsSectionsListElectionsSectionsToAttach);
            }
            sections.setElectionsSectionsList(attachedElectionsSectionsList);
            List<UserSections> attachedUserSectionsList = new ArrayList<UserSections>();
            for (UserSections userSectionsListUserSectionsToAttach : sections.getUserSectionsList()) {
                userSectionsListUserSectionsToAttach = em.getReference(userSectionsListUserSectionsToAttach.getClass(), userSectionsListUserSectionsToAttach.getUserSectionsPK());
                attachedUserSectionsList.add(userSectionsListUserSectionsToAttach);
            }
            sections.setUserSectionsList(attachedUserSectionsList);
            List<Circuits> attachedCircuitsList = new ArrayList<Circuits>();
            for (Circuits circuitsListCircuitsToAttach : sections.getCircuitsList()) {
                circuitsListCircuitsToAttach = em.getReference(circuitsListCircuitsToAttach.getClass(), circuitsListCircuitsToAttach.getId());
                attachedCircuitsList.add(circuitsListCircuitsToAttach);
            }
            sections.setCircuitsList(attachedCircuitsList);
            em.persist(sections);
            if (idRegion != null) {
                idRegion.getSectionsList().add(sections);
                idRegion = em.merge(idRegion);
            }
            for (ElectionsSections electionsSectionsListElectionsSections : sections.getElectionsSectionsList()) {
                Sections oldIdSectionOfElectionsSectionsListElectionsSections = electionsSectionsListElectionsSections.getIdSection();
                electionsSectionsListElectionsSections.setIdSection(sections);
                electionsSectionsListElectionsSections = em.merge(electionsSectionsListElectionsSections);
                if (oldIdSectionOfElectionsSectionsListElectionsSections != null) {
                    oldIdSectionOfElectionsSectionsListElectionsSections.getElectionsSectionsList().remove(electionsSectionsListElectionsSections);
                    oldIdSectionOfElectionsSectionsListElectionsSections = em.merge(oldIdSectionOfElectionsSectionsListElectionsSections);
                }
            }
            for (UserSections userSectionsListUserSections : sections.getUserSectionsList()) {
                Sections oldIdSectionOfUserSectionsListUserSections = userSectionsListUserSections.getIdSection();
                userSectionsListUserSections.setIdSection(sections);
                userSectionsListUserSections = em.merge(userSectionsListUserSections);
                if (oldIdSectionOfUserSectionsListUserSections != null) {
                    oldIdSectionOfUserSectionsListUserSections.getUserSectionsList().remove(userSectionsListUserSections);
                    oldIdSectionOfUserSectionsListUserSections = em.merge(oldIdSectionOfUserSectionsListUserSections);
                }
            }
            for (Circuits circuitsListCircuits : sections.getCircuitsList()) {
                Sections oldIdSectionOfCircuitsListCircuits = circuitsListCircuits.getIdSection();
                circuitsListCircuits.setIdSection(sections);
                circuitsListCircuits = em.merge(circuitsListCircuits);
                if (oldIdSectionOfCircuitsListCircuits != null) {
                    oldIdSectionOfCircuitsListCircuits.getCircuitsList().remove(circuitsListCircuits);
                    oldIdSectionOfCircuitsListCircuits = em.merge(oldIdSectionOfCircuitsListCircuits);
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findSections(sections.getId()) != null) {
                throw new PreexistingEntityException("Sections " + sections + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Sections sections) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Sections persistentSections = em.find(Sections.class, sections.getId());
            Regions idRegionOld = persistentSections.getIdRegion();
            Regions idRegionNew = sections.getIdRegion();
            List<ElectionsSections> electionsSectionsListOld = persistentSections.getElectionsSectionsList();
            List<ElectionsSections> electionsSectionsListNew = sections.getElectionsSectionsList();
            List<UserSections> userSectionsListOld = persistentSections.getUserSectionsList();
            List<UserSections> userSectionsListNew = sections.getUserSectionsList();
            List<Circuits> circuitsListOld = persistentSections.getCircuitsList();
            List<Circuits> circuitsListNew = sections.getCircuitsList();
            List<String> illegalOrphanMessages = null;
            for (ElectionsSections electionsSectionsListOldElectionsSections : electionsSectionsListOld) {
                if (!electionsSectionsListNew.contains(electionsSectionsListOldElectionsSections)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain ElectionsSections " + electionsSectionsListOldElectionsSections + " since its idSection field is not nullable.");
                }
            }
            for (UserSections userSectionsListOldUserSections : userSectionsListOld) {
                if (!userSectionsListNew.contains(userSectionsListOldUserSections)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain UserSections " + userSectionsListOldUserSections + " since its idSection field is not nullable.");
                }
            }
            for (Circuits circuitsListOldCircuits : circuitsListOld) {
                if (!circuitsListNew.contains(circuitsListOldCircuits)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Circuits " + circuitsListOldCircuits + " since its idSection field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            if (idRegionNew != null) {
                idRegionNew = em.getReference(idRegionNew.getClass(), idRegionNew.getId());
                sections.setIdRegion(idRegionNew);
            }
            List<ElectionsSections> attachedElectionsSectionsListNew = new ArrayList<ElectionsSections>();
            for (ElectionsSections electionsSectionsListNewElectionsSectionsToAttach : electionsSectionsListNew) {
                electionsSectionsListNewElectionsSectionsToAttach = em.getReference(electionsSectionsListNewElectionsSectionsToAttach.getClass(), electionsSectionsListNewElectionsSectionsToAttach.getElectionsSectionsPK());
                attachedElectionsSectionsListNew.add(electionsSectionsListNewElectionsSectionsToAttach);
            }
            electionsSectionsListNew = attachedElectionsSectionsListNew;
            sections.setElectionsSectionsList(electionsSectionsListNew);
            List<UserSections> attachedUserSectionsListNew = new ArrayList<UserSections>();
            for (UserSections userSectionsListNewUserSectionsToAttach : userSectionsListNew) {
                userSectionsListNewUserSectionsToAttach = em.getReference(userSectionsListNewUserSectionsToAttach.getClass(), userSectionsListNewUserSectionsToAttach.getUserSectionsPK());
                attachedUserSectionsListNew.add(userSectionsListNewUserSectionsToAttach);
            }
            userSectionsListNew = attachedUserSectionsListNew;
            sections.setUserSectionsList(userSectionsListNew);
            List<Circuits> attachedCircuitsListNew = new ArrayList<Circuits>();
            for (Circuits circuitsListNewCircuitsToAttach : circuitsListNew) {
                circuitsListNewCircuitsToAttach = em.getReference(circuitsListNewCircuitsToAttach.getClass(), circuitsListNewCircuitsToAttach.getId());
                attachedCircuitsListNew.add(circuitsListNewCircuitsToAttach);
            }
            circuitsListNew = attachedCircuitsListNew;
            sections.setCircuitsList(circuitsListNew);
            sections = em.merge(sections);
            if (idRegionOld != null && !idRegionOld.equals(idRegionNew)) {
                idRegionOld.getSectionsList().remove(sections);
                idRegionOld = em.merge(idRegionOld);
            }
            if (idRegionNew != null && !idRegionNew.equals(idRegionOld)) {
                idRegionNew.getSectionsList().add(sections);
                idRegionNew = em.merge(idRegionNew);
            }
            for (ElectionsSections electionsSectionsListNewElectionsSections : electionsSectionsListNew) {
                if (!electionsSectionsListOld.contains(electionsSectionsListNewElectionsSections)) {
                    Sections oldIdSectionOfElectionsSectionsListNewElectionsSections = electionsSectionsListNewElectionsSections.getIdSection();
                    electionsSectionsListNewElectionsSections.setIdSection(sections);
                    electionsSectionsListNewElectionsSections = em.merge(electionsSectionsListNewElectionsSections);
                    if (oldIdSectionOfElectionsSectionsListNewElectionsSections != null && !oldIdSectionOfElectionsSectionsListNewElectionsSections.equals(sections)) {
                        oldIdSectionOfElectionsSectionsListNewElectionsSections.getElectionsSectionsList().remove(electionsSectionsListNewElectionsSections);
                        oldIdSectionOfElectionsSectionsListNewElectionsSections = em.merge(oldIdSectionOfElectionsSectionsListNewElectionsSections);
                    }
                }
            }
            for (UserSections userSectionsListNewUserSections : userSectionsListNew) {
                if (!userSectionsListOld.contains(userSectionsListNewUserSections)) {
                    Sections oldIdSectionOfUserSectionsListNewUserSections = userSectionsListNewUserSections.getIdSection();
                    userSectionsListNewUserSections.setIdSection(sections);
                    userSectionsListNewUserSections = em.merge(userSectionsListNewUserSections);
                    if (oldIdSectionOfUserSectionsListNewUserSections != null && !oldIdSectionOfUserSectionsListNewUserSections.equals(sections)) {
                        oldIdSectionOfUserSectionsListNewUserSections.getUserSectionsList().remove(userSectionsListNewUserSections);
                        oldIdSectionOfUserSectionsListNewUserSections = em.merge(oldIdSectionOfUserSectionsListNewUserSections);
                    }
                }
            }
            for (Circuits circuitsListNewCircuits : circuitsListNew) {
                if (!circuitsListOld.contains(circuitsListNewCircuits)) {
                    Sections oldIdSectionOfCircuitsListNewCircuits = circuitsListNewCircuits.getIdSection();
                    circuitsListNewCircuits.setIdSection(sections);
                    circuitsListNewCircuits = em.merge(circuitsListNewCircuits);
                    if (oldIdSectionOfCircuitsListNewCircuits != null && !oldIdSectionOfCircuitsListNewCircuits.equals(sections)) {
                        oldIdSectionOfCircuitsListNewCircuits.getCircuitsList().remove(circuitsListNewCircuits);
                        oldIdSectionOfCircuitsListNewCircuits = em.merge(oldIdSectionOfCircuitsListNewCircuits);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = sections.getId();
                if (findSections(id) == null) {
                    throw new NonexistentEntityException("The sections with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Sections sections;
            try {
                sections = em.getReference(Sections.class, id);
                sections.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The sections with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            List<ElectionsSections> electionsSectionsListOrphanCheck = sections.getElectionsSectionsList();
            for (ElectionsSections electionsSectionsListOrphanCheckElectionsSections : electionsSectionsListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Sections (" + sections + ") cannot be destroyed since the ElectionsSections " + electionsSectionsListOrphanCheckElectionsSections + " in its electionsSectionsList field has a non-nullable idSection field.");
            }
            List<UserSections> userSectionsListOrphanCheck = sections.getUserSectionsList();
            for (UserSections userSectionsListOrphanCheckUserSections : userSectionsListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Sections (" + sections + ") cannot be destroyed since the UserSections " + userSectionsListOrphanCheckUserSections + " in its userSectionsList field has a non-nullable idSection field.");
            }
            List<Circuits> circuitsListOrphanCheck = sections.getCircuitsList();
            for (Circuits circuitsListOrphanCheckCircuits : circuitsListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Sections (" + sections + ") cannot be destroyed since the Circuits " + circuitsListOrphanCheckCircuits + " in its circuitsList field has a non-nullable idSection field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            Regions idRegion = sections.getIdRegion();
            if (idRegion != null) {
                idRegion.getSectionsList().remove(sections);
                idRegion = em.merge(idRegion);
            }
            em.remove(sections);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Sections> findSectionsEntities() {
        return findSectionsEntities(true, -1, -1);
    }

    public List<Sections> findSectionsEntities(int maxResults, int firstResult) {
        return findSectionsEntities(false, maxResults, firstResult);
    }

    private List<Sections> findSectionsEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Sections.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Sections findSections(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Sections.class, id);
        } finally {
            em.close();
        }
    }

    public int getSectionsCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Sections> rt = cq.from(Sections.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
