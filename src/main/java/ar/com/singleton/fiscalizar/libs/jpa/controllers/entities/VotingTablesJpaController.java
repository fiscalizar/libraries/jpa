/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ar.com.singleton.fiscalizar.libs.jpa.controllers.entities;

import ar.com.singleton.fiscalizar.libs.jpa.controllers.exceptions.IllegalOrphanException;
import ar.com.singleton.fiscalizar.libs.jpa.controllers.exceptions.NonexistentEntityException;
import ar.com.singleton.fiscalizar.libs.jpa.controllers.exceptions.PreexistingEntityException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import ar.com.singleton.fiscalizar.libs.jpa.entities.Institutions;
import ar.com.singleton.fiscalizar.libs.jpa.entities.relations.UserVotingTables;
import java.util.ArrayList;
import java.util.List;
import ar.com.singleton.fiscalizar.libs.jpa.entities.Polls;
import ar.com.singleton.fiscalizar.libs.jpa.entities.VotingTables;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author pato
 */
public class VotingTablesJpaController implements Serializable {

    public VotingTablesJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(VotingTables votingTables) throws PreexistingEntityException, Exception {
        if (votingTables.getUserVotingTablesList() == null) {
            votingTables.setUserVotingTablesList(new ArrayList<UserVotingTables>());
        }
        if (votingTables.getPollsList() == null) {
            votingTables.setPollsList(new ArrayList<Polls>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Institutions idInstitution = votingTables.getIdInstitution();
            if (idInstitution != null) {
                idInstitution = em.getReference(idInstitution.getClass(), idInstitution.getId());
                votingTables.setIdInstitution(idInstitution);
            }
            List<UserVotingTables> attachedUserVotingTablesList = new ArrayList<UserVotingTables>();
            for (UserVotingTables userVotingTablesListUserVotingTablesToAttach : votingTables.getUserVotingTablesList()) {
                userVotingTablesListUserVotingTablesToAttach = em.getReference(userVotingTablesListUserVotingTablesToAttach.getClass(), userVotingTablesListUserVotingTablesToAttach.getUserVotingTablesPK());
                attachedUserVotingTablesList.add(userVotingTablesListUserVotingTablesToAttach);
            }
            votingTables.setUserVotingTablesList(attachedUserVotingTablesList);
            List<Polls> attachedPollsList = new ArrayList<Polls>();
            for (Polls pollsListPollsToAttach : votingTables.getPollsList()) {
                pollsListPollsToAttach = em.getReference(pollsListPollsToAttach.getClass(), pollsListPollsToAttach.getId());
                attachedPollsList.add(pollsListPollsToAttach);
            }
            votingTables.setPollsList(attachedPollsList);
            em.persist(votingTables);
            if (idInstitution != null) {
                idInstitution.getVotingTablesList().add(votingTables);
                idInstitution = em.merge(idInstitution);
            }
            for (UserVotingTables userVotingTablesListUserVotingTables : votingTables.getUserVotingTablesList()) {
                VotingTables oldIdVotingTableOfUserVotingTablesListUserVotingTables = userVotingTablesListUserVotingTables.getIdVotingTable();
                userVotingTablesListUserVotingTables.setIdVotingTable(votingTables);
                userVotingTablesListUserVotingTables = em.merge(userVotingTablesListUserVotingTables);
                if (oldIdVotingTableOfUserVotingTablesListUserVotingTables != null) {
                    oldIdVotingTableOfUserVotingTablesListUserVotingTables.getUserVotingTablesList().remove(userVotingTablesListUserVotingTables);
                    oldIdVotingTableOfUserVotingTablesListUserVotingTables = em.merge(oldIdVotingTableOfUserVotingTablesListUserVotingTables);
                }
            }
            for (Polls pollsListPolls : votingTables.getPollsList()) {
                VotingTables oldIdVotingTableOfPollsListPolls = pollsListPolls.getIdVotingTable();
                pollsListPolls.setIdVotingTable(votingTables);
                pollsListPolls = em.merge(pollsListPolls);
                if (oldIdVotingTableOfPollsListPolls != null) {
                    oldIdVotingTableOfPollsListPolls.getPollsList().remove(pollsListPolls);
                    oldIdVotingTableOfPollsListPolls = em.merge(oldIdVotingTableOfPollsListPolls);
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findVotingTables(votingTables.getId()) != null) {
                throw new PreexistingEntityException("VotingTables " + votingTables + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(VotingTables votingTables) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            VotingTables persistentVotingTables = em.find(VotingTables.class, votingTables.getId());
            Institutions idInstitutionOld = persistentVotingTables.getIdInstitution();
            Institutions idInstitutionNew = votingTables.getIdInstitution();
            List<UserVotingTables> userVotingTablesListOld = persistentVotingTables.getUserVotingTablesList();
            List<UserVotingTables> userVotingTablesListNew = votingTables.getUserVotingTablesList();
            List<Polls> pollsListOld = persistentVotingTables.getPollsList();
            List<Polls> pollsListNew = votingTables.getPollsList();
            List<String> illegalOrphanMessages = null;
            for (UserVotingTables userVotingTablesListOldUserVotingTables : userVotingTablesListOld) {
                if (!userVotingTablesListNew.contains(userVotingTablesListOldUserVotingTables)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain UserVotingTables " + userVotingTablesListOldUserVotingTables + " since its idVotingTable field is not nullable.");
                }
            }
            for (Polls pollsListOldPolls : pollsListOld) {
                if (!pollsListNew.contains(pollsListOldPolls)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Polls " + pollsListOldPolls + " since its idVotingTable field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            if (idInstitutionNew != null) {
                idInstitutionNew = em.getReference(idInstitutionNew.getClass(), idInstitutionNew.getId());
                votingTables.setIdInstitution(idInstitutionNew);
            }
            List<UserVotingTables> attachedUserVotingTablesListNew = new ArrayList<UserVotingTables>();
            for (UserVotingTables userVotingTablesListNewUserVotingTablesToAttach : userVotingTablesListNew) {
                userVotingTablesListNewUserVotingTablesToAttach = em.getReference(userVotingTablesListNewUserVotingTablesToAttach.getClass(), userVotingTablesListNewUserVotingTablesToAttach.getUserVotingTablesPK());
                attachedUserVotingTablesListNew.add(userVotingTablesListNewUserVotingTablesToAttach);
            }
            userVotingTablesListNew = attachedUserVotingTablesListNew;
            votingTables.setUserVotingTablesList(userVotingTablesListNew);
            List<Polls> attachedPollsListNew = new ArrayList<Polls>();
            for (Polls pollsListNewPollsToAttach : pollsListNew) {
                pollsListNewPollsToAttach = em.getReference(pollsListNewPollsToAttach.getClass(), pollsListNewPollsToAttach.getId());
                attachedPollsListNew.add(pollsListNewPollsToAttach);
            }
            pollsListNew = attachedPollsListNew;
            votingTables.setPollsList(pollsListNew);
            votingTables = em.merge(votingTables);
            if (idInstitutionOld != null && !idInstitutionOld.equals(idInstitutionNew)) {
                idInstitutionOld.getVotingTablesList().remove(votingTables);
                idInstitutionOld = em.merge(idInstitutionOld);
            }
            if (idInstitutionNew != null && !idInstitutionNew.equals(idInstitutionOld)) {
                idInstitutionNew.getVotingTablesList().add(votingTables);
                idInstitutionNew = em.merge(idInstitutionNew);
            }
            for (UserVotingTables userVotingTablesListNewUserVotingTables : userVotingTablesListNew) {
                if (!userVotingTablesListOld.contains(userVotingTablesListNewUserVotingTables)) {
                    VotingTables oldIdVotingTableOfUserVotingTablesListNewUserVotingTables = userVotingTablesListNewUserVotingTables.getIdVotingTable();
                    userVotingTablesListNewUserVotingTables.setIdVotingTable(votingTables);
                    userVotingTablesListNewUserVotingTables = em.merge(userVotingTablesListNewUserVotingTables);
                    if (oldIdVotingTableOfUserVotingTablesListNewUserVotingTables != null && !oldIdVotingTableOfUserVotingTablesListNewUserVotingTables.equals(votingTables)) {
                        oldIdVotingTableOfUserVotingTablesListNewUserVotingTables.getUserVotingTablesList().remove(userVotingTablesListNewUserVotingTables);
                        oldIdVotingTableOfUserVotingTablesListNewUserVotingTables = em.merge(oldIdVotingTableOfUserVotingTablesListNewUserVotingTables);
                    }
                }
            }
            for (Polls pollsListNewPolls : pollsListNew) {
                if (!pollsListOld.contains(pollsListNewPolls)) {
                    VotingTables oldIdVotingTableOfPollsListNewPolls = pollsListNewPolls.getIdVotingTable();
                    pollsListNewPolls.setIdVotingTable(votingTables);
                    pollsListNewPolls = em.merge(pollsListNewPolls);
                    if (oldIdVotingTableOfPollsListNewPolls != null && !oldIdVotingTableOfPollsListNewPolls.equals(votingTables)) {
                        oldIdVotingTableOfPollsListNewPolls.getPollsList().remove(pollsListNewPolls);
                        oldIdVotingTableOfPollsListNewPolls = em.merge(oldIdVotingTableOfPollsListNewPolls);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = votingTables.getId();
                if (findVotingTables(id) == null) {
                    throw new NonexistentEntityException("The votingTables with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            VotingTables votingTables;
            try {
                votingTables = em.getReference(VotingTables.class, id);
                votingTables.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The votingTables with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            List<UserVotingTables> userVotingTablesListOrphanCheck = votingTables.getUserVotingTablesList();
            for (UserVotingTables userVotingTablesListOrphanCheckUserVotingTables : userVotingTablesListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This VotingTables (" + votingTables + ") cannot be destroyed since the UserVotingTables " + userVotingTablesListOrphanCheckUserVotingTables + " in its userVotingTablesList field has a non-nullable idVotingTable field.");
            }
            List<Polls> pollsListOrphanCheck = votingTables.getPollsList();
            for (Polls pollsListOrphanCheckPolls : pollsListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This VotingTables (" + votingTables + ") cannot be destroyed since the Polls " + pollsListOrphanCheckPolls + " in its pollsList field has a non-nullable idVotingTable field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            Institutions idInstitution = votingTables.getIdInstitution();
            if (idInstitution != null) {
                idInstitution.getVotingTablesList().remove(votingTables);
                idInstitution = em.merge(idInstitution);
            }
            em.remove(votingTables);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<VotingTables> findVotingTablesEntities() {
        return findVotingTablesEntities(true, -1, -1);
    }

    public List<VotingTables> findVotingTablesEntities(int maxResults, int firstResult) {
        return findVotingTablesEntities(false, maxResults, firstResult);
    }

    private List<VotingTables> findVotingTablesEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(VotingTables.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public VotingTables findVotingTables(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(VotingTables.class, id);
        } finally {
            em.close();
        }
    }

    public int getVotingTablesCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<VotingTables> rt = cq.from(VotingTables.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
