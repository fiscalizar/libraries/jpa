/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ar.com.singleton.fiscalizar.libs.jpa.entities.relations;

import ar.com.singleton.fiscalizar.libs.jpa.entities.Sections;
import ar.com.singleton.fiscalizar.libs.jpa.entities.Users;
import ar.com.singleton.fiscalizar.libs.jpa.entities.keys.UserSectionsPK;
import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author pato
 */
@Entity
@Table(name = "UserSections", catalog = "", schema = "")
@XmlRootElement
public class UserSections implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected UserSectionsPK userSectionsPK;
    @Basic(optional = false)
    @Column(name = "createdAt")
    private String createdAt;
    @Column(name = "updatedAt")
    private String updatedAt;
    @Basic(optional = false)
    @Column(name = "deleted")
    private int deleted;
    @PrimaryKeyJoinColumn(name = "idUser", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Users idUser;
    @PrimaryKeyJoinColumn(name = "idSection", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Sections idSection;

    public UserSections() {
    }

    public UserSections(UserSectionsPK userSectionsPK) {
        this.userSectionsPK = userSectionsPK;
    }

    public UserSections(UserSectionsPK userSectionsPK, String createdAt, int deleted) {
        this.userSectionsPK = userSectionsPK;
        this.createdAt = createdAt;
        this.deleted = deleted;
    }

    public UserSectionsPK getUserSectionsPK() {
        return userSectionsPK;
    }

    public void setUserSectionsPK(UserSectionsPK userSectionsPK) {
        this.userSectionsPK = userSectionsPK;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public int getDeleted() {
        return deleted;
    }

    public void setDeleted(int deleted) {
        this.deleted = deleted;
    }

    public Users getIdUser() {
        return idUser;
    }

    public void setIdUser(Users idUser) {
        this.idUser = idUser;
    }

    public Sections getIdSection() {
        return idSection;
    }

    public void setIdSection(Sections idSection) {
        this.idSection = idSection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (userSectionsPK != null ? userSectionsPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UserSections)) {
            return false;
        }
        UserSections other = (UserSections) object;
        if ((this.userSectionsPK == null && other.userSectionsPK != null) || (this.userSectionsPK != null && !this.userSectionsPK.equals(other.userSectionsPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ar.com.singleton.app.fiscalizar.database.UserSections[ userSectionsPK=" + userSectionsPK + " ]";
    }
    
}
