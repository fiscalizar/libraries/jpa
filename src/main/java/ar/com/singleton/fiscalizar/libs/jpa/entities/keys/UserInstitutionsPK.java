/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ar.com.singleton.fiscalizar.libs.jpa.entities.keys;

import ar.com.singleton.fiscalizar.libs.jpa.entities.Institutions;
import ar.com.singleton.fiscalizar.libs.jpa.entities.Users;
import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Basic;
import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 *
 * @author pato
 */
@Embeddable
public class UserInstitutionsPK implements Serializable {

    @Basic
    @ManyToOne
    @JoinColumn(name = "idUser", insertable = false, updatable = false)
    private Users idUser;
    
    @Basic
    @ManyToOne
    @JoinColumn(name = "idInstitution",insertable = false, updatable = false)
    private Institutions idInstitution;
    
    public UserInstitutionsPK(Users idUser, Institutions idInstitution) {
        this.idInstitution = idInstitution;
        this.idUser = idUser;
    }

    public UserInstitutionsPK() {
    }

    public Users getIdUser() {
        return idUser;
    }

    public void setIdUser(Users idUser) {
        this.idUser = idUser;
    }

    public Institutions getIdInstitution() {
        return idInstitution;
    }

    public void setIdInstitution(Institutions idInstitution) {
        this.idInstitution = idInstitution;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 67 * hash + Objects.hashCode(this.idUser);
        hash = 67 * hash + Objects.hashCode(this.idInstitution);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final UserInstitutionsPK other = (UserInstitutionsPK) obj;
        if (!Objects.equals(this.idUser, other.idUser)) {
            return false;
        }
        return Objects.equals(this.idInstitution, other.idInstitution);
    }
    
}
