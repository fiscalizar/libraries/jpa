/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ar.com.singleton.fiscalizar.libs.jpa.controllers.entities.relations;

import ar.com.singleton.fiscalizar.libs.jpa.controllers.exceptions.NonexistentEntityException;
import ar.com.singleton.fiscalizar.libs.jpa.controllers.exceptions.PreexistingEntityException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import ar.com.singleton.fiscalizar.libs.jpa.entities.Users;
import ar.com.singleton.fiscalizar.libs.jpa.entities.Institutions;
import ar.com.singleton.fiscalizar.libs.jpa.entities.keys.UserInstitutionsPK;
import ar.com.singleton.fiscalizar.libs.jpa.entities.relations.UserInstitutions;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author pato
 */
public class UserInstitutionsJpaController implements Serializable {

    public UserInstitutionsJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(UserInstitutions userInstitutions) throws PreexistingEntityException, Exception {
        if (userInstitutions.getUserInstitutionsPK() == null) {
            userInstitutions.setUserInstitutionsPK(new UserInstitutionsPK());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Users idUser = userInstitutions.getIdUser();
            if (idUser != null) {
                idUser = em.getReference(idUser.getClass(), idUser.getId());
                userInstitutions.setIdUser(idUser);
            }
            Institutions idUserInstitution = userInstitutions.getIdUserInstitution();
            if (idUserInstitution != null) {
                idUserInstitution = em.getReference(idUserInstitution.getClass(), idUserInstitution.getId());
                userInstitutions.setIdUserInstitution(idUserInstitution);
            }
            em.persist(userInstitutions);
            if (idUser != null) {
                idUser.getUserInstitutionsList().add(userInstitutions);
                idUser = em.merge(idUser);
            }
            if (idUserInstitution != null) {
                idUserInstitution.getUserInstitutionsList().add(userInstitutions);
                idUserInstitution = em.merge(idUserInstitution);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findUserInstitutions(userInstitutions.getUserInstitutionsPK()) != null) {
                throw new PreexistingEntityException("UserInstitutions " + userInstitutions + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(UserInstitutions userInstitutions) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            UserInstitutions persistentUserInstitutions = em.find(UserInstitutions.class, userInstitutions.getUserInstitutionsPK());
            Users idUserOld = persistentUserInstitutions.getIdUser();
            Users idUserNew = userInstitutions.getIdUser();
            Institutions idUserInstitutionOld = persistentUserInstitutions.getIdUserInstitution();
            Institutions idUserInstitutionNew = userInstitutions.getIdUserInstitution();
            if (idUserNew != null) {
                idUserNew = em.getReference(idUserNew.getClass(), idUserNew.getId());
                userInstitutions.setIdUser(idUserNew);
            }
            if (idUserInstitutionNew != null) {
                idUserInstitutionNew = em.getReference(idUserInstitutionNew.getClass(), idUserInstitutionNew.getId());
                userInstitutions.setIdUserInstitution(idUserInstitutionNew);
            }
            userInstitutions = em.merge(userInstitutions);
            if (idUserOld != null && !idUserOld.equals(idUserNew)) {
                idUserOld.getUserInstitutionsList().remove(userInstitutions);
                idUserOld = em.merge(idUserOld);
            }
            if (idUserNew != null && !idUserNew.equals(idUserOld)) {
                idUserNew.getUserInstitutionsList().add(userInstitutions);
                idUserNew = em.merge(idUserNew);
            }
            if (idUserInstitutionOld != null && !idUserInstitutionOld.equals(idUserInstitutionNew)) {
                idUserInstitutionOld.getUserInstitutionsList().remove(userInstitutions);
                idUserInstitutionOld = em.merge(idUserInstitutionOld);
            }
            if (idUserInstitutionNew != null && !idUserInstitutionNew.equals(idUserInstitutionOld)) {
                idUserInstitutionNew.getUserInstitutionsList().add(userInstitutions);
                idUserInstitutionNew = em.merge(idUserInstitutionNew);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                UserInstitutionsPK id = userInstitutions.getUserInstitutionsPK();
                if (findUserInstitutions(id) == null) {
                    throw new NonexistentEntityException("The userInstitutions with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(UserInstitutionsPK id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            UserInstitutions userInstitutions;
            try {
                userInstitutions = em.getReference(UserInstitutions.class, id);
                userInstitutions.getUserInstitutionsPK();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The userInstitutions with id " + id + " no longer exists.", enfe);
            }
            Users idUser = userInstitutions.getIdUser();
            if (idUser != null) {
                idUser.getUserInstitutionsList().remove(userInstitutions);
                idUser = em.merge(idUser);
            }
            Institutions idUserInstitution = userInstitutions.getIdUserInstitution();
            if (idUserInstitution != null) {
                idUserInstitution.getUserInstitutionsList().remove(userInstitutions);
                idUserInstitution = em.merge(idUserInstitution);
            }
            em.remove(userInstitutions);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<UserInstitutions> findUserInstitutionsEntities() {
        return findUserInstitutionsEntities(true, -1, -1);
    }

    public List<UserInstitutions> findUserInstitutionsEntities(int maxResults, int firstResult) {
        return findUserInstitutionsEntities(false, maxResults, firstResult);
    }

    private List<UserInstitutions> findUserInstitutionsEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(UserInstitutions.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public UserInstitutions findUserInstitutions(UserInstitutionsPK id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(UserInstitutions.class, id);
        } finally {
            em.close();
        }
    }

    public int getUserInstitutionsCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<UserInstitutions> rt = cq.from(UserInstitutions.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
