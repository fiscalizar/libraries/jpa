/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ar.com.singleton.fiscalizar.libs.jpa.controllers.entities.relations;

import ar.com.singleton.fiscalizar.libs.jpa.controllers.exceptions.NonexistentEntityException;
import ar.com.singleton.fiscalizar.libs.jpa.controllers.exceptions.PreexistingEntityException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import ar.com.singleton.fiscalizar.libs.jpa.entities.Elections;
import ar.com.singleton.fiscalizar.libs.jpa.entities.Regions;
import ar.com.singleton.fiscalizar.libs.jpa.entities.keys.ElectionsRegionsPK;
import ar.com.singleton.fiscalizar.libs.jpa.entities.relations.ElectionsRegions;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author pato
 */
public class ElectionsRegionsJpaController implements Serializable {

    public ElectionsRegionsJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(ElectionsRegions electionsRegions) throws PreexistingEntityException, Exception {
        if (electionsRegions.getElectionsRegionsPK() == null) {
            electionsRegions.setElectionsRegionsPK(new ElectionsRegionsPK());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Elections idElection = electionsRegions.getIdElection();
            if (idElection != null) {
                idElection = em.getReference(idElection.getClass(), idElection.getId());
                electionsRegions.setIdElection(idElection);
            }
            Regions idRegion = electionsRegions.getIdRegion();
            if (idRegion != null) {
                idRegion = em.getReference(idRegion.getClass(), idRegion.getId());
                electionsRegions.setIdRegion(idRegion);
            }
            em.persist(electionsRegions);
            if (idElection != null) {
                idElection.getElectionsRegionsList().add(electionsRegions);
                idElection = em.merge(idElection);
            }
            if (idRegion != null) {
                idRegion.getElectionsRegionsList().add(electionsRegions);
                idRegion = em.merge(idRegion);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findElectionsRegions(electionsRegions.getElectionsRegionsPK()) != null) {
                throw new PreexistingEntityException("ElectionsRegions " + electionsRegions + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(ElectionsRegions electionsRegions) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            ElectionsRegions persistentElectionsRegions = em.find(ElectionsRegions.class, electionsRegions.getElectionsRegionsPK());
            Elections idElectionOld = persistentElectionsRegions.getIdElection();
            Elections idElectionNew = electionsRegions.getIdElection();
            Regions idRegionOld = persistentElectionsRegions.getIdRegion();
            Regions idRegionNew = electionsRegions.getIdRegion();
            if (idElectionNew != null) {
                idElectionNew = em.getReference(idElectionNew.getClass(), idElectionNew.getId());
                electionsRegions.setIdElection(idElectionNew);
            }
            if (idRegionNew != null) {
                idRegionNew = em.getReference(idRegionNew.getClass(), idRegionNew.getId());
                electionsRegions.setIdRegion(idRegionNew);
            }
            electionsRegions = em.merge(electionsRegions);
            if (idElectionOld != null && !idElectionOld.equals(idElectionNew)) {
                idElectionOld.getElectionsRegionsList().remove(electionsRegions);
                idElectionOld = em.merge(idElectionOld);
            }
            if (idElectionNew != null && !idElectionNew.equals(idElectionOld)) {
                idElectionNew.getElectionsRegionsList().add(electionsRegions);
                idElectionNew = em.merge(idElectionNew);
            }
            if (idRegionOld != null && !idRegionOld.equals(idRegionNew)) {
                idRegionOld.getElectionsRegionsList().remove(electionsRegions);
                idRegionOld = em.merge(idRegionOld);
            }
            if (idRegionNew != null && !idRegionNew.equals(idRegionOld)) {
                idRegionNew.getElectionsRegionsList().add(electionsRegions);
                idRegionNew = em.merge(idRegionNew);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                ElectionsRegionsPK id = electionsRegions.getElectionsRegionsPK();
                if (findElectionsRegions(id) == null) {
                    throw new NonexistentEntityException("The electionsRegions with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(ElectionsRegionsPK id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            ElectionsRegions electionsRegions;
            try {
                electionsRegions = em.getReference(ElectionsRegions.class, id);
                electionsRegions.getElectionsRegionsPK();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The electionsRegions with id " + id + " no longer exists.", enfe);
            }
            Elections idElection = electionsRegions.getIdElection();
            if (idElection != null) {
                idElection.getElectionsRegionsList().remove(electionsRegions);
                idElection = em.merge(idElection);
            }
            Regions idRegion = electionsRegions.getIdRegion();
            if (idRegion != null) {
                idRegion.getElectionsRegionsList().remove(electionsRegions);
                idRegion = em.merge(idRegion);
            }
            em.remove(electionsRegions);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<ElectionsRegions> findElectionsRegionsEntities() {
        return findElectionsRegionsEntities(true, -1, -1);
    }

    public List<ElectionsRegions> findElectionsRegionsEntities(int maxResults, int firstResult) {
        return findElectionsRegionsEntities(false, maxResults, firstResult);
    }

    private List<ElectionsRegions> findElectionsRegionsEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(ElectionsRegions.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public ElectionsRegions findElectionsRegions(ElectionsRegionsPK id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(ElectionsRegions.class, id);
        } finally {
            em.close();
        }
    }

    public int getElectionsRegionsCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<ElectionsRegions> rt = cq.from(ElectionsRegions.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
