/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ar.com.singleton.fiscalizar.libs.jpa.views.results.votes;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author pato
 */
@MappedSuperclass
@Table(name = "R_VotesResultsByRegionsComplete", catalog = "", schema = "")
@XmlRootElement
public class RVotesResultsByRegionsComplete implements Serializable {

    private static final long serialVersionUID = 1L;
    @Column(name = "idElection")
    private Integer idElection;
    @Column(name = "idCategory")
    private Integer idCategory;
    @Column(name = "idParty")
    private Integer idParty;
    @Column(name = "idRegion")
    private Integer idRegion;
    @Column(name = "votedTables")
    private String votedTables;
    @Column(name = "votes")
    private String votes;
    @Column(name = "votedElectors")
    private String votedElectors;
    @Column(name = "partyPercentage")
    private String partyPercentage;

    public RVotesResultsByRegionsComplete() {
    }

    public Integer getIdElection() {
        return idElection;
    }

    public void setIdElection(Integer idElection) {
        this.idElection = idElection;
    }

    public Integer getIdCategory() {
        return idCategory;
    }

    public void setIdCategory(Integer idCategory) {
        this.idCategory = idCategory;
    }

    public Integer getIdParty() {
        return idParty;
    }

    public void setIdParty(Integer idParty) {
        this.idParty = idParty;
    }

    public Integer getIdRegion() {
        return idRegion;
    }

    public void setIdRegion(Integer idRegion) {
        this.idRegion = idRegion;
    }

    public String getVotedTables() {
        return votedTables;
    }

    public void setVotedTables(String votedTables) {
        this.votedTables = votedTables;
    }

    public String getVotes() {
        return votes;
    }

    public void setVotes(String votes) {
        this.votes = votes;
    }

    public String getVotedElectors() {
        return votedElectors;
    }

    public void setVotedElectors(String votedElectors) {
        this.votedElectors = votedElectors;
    }

    public String getPartyPercentage() {
        return partyPercentage;
    }

    public void setPartyPercentage(String partyPercentage) {
        this.partyPercentage = partyPercentage;
    }
    
}
