/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ar.com.singleton.fiscalizar.libs.jpa.entities;

import ar.com.singleton.fiscalizar.libs.jpa.entities.relations.UserSections;
import ar.com.singleton.fiscalizar.libs.jpa.entities.relations.ElectionsSections;
import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author pato
 */
@Entity
@Table(name = "Sections", catalog = "", schema = "")
@XmlRootElement
public class Sections implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "name")
    private String name;
    @Basic(optional = false)
    @Column(name = "createdAt")
    private String createdAt;
    @Column(name = "updatedAt")
    private String updatedAt;
    @Basic(optional = false)
    @Column(name = "deleted")
    private int deleted;
    @JoinColumn(name = "idRegion", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Regions idRegion;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idSection")
    private List<ElectionsSections> electionsSectionsList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idSection")
    private List<UserSections> userSectionsList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idSection")
    private List<Circuits> circuitsList;

    public Sections() {
    }

    public Sections(Integer id) {
        this.id = id;
    }

    public Sections(Integer id, String name, String createdAt, int deleted) {
        this.id = id;
        this.name = name;
        this.createdAt = createdAt;
        this.deleted = deleted;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public int getDeleted() {
        return deleted;
    }

    public void setDeleted(int deleted) {
        this.deleted = deleted;
    }

    public Regions getIdRegion() {
        return idRegion;
    }

    public void setIdRegion(Regions idRegion) {
        this.idRegion = idRegion;
    }

    @XmlTransient
    public List<ElectionsSections> getElectionsSectionsList() {
        return electionsSectionsList;
    }

    public void setElectionsSectionsList(List<ElectionsSections> electionsSectionsList) {
        this.electionsSectionsList = electionsSectionsList;
    }

    @XmlTransient
    public List<UserSections> getUserSectionsList() {
        return userSectionsList;
    }

    public void setUserSectionsList(List<UserSections> userSectionsList) {
        this.userSectionsList = userSectionsList;
    }

    @XmlTransient
    public List<Circuits> getCircuitsList() {
        return circuitsList;
    }

    public void setCircuitsList(List<Circuits> circuitsList) {
        this.circuitsList = circuitsList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Sections)) {
            return false;
        }
        Sections other = (Sections) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ar.com.singleton.app.fiscalizar.database.Sections[ id=" + id + " ]";
    }
    
}
