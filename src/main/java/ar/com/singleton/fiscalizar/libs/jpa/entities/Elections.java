/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ar.com.singleton.fiscalizar.libs.jpa.entities;

import ar.com.singleton.fiscalizar.libs.jpa.entities.relations.UserElections;
import ar.com.singleton.fiscalizar.libs.jpa.entities.relations.ElectionsRegions;
import ar.com.singleton.fiscalizar.libs.jpa.entities.relations.ElectionsCategories;
import ar.com.singleton.fiscalizar.libs.jpa.entities.relations.ElectionsSections;
import ar.com.singleton.fiscalizar.libs.jpa.entities.relations.ElectionsParties;
import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author pato
 */
@Entity
@Table(name = "Elections", catalog = "", schema = "")
@XmlRootElement
public class Elections implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "name")
    private String name;
    @Basic(optional = false)
    @Column(name = "year")
    private String year;
    @Basic(optional = false)
    @Column(name = "createdAt")
    private String createdAt;
    @Column(name = "updatedAt")
    private String updatedAt;
    @Basic(optional = false)
    @Column(name = "deleted")
    private int deleted;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idElection")
    private List<ElectionsSections> electionsSectionsList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idElection")
    private List<UserElections> userElectionsList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idElection")
    private List<ElectionsCategories> electionsCategoriesList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idElection")
    private List<Polls> pollsList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idElection")
    private List<ElectionsRegions> electionsRegionsList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idElection")
    private List<ElectionsParties> electionsPartiesList;

    public Elections() {
    }

    public Elections(Integer id) {
        this.id = id;
    }

    public Elections(Integer id, String name, String year, String createdAt, int deleted) {
        this.id = id;
        this.name = name;
        this.year = year;
        this.createdAt = createdAt;
        this.deleted = deleted;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public int getDeleted() {
        return deleted;
    }

    public void setDeleted(int deleted) {
        this.deleted = deleted;
    }

    @XmlTransient
    public List<ElectionsSections> getElectionsSectionsList() {
        return electionsSectionsList;
    }

    public void setElectionsSectionsList(List<ElectionsSections> electionsSectionsList) {
        this.electionsSectionsList = electionsSectionsList;
    }

    @XmlTransient
    public List<UserElections> getUserElectionsList() {
        return userElectionsList;
    }

    public void setUserElectionsList(List<UserElections> userElectionsList) {
        this.userElectionsList = userElectionsList;
    }

    @XmlTransient
    public List<ElectionsCategories> getElectionsCategoriesList() {
        return electionsCategoriesList;
    }

    public void setElectionsCategoriesList(List<ElectionsCategories> electionsCategoriesList) {
        this.electionsCategoriesList = electionsCategoriesList;
    }

    @XmlTransient
    public List<Polls> getPollsList() {
        return pollsList;
    }

    public void setPollsList(List<Polls> pollsList) {
        this.pollsList = pollsList;
    }

    @XmlTransient
    public List<ElectionsRegions> getElectionsRegionsList() {
        return electionsRegionsList;
    }

    public void setElectionsRegionsList(List<ElectionsRegions> electionsRegionsList) {
        this.electionsRegionsList = electionsRegionsList;
    }

    @XmlTransient
    public List<ElectionsParties> getElectionsPartiesList() {
        return electionsPartiesList;
    }

    public void setElectionsPartiesList(List<ElectionsParties> electionsPartiesList) {
        this.electionsPartiesList = electionsPartiesList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Elections)) {
            return false;
        }
        Elections other = (Elections) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ar.com.singleton.app.fiscalizar.database.Elections[ id=" + id + " ]";
    }
    
}
