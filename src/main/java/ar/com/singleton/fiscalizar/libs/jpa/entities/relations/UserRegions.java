/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ar.com.singleton.fiscalizar.libs.jpa.entities.relations;

import ar.com.singleton.fiscalizar.libs.jpa.entities.Regions;
import ar.com.singleton.fiscalizar.libs.jpa.entities.Users;
import ar.com.singleton.fiscalizar.libs.jpa.entities.keys.UserRegionsPK;
import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author pato
 */
@Entity
@Table(name = "UserRegions", catalog = "", schema = "")
@XmlRootElement
public class UserRegions implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected UserRegionsPK userRegionsPK;
    @Basic(optional = false)
    @Column(name = "createdAt")
    private String createdAt;
    @Column(name = "updatedAt")
    private String updatedAt;
    @Basic(optional = false)
    @Column(name = "deleted")
    private int deleted;
    @PrimaryKeyJoinColumn(name = "idUser", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Users idUser;
    @PrimaryKeyJoinColumn(name = "idRegion", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Regions idRegion;

    public UserRegions() {
    }

    public UserRegions(UserRegionsPK userRegionsPK) {
        this.userRegionsPK = userRegionsPK;
    }

    public UserRegions(UserRegionsPK userRegionsPK, String createdAt, int deleted) {
        this.userRegionsPK = userRegionsPK;
        this.createdAt = createdAt;
        this.deleted = deleted;
    }

    public UserRegionsPK getUserRegionsPK() {
        return userRegionsPK;
    }

    public void setUserRegionsPK(UserRegionsPK userRegionsPK) {
        this.userRegionsPK = userRegionsPK;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public int getDeleted() {
        return deleted;
    }

    public void setDeleted(int deleted) {
        this.deleted = deleted;
    }

    public Users getIdUser() {
        return idUser;
    }

    public void setIdUser(Users idUser) {
        this.idUser = idUser;
    }

    public Regions getIdRegion() {
        return idRegion;
    }

    public void setIdRegion(Regions idRegion) {
        this.idRegion = idRegion;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (userRegionsPK != null ? userRegionsPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UserRegions)) {
            return false;
        }
        UserRegions other = (UserRegions) object;
        if ((this.userRegionsPK == null && other.userRegionsPK != null) || (this.userRegionsPK != null && !this.userRegionsPK.equals(other.userRegionsPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ar.com.singleton.app.fiscalizar.database.UserRegions[ userRegionsPK=" + userRegionsPK + " ]";
    }
    
}
