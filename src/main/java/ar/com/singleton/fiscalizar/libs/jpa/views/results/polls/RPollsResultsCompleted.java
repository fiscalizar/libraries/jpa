/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ar.com.singleton.fiscalizar.libs.jpa.views.results.polls;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author pato
 */
@MappedSuperclass
@Table(name = "R_PollsResultsCompleted", catalog = "", schema = "")
@XmlRootElement
public class RPollsResultsCompleted implements Serializable {

    private static final long serialVersionUID = 1L;
    @Column(name = "idVotingTable")
    private Integer idVotingTable;
    @Column(name = "idElection")
    private Integer idElection;
    @Column(name = "idCategory")
    private Integer idCategory;
    @Column(name = "idInstitution")
    private Integer idInstitution;

    public RPollsResultsCompleted() {
    }

    public Integer getIdVotingTable() {
        return idVotingTable;
    }

    public void setIdVotingTable(Integer idVotingTable) {
        this.idVotingTable = idVotingTable;
    }

    public Integer getIdElection() {
        return idElection;
    }

    public void setIdElection(Integer idElection) {
        this.idElection = idElection;
    }

    public Integer getIdCategory() {
        return idCategory;
    }

    public void setIdCategory(Integer idCategory) {
        this.idCategory = idCategory;
    }

    public Integer getIdInstitution() {
        return idInstitution;
    }

    public void setIdInstitution(Integer idInstitution) {
        this.idInstitution = idInstitution;
    }
    
}
