/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ar.com.singleton.fiscalizar.libs.jpa.controllers.entities.relations;

import ar.com.singleton.fiscalizar.libs.jpa.controllers.exceptions.NonexistentEntityException;
import ar.com.singleton.fiscalizar.libs.jpa.controllers.exceptions.PreexistingEntityException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import ar.com.singleton.fiscalizar.libs.jpa.entities.Users;
import ar.com.singleton.fiscalizar.libs.jpa.entities.Regions;
import ar.com.singleton.fiscalizar.libs.jpa.entities.keys.UserRegionsPK;
import ar.com.singleton.fiscalizar.libs.jpa.entities.relations.UserRegions;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author pato
 */
public class UserRegionsJpaController implements Serializable {

    public UserRegionsJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(UserRegions userRegions) throws PreexistingEntityException, Exception {
        if (userRegions.getUserRegionsPK() == null) {
            userRegions.setUserRegionsPK(new UserRegionsPK());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Users idUser = userRegions.getIdUser();
            if (idUser != null) {
                idUser = em.getReference(idUser.getClass(), idUser.getId());
                userRegions.setIdUser(idUser);
            }
            Regions idRegion = userRegions.getIdRegion();
            if (idRegion != null) {
                idRegion = em.getReference(idRegion.getClass(), idRegion.getId());
                userRegions.setIdRegion(idRegion);
            }
            em.persist(userRegions);
            if (idUser != null) {
                idUser.getUserRegionsList().add(userRegions);
                idUser = em.merge(idUser);
            }
            if (idRegion != null) {
                idRegion.getUserRegionsList().add(userRegions);
                idRegion = em.merge(idRegion);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findUserRegions(userRegions.getUserRegionsPK()) != null) {
                throw new PreexistingEntityException("UserRegions " + userRegions + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(UserRegions userRegions) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            UserRegions persistentUserRegions = em.find(UserRegions.class, userRegions.getUserRegionsPK());
            Users idUserOld = persistentUserRegions.getIdUser();
            Users idUserNew = userRegions.getIdUser();
            Regions idRegionOld = persistentUserRegions.getIdRegion();
            Regions idRegionNew = userRegions.getIdRegion();
            if (idUserNew != null) {
                idUserNew = em.getReference(idUserNew.getClass(), idUserNew.getId());
                userRegions.setIdUser(idUserNew);
            }
            if (idRegionNew != null) {
                idRegionNew = em.getReference(idRegionNew.getClass(), idRegionNew.getId());
                userRegions.setIdRegion(idRegionNew);
            }
            userRegions = em.merge(userRegions);
            if (idUserOld != null && !idUserOld.equals(idUserNew)) {
                idUserOld.getUserRegionsList().remove(userRegions);
                idUserOld = em.merge(idUserOld);
            }
            if (idUserNew != null && !idUserNew.equals(idUserOld)) {
                idUserNew.getUserRegionsList().add(userRegions);
                idUserNew = em.merge(idUserNew);
            }
            if (idRegionOld != null && !idRegionOld.equals(idRegionNew)) {
                idRegionOld.getUserRegionsList().remove(userRegions);
                idRegionOld = em.merge(idRegionOld);
            }
            if (idRegionNew != null && !idRegionNew.equals(idRegionOld)) {
                idRegionNew.getUserRegionsList().add(userRegions);
                idRegionNew = em.merge(idRegionNew);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                UserRegionsPK id = userRegions.getUserRegionsPK();
                if (findUserRegions(id) == null) {
                    throw new NonexistentEntityException("The userRegions with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(UserRegionsPK id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            UserRegions userRegions;
            try {
                userRegions = em.getReference(UserRegions.class, id);
                userRegions.getUserRegionsPK();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The userRegions with id " + id + " no longer exists.", enfe);
            }
            Users idUser = userRegions.getIdUser();
            if (idUser != null) {
                idUser.getUserRegionsList().remove(userRegions);
                idUser = em.merge(idUser);
            }
            Regions idRegion = userRegions.getIdRegion();
            if (idRegion != null) {
                idRegion.getUserRegionsList().remove(userRegions);
                idRegion = em.merge(idRegion);
            }
            em.remove(userRegions);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<UserRegions> findUserRegionsEntities() {
        return findUserRegionsEntities(true, -1, -1);
    }

    public List<UserRegions> findUserRegionsEntities(int maxResults, int firstResult) {
        return findUserRegionsEntities(false, maxResults, firstResult);
    }

    private List<UserRegions> findUserRegionsEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(UserRegions.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public UserRegions findUserRegions(UserRegionsPK id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(UserRegions.class, id);
        } finally {
            em.close();
        }
    }

    public int getUserRegionsCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<UserRegions> rt = cq.from(UserRegions.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
