/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ar.com.singleton.fiscalizar.libs.jpa.controllers.entities;

import ar.com.singleton.fiscalizar.libs.jpa.controllers.exceptions.IllegalOrphanException;
import ar.com.singleton.fiscalizar.libs.jpa.controllers.exceptions.NonexistentEntityException;
import ar.com.singleton.fiscalizar.libs.jpa.controllers.exceptions.PreexistingEntityException;
import ar.com.singleton.fiscalizar.libs.jpa.entities.Elections;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import ar.com.singleton.fiscalizar.libs.jpa.entities.relations.ElectionsSections;
import java.util.ArrayList;
import java.util.List;
import ar.com.singleton.fiscalizar.libs.jpa.entities.relations.UserElections;
import ar.com.singleton.fiscalizar.libs.jpa.entities.relations.ElectionsCategories;
import ar.com.singleton.fiscalizar.libs.jpa.entities.Polls;
import ar.com.singleton.fiscalizar.libs.jpa.entities.relations.ElectionsRegions;
import ar.com.singleton.fiscalizar.libs.jpa.entities.relations.ElectionsParties;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author pato
 */
public class ElectionsJpaController implements Serializable {

    public ElectionsJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Elections elections) throws PreexistingEntityException, Exception {
        if (elections.getElectionsSectionsList() == null) {
            elections.setElectionsSectionsList(new ArrayList<ElectionsSections>());
        }
        if (elections.getUserElectionsList() == null) {
            elections.setUserElectionsList(new ArrayList<UserElections>());
        }
        if (elections.getElectionsCategoriesList() == null) {
            elections.setElectionsCategoriesList(new ArrayList<ElectionsCategories>());
        }
        if (elections.getPollsList() == null) {
            elections.setPollsList(new ArrayList<Polls>());
        }
        if (elections.getElectionsRegionsList() == null) {
            elections.setElectionsRegionsList(new ArrayList<ElectionsRegions>());
        }
        if (elections.getElectionsPartiesList() == null) {
            elections.setElectionsPartiesList(new ArrayList<ElectionsParties>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            List<ElectionsSections> attachedElectionsSectionsList = new ArrayList<ElectionsSections>();
            for (ElectionsSections electionsSectionsListElectionsSectionsToAttach : elections.getElectionsSectionsList()) {
                electionsSectionsListElectionsSectionsToAttach = em.getReference(electionsSectionsListElectionsSectionsToAttach.getClass(), electionsSectionsListElectionsSectionsToAttach.getElectionsSectionsPK());
                attachedElectionsSectionsList.add(electionsSectionsListElectionsSectionsToAttach);
            }
            elections.setElectionsSectionsList(attachedElectionsSectionsList);
            List<UserElections> attachedUserElectionsList = new ArrayList<UserElections>();
            for (UserElections userElectionsListUserElectionsToAttach : elections.getUserElectionsList()) {
                userElectionsListUserElectionsToAttach = em.getReference(userElectionsListUserElectionsToAttach.getClass(), userElectionsListUserElectionsToAttach.getUserElectionsPK());
                attachedUserElectionsList.add(userElectionsListUserElectionsToAttach);
            }
            elections.setUserElectionsList(attachedUserElectionsList);
            List<ElectionsCategories> attachedElectionsCategoriesList = new ArrayList<ElectionsCategories>();
            for (ElectionsCategories electionsCategoriesListElectionsCategoriesToAttach : elections.getElectionsCategoriesList()) {
                electionsCategoriesListElectionsCategoriesToAttach = em.getReference(electionsCategoriesListElectionsCategoriesToAttach.getClass(), electionsCategoriesListElectionsCategoriesToAttach.getElectionsCategoriesPK());
                attachedElectionsCategoriesList.add(electionsCategoriesListElectionsCategoriesToAttach);
            }
            elections.setElectionsCategoriesList(attachedElectionsCategoriesList);
            List<Polls> attachedPollsList = new ArrayList<Polls>();
            for (Polls pollsListPollsToAttach : elections.getPollsList()) {
                pollsListPollsToAttach = em.getReference(pollsListPollsToAttach.getClass(), pollsListPollsToAttach.getId());
                attachedPollsList.add(pollsListPollsToAttach);
            }
            elections.setPollsList(attachedPollsList);
            List<ElectionsRegions> attachedElectionsRegionsList = new ArrayList<ElectionsRegions>();
            for (ElectionsRegions electionsRegionsListElectionsRegionsToAttach : elections.getElectionsRegionsList()) {
                electionsRegionsListElectionsRegionsToAttach = em.getReference(electionsRegionsListElectionsRegionsToAttach.getClass(), electionsRegionsListElectionsRegionsToAttach.getElectionsRegionsPK());
                attachedElectionsRegionsList.add(electionsRegionsListElectionsRegionsToAttach);
            }
            elections.setElectionsRegionsList(attachedElectionsRegionsList);
            List<ElectionsParties> attachedElectionsPartiesList = new ArrayList<ElectionsParties>();
            for (ElectionsParties electionsPartiesListElectionsPartiesToAttach : elections.getElectionsPartiesList()) {
                electionsPartiesListElectionsPartiesToAttach = em.getReference(electionsPartiesListElectionsPartiesToAttach.getClass(), electionsPartiesListElectionsPartiesToAttach.getElectionsPartiesPK());
                attachedElectionsPartiesList.add(electionsPartiesListElectionsPartiesToAttach);
            }
            elections.setElectionsPartiesList(attachedElectionsPartiesList);
            em.persist(elections);
            for (ElectionsSections electionsSectionsListElectionsSections : elections.getElectionsSectionsList()) {
                Elections oldIdElectionOfElectionsSectionsListElectionsSections = electionsSectionsListElectionsSections.getIdElection();
                electionsSectionsListElectionsSections.setIdElection(elections);
                electionsSectionsListElectionsSections = em.merge(electionsSectionsListElectionsSections);
                if (oldIdElectionOfElectionsSectionsListElectionsSections != null) {
                    oldIdElectionOfElectionsSectionsListElectionsSections.getElectionsSectionsList().remove(electionsSectionsListElectionsSections);
                    oldIdElectionOfElectionsSectionsListElectionsSections = em.merge(oldIdElectionOfElectionsSectionsListElectionsSections);
                }
            }
            for (UserElections userElectionsListUserElections : elections.getUserElectionsList()) {
                Elections oldIdElectionOfUserElectionsListUserElections = userElectionsListUserElections.getIdElection();
                userElectionsListUserElections.setIdElection(elections);
                userElectionsListUserElections = em.merge(userElectionsListUserElections);
                if (oldIdElectionOfUserElectionsListUserElections != null) {
                    oldIdElectionOfUserElectionsListUserElections.getUserElectionsList().remove(userElectionsListUserElections);
                    oldIdElectionOfUserElectionsListUserElections = em.merge(oldIdElectionOfUserElectionsListUserElections);
                }
            }
            for (ElectionsCategories electionsCategoriesListElectionsCategories : elections.getElectionsCategoriesList()) {
                Elections oldIdElectionOfElectionsCategoriesListElectionsCategories = electionsCategoriesListElectionsCategories.getIdElection();
                electionsCategoriesListElectionsCategories.setIdElection(elections);
                electionsCategoriesListElectionsCategories = em.merge(electionsCategoriesListElectionsCategories);
                if (oldIdElectionOfElectionsCategoriesListElectionsCategories != null) {
                    oldIdElectionOfElectionsCategoriesListElectionsCategories.getElectionsCategoriesList().remove(electionsCategoriesListElectionsCategories);
                    oldIdElectionOfElectionsCategoriesListElectionsCategories = em.merge(oldIdElectionOfElectionsCategoriesListElectionsCategories);
                }
            }
            for (Polls pollsListPolls : elections.getPollsList()) {
                Elections oldIdElectionOfPollsListPolls = pollsListPolls.getIdElection();
                pollsListPolls.setIdElection(elections);
                pollsListPolls = em.merge(pollsListPolls);
                if (oldIdElectionOfPollsListPolls != null) {
                    oldIdElectionOfPollsListPolls.getPollsList().remove(pollsListPolls);
                    oldIdElectionOfPollsListPolls = em.merge(oldIdElectionOfPollsListPolls);
                }
            }
            for (ElectionsRegions electionsRegionsListElectionsRegions : elections.getElectionsRegionsList()) {
                Elections oldIdElectionOfElectionsRegionsListElectionsRegions = electionsRegionsListElectionsRegions.getIdElection();
                electionsRegionsListElectionsRegions.setIdElection(elections);
                electionsRegionsListElectionsRegions = em.merge(electionsRegionsListElectionsRegions);
                if (oldIdElectionOfElectionsRegionsListElectionsRegions != null) {
                    oldIdElectionOfElectionsRegionsListElectionsRegions.getElectionsRegionsList().remove(electionsRegionsListElectionsRegions);
                    oldIdElectionOfElectionsRegionsListElectionsRegions = em.merge(oldIdElectionOfElectionsRegionsListElectionsRegions);
                }
            }
            for (ElectionsParties electionsPartiesListElectionsParties : elections.getElectionsPartiesList()) {
                Elections oldIdElectionOfElectionsPartiesListElectionsParties = electionsPartiesListElectionsParties.getIdElection();
                electionsPartiesListElectionsParties.setIdElection(elections);
                electionsPartiesListElectionsParties = em.merge(electionsPartiesListElectionsParties);
                if (oldIdElectionOfElectionsPartiesListElectionsParties != null) {
                    oldIdElectionOfElectionsPartiesListElectionsParties.getElectionsPartiesList().remove(electionsPartiesListElectionsParties);
                    oldIdElectionOfElectionsPartiesListElectionsParties = em.merge(oldIdElectionOfElectionsPartiesListElectionsParties);
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findElections(elections.getId()) != null) {
                throw new PreexistingEntityException("Elections " + elections + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Elections elections) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Elections persistentElections = em.find(Elections.class, elections.getId());
            List<ElectionsSections> electionsSectionsListOld = persistentElections.getElectionsSectionsList();
            List<ElectionsSections> electionsSectionsListNew = elections.getElectionsSectionsList();
            List<UserElections> userElectionsListOld = persistentElections.getUserElectionsList();
            List<UserElections> userElectionsListNew = elections.getUserElectionsList();
            List<ElectionsCategories> electionsCategoriesListOld = persistentElections.getElectionsCategoriesList();
            List<ElectionsCategories> electionsCategoriesListNew = elections.getElectionsCategoriesList();
            List<Polls> pollsListOld = persistentElections.getPollsList();
            List<Polls> pollsListNew = elections.getPollsList();
            List<ElectionsRegions> electionsRegionsListOld = persistentElections.getElectionsRegionsList();
            List<ElectionsRegions> electionsRegionsListNew = elections.getElectionsRegionsList();
            List<ElectionsParties> electionsPartiesListOld = persistentElections.getElectionsPartiesList();
            List<ElectionsParties> electionsPartiesListNew = elections.getElectionsPartiesList();
            List<String> illegalOrphanMessages = null;
            for (ElectionsSections electionsSectionsListOldElectionsSections : electionsSectionsListOld) {
                if (!electionsSectionsListNew.contains(electionsSectionsListOldElectionsSections)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain ElectionsSections " + electionsSectionsListOldElectionsSections + " since its idElection field is not nullable.");
                }
            }
            for (UserElections userElectionsListOldUserElections : userElectionsListOld) {
                if (!userElectionsListNew.contains(userElectionsListOldUserElections)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain UserElections " + userElectionsListOldUserElections + " since its idElection field is not nullable.");
                }
            }
            for (ElectionsCategories electionsCategoriesListOldElectionsCategories : electionsCategoriesListOld) {
                if (!electionsCategoriesListNew.contains(electionsCategoriesListOldElectionsCategories)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain ElectionsCategories " + electionsCategoriesListOldElectionsCategories + " since its idElection field is not nullable.");
                }
            }
            for (Polls pollsListOldPolls : pollsListOld) {
                if (!pollsListNew.contains(pollsListOldPolls)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Polls " + pollsListOldPolls + " since its idElection field is not nullable.");
                }
            }
            for (ElectionsRegions electionsRegionsListOldElectionsRegions : electionsRegionsListOld) {
                if (!electionsRegionsListNew.contains(electionsRegionsListOldElectionsRegions)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain ElectionsRegions " + electionsRegionsListOldElectionsRegions + " since its idElection field is not nullable.");
                }
            }
            for (ElectionsParties electionsPartiesListOldElectionsParties : electionsPartiesListOld) {
                if (!electionsPartiesListNew.contains(electionsPartiesListOldElectionsParties)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain ElectionsParties " + electionsPartiesListOldElectionsParties + " since its idElection field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            List<ElectionsSections> attachedElectionsSectionsListNew = new ArrayList<ElectionsSections>();
            for (ElectionsSections electionsSectionsListNewElectionsSectionsToAttach : electionsSectionsListNew) {
                electionsSectionsListNewElectionsSectionsToAttach = em.getReference(electionsSectionsListNewElectionsSectionsToAttach.getClass(), electionsSectionsListNewElectionsSectionsToAttach.getElectionsSectionsPK());
                attachedElectionsSectionsListNew.add(electionsSectionsListNewElectionsSectionsToAttach);
            }
            electionsSectionsListNew = attachedElectionsSectionsListNew;
            elections.setElectionsSectionsList(electionsSectionsListNew);
            List<UserElections> attachedUserElectionsListNew = new ArrayList<UserElections>();
            for (UserElections userElectionsListNewUserElectionsToAttach : userElectionsListNew) {
                userElectionsListNewUserElectionsToAttach = em.getReference(userElectionsListNewUserElectionsToAttach.getClass(), userElectionsListNewUserElectionsToAttach.getUserElectionsPK());
                attachedUserElectionsListNew.add(userElectionsListNewUserElectionsToAttach);
            }
            userElectionsListNew = attachedUserElectionsListNew;
            elections.setUserElectionsList(userElectionsListNew);
            List<ElectionsCategories> attachedElectionsCategoriesListNew = new ArrayList<ElectionsCategories>();
            for (ElectionsCategories electionsCategoriesListNewElectionsCategoriesToAttach : electionsCategoriesListNew) {
                electionsCategoriesListNewElectionsCategoriesToAttach = em.getReference(electionsCategoriesListNewElectionsCategoriesToAttach.getClass(), electionsCategoriesListNewElectionsCategoriesToAttach.getElectionsCategoriesPK());
                attachedElectionsCategoriesListNew.add(electionsCategoriesListNewElectionsCategoriesToAttach);
            }
            electionsCategoriesListNew = attachedElectionsCategoriesListNew;
            elections.setElectionsCategoriesList(electionsCategoriesListNew);
            List<Polls> attachedPollsListNew = new ArrayList<Polls>();
            for (Polls pollsListNewPollsToAttach : pollsListNew) {
                pollsListNewPollsToAttach = em.getReference(pollsListNewPollsToAttach.getClass(), pollsListNewPollsToAttach.getId());
                attachedPollsListNew.add(pollsListNewPollsToAttach);
            }
            pollsListNew = attachedPollsListNew;
            elections.setPollsList(pollsListNew);
            List<ElectionsRegions> attachedElectionsRegionsListNew = new ArrayList<ElectionsRegions>();
            for (ElectionsRegions electionsRegionsListNewElectionsRegionsToAttach : electionsRegionsListNew) {
                electionsRegionsListNewElectionsRegionsToAttach = em.getReference(electionsRegionsListNewElectionsRegionsToAttach.getClass(), electionsRegionsListNewElectionsRegionsToAttach.getElectionsRegionsPK());
                attachedElectionsRegionsListNew.add(electionsRegionsListNewElectionsRegionsToAttach);
            }
            electionsRegionsListNew = attachedElectionsRegionsListNew;
            elections.setElectionsRegionsList(electionsRegionsListNew);
            List<ElectionsParties> attachedElectionsPartiesListNew = new ArrayList<ElectionsParties>();
            for (ElectionsParties electionsPartiesListNewElectionsPartiesToAttach : electionsPartiesListNew) {
                electionsPartiesListNewElectionsPartiesToAttach = em.getReference(electionsPartiesListNewElectionsPartiesToAttach.getClass(), electionsPartiesListNewElectionsPartiesToAttach.getElectionsPartiesPK());
                attachedElectionsPartiesListNew.add(electionsPartiesListNewElectionsPartiesToAttach);
            }
            electionsPartiesListNew = attachedElectionsPartiesListNew;
            elections.setElectionsPartiesList(electionsPartiesListNew);
            elections = em.merge(elections);
            for (ElectionsSections electionsSectionsListNewElectionsSections : electionsSectionsListNew) {
                if (!electionsSectionsListOld.contains(electionsSectionsListNewElectionsSections)) {
                    Elections oldIdElectionOfElectionsSectionsListNewElectionsSections = electionsSectionsListNewElectionsSections.getIdElection();
                    electionsSectionsListNewElectionsSections.setIdElection(elections);
                    electionsSectionsListNewElectionsSections = em.merge(electionsSectionsListNewElectionsSections);
                    if (oldIdElectionOfElectionsSectionsListNewElectionsSections != null && !oldIdElectionOfElectionsSectionsListNewElectionsSections.equals(elections)) {
                        oldIdElectionOfElectionsSectionsListNewElectionsSections.getElectionsSectionsList().remove(electionsSectionsListNewElectionsSections);
                        oldIdElectionOfElectionsSectionsListNewElectionsSections = em.merge(oldIdElectionOfElectionsSectionsListNewElectionsSections);
                    }
                }
            }
            for (UserElections userElectionsListNewUserElections : userElectionsListNew) {
                if (!userElectionsListOld.contains(userElectionsListNewUserElections)) {
                    Elections oldIdElectionOfUserElectionsListNewUserElections = userElectionsListNewUserElections.getIdElection();
                    userElectionsListNewUserElections.setIdElection(elections);
                    userElectionsListNewUserElections = em.merge(userElectionsListNewUserElections);
                    if (oldIdElectionOfUserElectionsListNewUserElections != null && !oldIdElectionOfUserElectionsListNewUserElections.equals(elections)) {
                        oldIdElectionOfUserElectionsListNewUserElections.getUserElectionsList().remove(userElectionsListNewUserElections);
                        oldIdElectionOfUserElectionsListNewUserElections = em.merge(oldIdElectionOfUserElectionsListNewUserElections);
                    }
                }
            }
            for (ElectionsCategories electionsCategoriesListNewElectionsCategories : electionsCategoriesListNew) {
                if (!electionsCategoriesListOld.contains(electionsCategoriesListNewElectionsCategories)) {
                    Elections oldIdElectionOfElectionsCategoriesListNewElectionsCategories = electionsCategoriesListNewElectionsCategories.getIdElection();
                    electionsCategoriesListNewElectionsCategories.setIdElection(elections);
                    electionsCategoriesListNewElectionsCategories = em.merge(electionsCategoriesListNewElectionsCategories);
                    if (oldIdElectionOfElectionsCategoriesListNewElectionsCategories != null && !oldIdElectionOfElectionsCategoriesListNewElectionsCategories.equals(elections)) {
                        oldIdElectionOfElectionsCategoriesListNewElectionsCategories.getElectionsCategoriesList().remove(electionsCategoriesListNewElectionsCategories);
                        oldIdElectionOfElectionsCategoriesListNewElectionsCategories = em.merge(oldIdElectionOfElectionsCategoriesListNewElectionsCategories);
                    }
                }
            }
            for (Polls pollsListNewPolls : pollsListNew) {
                if (!pollsListOld.contains(pollsListNewPolls)) {
                    Elections oldIdElectionOfPollsListNewPolls = pollsListNewPolls.getIdElection();
                    pollsListNewPolls.setIdElection(elections);
                    pollsListNewPolls = em.merge(pollsListNewPolls);
                    if (oldIdElectionOfPollsListNewPolls != null && !oldIdElectionOfPollsListNewPolls.equals(elections)) {
                        oldIdElectionOfPollsListNewPolls.getPollsList().remove(pollsListNewPolls);
                        oldIdElectionOfPollsListNewPolls = em.merge(oldIdElectionOfPollsListNewPolls);
                    }
                }
            }
            for (ElectionsRegions electionsRegionsListNewElectionsRegions : electionsRegionsListNew) {
                if (!electionsRegionsListOld.contains(electionsRegionsListNewElectionsRegions)) {
                    Elections oldIdElectionOfElectionsRegionsListNewElectionsRegions = electionsRegionsListNewElectionsRegions.getIdElection();
                    electionsRegionsListNewElectionsRegions.setIdElection(elections);
                    electionsRegionsListNewElectionsRegions = em.merge(electionsRegionsListNewElectionsRegions);
                    if (oldIdElectionOfElectionsRegionsListNewElectionsRegions != null && !oldIdElectionOfElectionsRegionsListNewElectionsRegions.equals(elections)) {
                        oldIdElectionOfElectionsRegionsListNewElectionsRegions.getElectionsRegionsList().remove(electionsRegionsListNewElectionsRegions);
                        oldIdElectionOfElectionsRegionsListNewElectionsRegions = em.merge(oldIdElectionOfElectionsRegionsListNewElectionsRegions);
                    }
                }
            }
            for (ElectionsParties electionsPartiesListNewElectionsParties : electionsPartiesListNew) {
                if (!electionsPartiesListOld.contains(electionsPartiesListNewElectionsParties)) {
                    Elections oldIdElectionOfElectionsPartiesListNewElectionsParties = electionsPartiesListNewElectionsParties.getIdElection();
                    electionsPartiesListNewElectionsParties.setIdElection(elections);
                    electionsPartiesListNewElectionsParties = em.merge(electionsPartiesListNewElectionsParties);
                    if (oldIdElectionOfElectionsPartiesListNewElectionsParties != null && !oldIdElectionOfElectionsPartiesListNewElectionsParties.equals(elections)) {
                        oldIdElectionOfElectionsPartiesListNewElectionsParties.getElectionsPartiesList().remove(electionsPartiesListNewElectionsParties);
                        oldIdElectionOfElectionsPartiesListNewElectionsParties = em.merge(oldIdElectionOfElectionsPartiesListNewElectionsParties);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = elections.getId();
                if (findElections(id) == null) {
                    throw new NonexistentEntityException("The elections with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Elections elections;
            try {
                elections = em.getReference(Elections.class, id);
                elections.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The elections with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            List<ElectionsSections> electionsSectionsListOrphanCheck = elections.getElectionsSectionsList();
            for (ElectionsSections electionsSectionsListOrphanCheckElectionsSections : electionsSectionsListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Elections (" + elections + ") cannot be destroyed since the ElectionsSections " + electionsSectionsListOrphanCheckElectionsSections + " in its electionsSectionsList field has a non-nullable idElection field.");
            }
            List<UserElections> userElectionsListOrphanCheck = elections.getUserElectionsList();
            for (UserElections userElectionsListOrphanCheckUserElections : userElectionsListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Elections (" + elections + ") cannot be destroyed since the UserElections " + userElectionsListOrphanCheckUserElections + " in its userElectionsList field has a non-nullable idElection field.");
            }
            List<ElectionsCategories> electionsCategoriesListOrphanCheck = elections.getElectionsCategoriesList();
            for (ElectionsCategories electionsCategoriesListOrphanCheckElectionsCategories : electionsCategoriesListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Elections (" + elections + ") cannot be destroyed since the ElectionsCategories " + electionsCategoriesListOrphanCheckElectionsCategories + " in its electionsCategoriesList field has a non-nullable idElection field.");
            }
            List<Polls> pollsListOrphanCheck = elections.getPollsList();
            for (Polls pollsListOrphanCheckPolls : pollsListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Elections (" + elections + ") cannot be destroyed since the Polls " + pollsListOrphanCheckPolls + " in its pollsList field has a non-nullable idElection field.");
            }
            List<ElectionsRegions> electionsRegionsListOrphanCheck = elections.getElectionsRegionsList();
            for (ElectionsRegions electionsRegionsListOrphanCheckElectionsRegions : electionsRegionsListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Elections (" + elections + ") cannot be destroyed since the ElectionsRegions " + electionsRegionsListOrphanCheckElectionsRegions + " in its electionsRegionsList field has a non-nullable idElection field.");
            }
            List<ElectionsParties> electionsPartiesListOrphanCheck = elections.getElectionsPartiesList();
            for (ElectionsParties electionsPartiesListOrphanCheckElectionsParties : electionsPartiesListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Elections (" + elections + ") cannot be destroyed since the ElectionsParties " + electionsPartiesListOrphanCheckElectionsParties + " in its electionsPartiesList field has a non-nullable idElection field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            em.remove(elections);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Elections> findElectionsEntities() {
        return findElectionsEntities(true, -1, -1);
    }

    public List<Elections> findElectionsEntities(int maxResults, int firstResult) {
        return findElectionsEntities(false, maxResults, firstResult);
    }

    private List<Elections> findElectionsEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Elections.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Elections findElections(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Elections.class, id);
        } finally {
            em.close();
        }
    }

    public int getElectionsCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Elections> rt = cq.from(Elections.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
