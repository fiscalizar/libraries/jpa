/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ar.com.singleton.fiscalizar.libs.jpa.controllers.entities;

import ar.com.singleton.fiscalizar.libs.jpa.controllers.exceptions.IllegalOrphanException;
import ar.com.singleton.fiscalizar.libs.jpa.controllers.exceptions.NonexistentEntityException;
import ar.com.singleton.fiscalizar.libs.jpa.controllers.exceptions.PreexistingEntityException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import ar.com.singleton.fiscalizar.libs.jpa.entities.Parties;
import ar.com.singleton.fiscalizar.libs.jpa.entities.Elections;
import ar.com.singleton.fiscalizar.libs.jpa.entities.Users;
import ar.com.singleton.fiscalizar.libs.jpa.entities.VotingTables;
import ar.com.singleton.fiscalizar.libs.jpa.entities.Categories;
import ar.com.singleton.fiscalizar.libs.jpa.entities.Polls;
import ar.com.singleton.fiscalizar.libs.jpa.entities.PollsFiles;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author pato
 */
public class PollsJpaController implements Serializable {

    public PollsJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Polls polls) throws PreexistingEntityException, Exception {
        if (polls.getPollsFilesList() == null) {
            polls.setPollsFilesList(new ArrayList<PollsFiles>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Parties idParty = polls.getIdParty();
            if (idParty != null) {
                idParty = em.getReference(idParty.getClass(), idParty.getId());
                polls.setIdParty(idParty);
            }
            Elections idElection = polls.getIdElection();
            if (idElection != null) {
                idElection = em.getReference(idElection.getClass(), idElection.getId());
                polls.setIdElection(idElection);
            }
            Users idUser = polls.getIdUser();
            if (idUser != null) {
                idUser = em.getReference(idUser.getClass(), idUser.getId());
                polls.setIdUser(idUser);
            }
            VotingTables idVotingTable = polls.getIdVotingTable();
            if (idVotingTable != null) {
                idVotingTable = em.getReference(idVotingTable.getClass(), idVotingTable.getId());
                polls.setIdVotingTable(idVotingTable);
            }
            Categories idCategory = polls.getIdCategory();
            if (idCategory != null) {
                idCategory = em.getReference(idCategory.getClass(), idCategory.getId());
                polls.setIdCategory(idCategory);
            }
            List<PollsFiles> attachedPollsFilesList = new ArrayList<PollsFiles>();
            for (PollsFiles pollsFilesListPollsFilesToAttach : polls.getPollsFilesList()) {
                pollsFilesListPollsFilesToAttach = em.getReference(pollsFilesListPollsFilesToAttach.getClass(), pollsFilesListPollsFilesToAttach.getId());
                attachedPollsFilesList.add(pollsFilesListPollsFilesToAttach);
            }
            polls.setPollsFilesList(attachedPollsFilesList);
            em.persist(polls);
            if (idParty != null) {
                idParty.getPollsList().add(polls);
                idParty = em.merge(idParty);
            }
            if (idElection != null) {
                idElection.getPollsList().add(polls);
                idElection = em.merge(idElection);
            }
            if (idUser != null) {
                idUser.getPollsList().add(polls);
                idUser = em.merge(idUser);
            }
            if (idVotingTable != null) {
                idVotingTable.getPollsList().add(polls);
                idVotingTable = em.merge(idVotingTable);
            }
            if (idCategory != null) {
                idCategory.getPollsList().add(polls);
                idCategory = em.merge(idCategory);
            }
            for (PollsFiles pollsFilesListPollsFiles : polls.getPollsFilesList()) {
                Polls oldIdPollOfPollsFilesListPollsFiles = pollsFilesListPollsFiles.getIdPoll();
                pollsFilesListPollsFiles.setIdPoll(polls);
                pollsFilesListPollsFiles = em.merge(pollsFilesListPollsFiles);
                if (oldIdPollOfPollsFilesListPollsFiles != null) {
                    oldIdPollOfPollsFilesListPollsFiles.getPollsFilesList().remove(pollsFilesListPollsFiles);
                    oldIdPollOfPollsFilesListPollsFiles = em.merge(oldIdPollOfPollsFilesListPollsFiles);
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findPolls(polls.getId()) != null) {
                throw new PreexistingEntityException("Polls " + polls + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Polls polls) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Polls persistentPolls = em.find(Polls.class, polls.getId());
            Parties idPartyOld = persistentPolls.getIdParty();
            Parties idPartyNew = polls.getIdParty();
            Elections idElectionOld = persistentPolls.getIdElection();
            Elections idElectionNew = polls.getIdElection();
            Users idUserOld = persistentPolls.getIdUser();
            Users idUserNew = polls.getIdUser();
            VotingTables idVotingTableOld = persistentPolls.getIdVotingTable();
            VotingTables idVotingTableNew = polls.getIdVotingTable();
            Categories idCategoryOld = persistentPolls.getIdCategory();
            Categories idCategoryNew = polls.getIdCategory();
            List<PollsFiles> pollsFilesListOld = persistentPolls.getPollsFilesList();
            List<PollsFiles> pollsFilesListNew = polls.getPollsFilesList();
            List<String> illegalOrphanMessages = null;
            for (PollsFiles pollsFilesListOldPollsFiles : pollsFilesListOld) {
                if (!pollsFilesListNew.contains(pollsFilesListOldPollsFiles)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain PollsFiles " + pollsFilesListOldPollsFiles + " since its idPoll field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            if (idPartyNew != null) {
                idPartyNew = em.getReference(idPartyNew.getClass(), idPartyNew.getId());
                polls.setIdParty(idPartyNew);
            }
            if (idElectionNew != null) {
                idElectionNew = em.getReference(idElectionNew.getClass(), idElectionNew.getId());
                polls.setIdElection(idElectionNew);
            }
            if (idUserNew != null) {
                idUserNew = em.getReference(idUserNew.getClass(), idUserNew.getId());
                polls.setIdUser(idUserNew);
            }
            if (idVotingTableNew != null) {
                idVotingTableNew = em.getReference(idVotingTableNew.getClass(), idVotingTableNew.getId());
                polls.setIdVotingTable(idVotingTableNew);
            }
            if (idCategoryNew != null) {
                idCategoryNew = em.getReference(idCategoryNew.getClass(), idCategoryNew.getId());
                polls.setIdCategory(idCategoryNew);
            }
            List<PollsFiles> attachedPollsFilesListNew = new ArrayList<PollsFiles>();
            for (PollsFiles pollsFilesListNewPollsFilesToAttach : pollsFilesListNew) {
                pollsFilesListNewPollsFilesToAttach = em.getReference(pollsFilesListNewPollsFilesToAttach.getClass(), pollsFilesListNewPollsFilesToAttach.getId());
                attachedPollsFilesListNew.add(pollsFilesListNewPollsFilesToAttach);
            }
            pollsFilesListNew = attachedPollsFilesListNew;
            polls.setPollsFilesList(pollsFilesListNew);
            polls = em.merge(polls);
            if (idPartyOld != null && !idPartyOld.equals(idPartyNew)) {
                idPartyOld.getPollsList().remove(polls);
                idPartyOld = em.merge(idPartyOld);
            }
            if (idPartyNew != null && !idPartyNew.equals(idPartyOld)) {
                idPartyNew.getPollsList().add(polls);
                idPartyNew = em.merge(idPartyNew);
            }
            if (idElectionOld != null && !idElectionOld.equals(idElectionNew)) {
                idElectionOld.getPollsList().remove(polls);
                idElectionOld = em.merge(idElectionOld);
            }
            if (idElectionNew != null && !idElectionNew.equals(idElectionOld)) {
                idElectionNew.getPollsList().add(polls);
                idElectionNew = em.merge(idElectionNew);
            }
            if (idUserOld != null && !idUserOld.equals(idUserNew)) {
                idUserOld.getPollsList().remove(polls);
                idUserOld = em.merge(idUserOld);
            }
            if (idUserNew != null && !idUserNew.equals(idUserOld)) {
                idUserNew.getPollsList().add(polls);
                idUserNew = em.merge(idUserNew);
            }
            if (idVotingTableOld != null && !idVotingTableOld.equals(idVotingTableNew)) {
                idVotingTableOld.getPollsList().remove(polls);
                idVotingTableOld = em.merge(idVotingTableOld);
            }
            if (idVotingTableNew != null && !idVotingTableNew.equals(idVotingTableOld)) {
                idVotingTableNew.getPollsList().add(polls);
                idVotingTableNew = em.merge(idVotingTableNew);
            }
            if (idCategoryOld != null && !idCategoryOld.equals(idCategoryNew)) {
                idCategoryOld.getPollsList().remove(polls);
                idCategoryOld = em.merge(idCategoryOld);
            }
            if (idCategoryNew != null && !idCategoryNew.equals(idCategoryOld)) {
                idCategoryNew.getPollsList().add(polls);
                idCategoryNew = em.merge(idCategoryNew);
            }
            for (PollsFiles pollsFilesListNewPollsFiles : pollsFilesListNew) {
                if (!pollsFilesListOld.contains(pollsFilesListNewPollsFiles)) {
                    Polls oldIdPollOfPollsFilesListNewPollsFiles = pollsFilesListNewPollsFiles.getIdPoll();
                    pollsFilesListNewPollsFiles.setIdPoll(polls);
                    pollsFilesListNewPollsFiles = em.merge(pollsFilesListNewPollsFiles);
                    if (oldIdPollOfPollsFilesListNewPollsFiles != null && !oldIdPollOfPollsFilesListNewPollsFiles.equals(polls)) {
                        oldIdPollOfPollsFilesListNewPollsFiles.getPollsFilesList().remove(pollsFilesListNewPollsFiles);
                        oldIdPollOfPollsFilesListNewPollsFiles = em.merge(oldIdPollOfPollsFilesListNewPollsFiles);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = polls.getId();
                if (findPolls(id) == null) {
                    throw new NonexistentEntityException("The polls with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Polls polls;
            try {
                polls = em.getReference(Polls.class, id);
                polls.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The polls with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            List<PollsFiles> pollsFilesListOrphanCheck = polls.getPollsFilesList();
            for (PollsFiles pollsFilesListOrphanCheckPollsFiles : pollsFilesListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Polls (" + polls + ") cannot be destroyed since the PollsFiles " + pollsFilesListOrphanCheckPollsFiles + " in its pollsFilesList field has a non-nullable idPoll field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            Parties idParty = polls.getIdParty();
            if (idParty != null) {
                idParty.getPollsList().remove(polls);
                idParty = em.merge(idParty);
            }
            Elections idElection = polls.getIdElection();
            if (idElection != null) {
                idElection.getPollsList().remove(polls);
                idElection = em.merge(idElection);
            }
            Users idUser = polls.getIdUser();
            if (idUser != null) {
                idUser.getPollsList().remove(polls);
                idUser = em.merge(idUser);
            }
            VotingTables idVotingTable = polls.getIdVotingTable();
            if (idVotingTable != null) {
                idVotingTable.getPollsList().remove(polls);
                idVotingTable = em.merge(idVotingTable);
            }
            Categories idCategory = polls.getIdCategory();
            if (idCategory != null) {
                idCategory.getPollsList().remove(polls);
                idCategory = em.merge(idCategory);
            }
            em.remove(polls);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Polls> findPollsEntities() {
        return findPollsEntities(true, -1, -1);
    }

    public List<Polls> findPollsEntities(int maxResults, int firstResult) {
        return findPollsEntities(false, maxResults, firstResult);
    }

    private List<Polls> findPollsEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Polls.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Polls findPolls(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Polls.class, id);
        } finally {
            em.close();
        }
    }

    public int getPollsCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Polls> rt = cq.from(Polls.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
