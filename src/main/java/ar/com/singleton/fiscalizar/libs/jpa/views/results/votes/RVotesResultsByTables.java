/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ar.com.singleton.fiscalizar.libs.jpa.views.results.votes;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author pato
 */
@MappedSuperclass
@Table(name = "R_VotesResultsByTables", catalog = "", schema = "")
@XmlRootElement
public class RVotesResultsByTables implements Serializable {

    private static final long serialVersionUID = 1L;
    @Column(name = "idElection")
    private Integer idElection;
    @Column(name = "idCategory")
    private Integer idCategory;
    @Column(name = "idParty")
    private Integer idParty;
    @Column(name = "idVotingTable")
    private Integer idVotingTable;
    @Column(name = "electors")
    private Integer electors;
    @Column(name = "idInstitution")
    private Integer idInstitution;
    @Column(name = "votes")
    private String votes;
    @Column(name = "partyPercentage")
    private String partyPercentage;

    public RVotesResultsByTables() {
    }

    public Integer getIdElection() {
        return idElection;
    }

    public void setIdElection(Integer idElection) {
        this.idElection = idElection;
    }

    public Integer getIdCategory() {
        return idCategory;
    }

    public void setIdCategory(Integer idCategory) {
        this.idCategory = idCategory;
    }

    public Integer getIdParty() {
        return idParty;
    }

    public void setIdParty(Integer idParty) {
        this.idParty = idParty;
    }

    public Integer getIdVotingTable() {
        return idVotingTable;
    }

    public void setIdVotingTable(Integer idVotingTable) {
        this.idVotingTable = idVotingTable;
    }

    public Integer getElectors() {
        return electors;
    }

    public void setElectors(Integer electors) {
        this.electors = electors;
    }

    public Integer getIdInstitution() {
        return idInstitution;
    }

    public void setIdInstitution(Integer idInstitution) {
        this.idInstitution = idInstitution;
    }

    public String getVotes() {
        return votes;
    }

    public void setVotes(String votes) {
        this.votes = votes;
    }

    public String getPartyPercentage() {
        return partyPercentage;
    }

    public void setPartyPercentage(String partyPercentage) {
        this.partyPercentage = partyPercentage;
    }
    
}
