/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ar.com.singleton.fiscalizar.libs.jpa.entities.relations;

import ar.com.singleton.fiscalizar.libs.jpa.entities.Institutions;
import ar.com.singleton.fiscalizar.libs.jpa.entities.Users;
import ar.com.singleton.fiscalizar.libs.jpa.entities.keys.UserInstitutionsPK;
import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author pato
 */
@Entity
@Table(name = "UserInstitutions", catalog = "", schema = "")
@XmlRootElement
public class UserInstitutions implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected UserInstitutionsPK userInstitutionsPK;
    @Basic(optional = false)
    @Column(name = "createdAt")
    private String createdAt;
    @Column(name = "updatedAt")
    private String updatedAt;
    @Basic(optional = false)
    @Column(name = "deleted")
    private int deleted;
    @PrimaryKeyJoinColumn(name = "idUser", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Users idUser;
    @PrimaryKeyJoinColumn(name = "idUserInstitution", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Institutions idUserInstitution;

    public UserInstitutions() {
    }

    public UserInstitutions(UserInstitutionsPK userInstitutionsPK) {
        this.userInstitutionsPK = userInstitutionsPK;
    }

    public UserInstitutions(UserInstitutionsPK userInstitutionsPK, String createdAt, int deleted) {
        this.userInstitutionsPK = userInstitutionsPK;
        this.createdAt = createdAt;
        this.deleted = deleted;
    }

    public UserInstitutionsPK getUserInstitutionsPK() {
        return userInstitutionsPK;
    }

    public void setUserInstitutionsPK(UserInstitutionsPK userInstitutionsPK) {
        this.userInstitutionsPK = userInstitutionsPK;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public int getDeleted() {
        return deleted;
    }

    public void setDeleted(int deleted) {
        this.deleted = deleted;
    }

    public Users getIdUser() {
        return idUser;
    }

    public void setIdUser(Users idUser) {
        this.idUser = idUser;
    }

    public Institutions getIdUserInstitution() {
        return idUserInstitution;
    }

    public void setIdUserInstitution(Institutions idUserInstitution) {
        this.idUserInstitution = idUserInstitution;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (userInstitutionsPK != null ? userInstitutionsPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UserInstitutions)) {
            return false;
        }
        UserInstitutions other = (UserInstitutions) object;
        if ((this.userInstitutionsPK == null && other.userInstitutionsPK != null) || (this.userInstitutionsPK != null && !this.userInstitutionsPK.equals(other.userInstitutionsPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ar.com.singleton.app.fiscalizar.database.UserInstitutions[ userInstitutionsPK=" + userInstitutionsPK + " ]";
    }
    
}
