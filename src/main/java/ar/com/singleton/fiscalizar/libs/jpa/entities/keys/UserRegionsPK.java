/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ar.com.singleton.fiscalizar.libs.jpa.entities.keys;

import ar.com.singleton.fiscalizar.libs.jpa.entities.Regions;
import ar.com.singleton.fiscalizar.libs.jpa.entities.Users;
import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Basic;
import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 *
 * @author pato
 */
@Embeddable
public class UserRegionsPK implements Serializable {

    @Basic
    @ManyToOne
    @JoinColumn(name = "idUser", insertable = false, updatable = false)
    private Users idUser;
    
    @Basic
    @ManyToOne
    @JoinColumn(name = "idRegion",insertable = false, updatable = false)
    private Regions idRegion;
    
    public UserRegionsPK(Users idUser, Regions idRegion) {
        this.idRegion = idRegion;
        this.idUser = idUser;
    }

    public UserRegionsPK() {
    }

    public Users getIdUser() {
        return idUser;
    }

    public void setIdUser(Users idUser) {
        this.idUser = idUser;
    }

    public Regions getIdRegion() {
        return idRegion;
    }

    public void setIdRegion(Regions idRegion) {
        this.idRegion = idRegion;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 67 * hash + Objects.hashCode(this.idUser);
        hash = 67 * hash + Objects.hashCode(this.idRegion);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final UserRegionsPK other = (UserRegionsPK) obj;
        if (!Objects.equals(this.idUser, other.idUser)) {
            return false;
        }
        return Objects.equals(this.idRegion, other.idRegion);
    }
    
}
