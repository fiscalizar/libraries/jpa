/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ar.com.singleton.fiscalizar.libs.jpa.controllers.entities.relations;

import ar.com.singleton.fiscalizar.libs.jpa.controllers.exceptions.NonexistentEntityException;
import ar.com.singleton.fiscalizar.libs.jpa.controllers.exceptions.PreexistingEntityException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import ar.com.singleton.fiscalizar.libs.jpa.entities.Circuits;
import ar.com.singleton.fiscalizar.libs.jpa.entities.Users;
import ar.com.singleton.fiscalizar.libs.jpa.entities.keys.UserCircuitsPK;
import ar.com.singleton.fiscalizar.libs.jpa.entities.relations.UserCircuits;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author pato
 */
public class UserCircuitsJpaController implements Serializable {

    public UserCircuitsJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(UserCircuits userCircuits) throws PreexistingEntityException, Exception {
        if (userCircuits.getUserCircuitsPK() == null) {
            userCircuits.setUserCircuitsPK(new UserCircuitsPK());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Circuits idCircuit = userCircuits.getIdCircuit();
            if (idCircuit != null) {
                idCircuit = em.getReference(idCircuit.getClass(), idCircuit.getId());
                userCircuits.setIdCircuit(idCircuit);
            }
            Users idUser = userCircuits.getIdUser();
            if (idUser != null) {
                idUser = em.getReference(idUser.getClass(), idUser.getId());
                userCircuits.setIdUser(idUser);
            }
            em.persist(userCircuits);
            if (idCircuit != null) {
                idCircuit.getUserCircuitsList().add(userCircuits);
                idCircuit = em.merge(idCircuit);
            }
            if (idUser != null) {
                idUser.getUserCircuitsList().add(userCircuits);
                idUser = em.merge(idUser);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findUserCircuits(userCircuits.getUserCircuitsPK()) != null) {
                throw new PreexistingEntityException("UserCircuits " + userCircuits + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(UserCircuits userCircuits) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            UserCircuits persistentUserCircuits = em.find(UserCircuits.class, userCircuits.getUserCircuitsPK());
            Circuits idCircuitOld = persistentUserCircuits.getIdCircuit();
            Circuits idCircuitNew = userCircuits.getIdCircuit();
            Users idUserOld = persistentUserCircuits.getIdUser();
            Users idUserNew = userCircuits.getIdUser();
            if (idCircuitNew != null) {
                idCircuitNew = em.getReference(idCircuitNew.getClass(), idCircuitNew.getId());
                userCircuits.setIdCircuit(idCircuitNew);
            }
            if (idUserNew != null) {
                idUserNew = em.getReference(idUserNew.getClass(), idUserNew.getId());
                userCircuits.setIdUser(idUserNew);
            }
            userCircuits = em.merge(userCircuits);
            if (idCircuitOld != null && !idCircuitOld.equals(idCircuitNew)) {
                idCircuitOld.getUserCircuitsList().remove(userCircuits);
                idCircuitOld = em.merge(idCircuitOld);
            }
            if (idCircuitNew != null && !idCircuitNew.equals(idCircuitOld)) {
                idCircuitNew.getUserCircuitsList().add(userCircuits);
                idCircuitNew = em.merge(idCircuitNew);
            }
            if (idUserOld != null && !idUserOld.equals(idUserNew)) {
                idUserOld.getUserCircuitsList().remove(userCircuits);
                idUserOld = em.merge(idUserOld);
            }
            if (idUserNew != null && !idUserNew.equals(idUserOld)) {
                idUserNew.getUserCircuitsList().add(userCircuits);
                idUserNew = em.merge(idUserNew);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                UserCircuitsPK id = userCircuits.getUserCircuitsPK();
                if (findUserCircuits(id) == null) {
                    throw new NonexistentEntityException("The userCircuits with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(UserCircuitsPK id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            UserCircuits userCircuits;
            try {
                userCircuits = em.getReference(UserCircuits.class, id);
                userCircuits.getUserCircuitsPK();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The userCircuits with id " + id + " no longer exists.", enfe);
            }
            Circuits idCircuit = userCircuits.getIdCircuit();
            if (idCircuit != null) {
                idCircuit.getUserCircuitsList().remove(userCircuits);
                idCircuit = em.merge(idCircuit);
            }
            Users idUser = userCircuits.getIdUser();
            if (idUser != null) {
                idUser.getUserCircuitsList().remove(userCircuits);
                idUser = em.merge(idUser);
            }
            em.remove(userCircuits);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<UserCircuits> findUserCircuitsEntities() {
        return findUserCircuitsEntities(true, -1, -1);
    }

    public List<UserCircuits> findUserCircuitsEntities(int maxResults, int firstResult) {
        return findUserCircuitsEntities(false, maxResults, firstResult);
    }

    private List<UserCircuits> findUserCircuitsEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(UserCircuits.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public UserCircuits findUserCircuits(UserCircuitsPK id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(UserCircuits.class, id);
        } finally {
            em.close();
        }
    }

    public int getUserCircuitsCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<UserCircuits> rt = cq.from(UserCircuits.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
