/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ar.com.singleton.fiscalizar.libs.jpa.entities.relations;

import ar.com.singleton.fiscalizar.libs.jpa.entities.Elections;
import ar.com.singleton.fiscalizar.libs.jpa.entities.Parties;
import ar.com.singleton.fiscalizar.libs.jpa.entities.keys.ElectionsPartiesPK;
import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author pato
 */
@Entity
@Table(name = "ElectionsParties", catalog = "", schema = "")
@XmlRootElement
public class ElectionsParties implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected ElectionsPartiesPK electionsPartiesPK;
    @Basic(optional = false)
    @Column(name = "createdAt")
    private String createdAt;
    @Column(name = "updatedAt")
    private String updatedAt;
    @Basic(optional = false)
    @Column(name = "deleted")
    private int deleted;
    @PrimaryKeyJoinColumn(name = "idParty", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Parties idParty;
    @PrimaryKeyJoinColumn(name = "idElection", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Elections idElection;

    public ElectionsParties() {
    }

    public ElectionsParties(ElectionsPartiesPK electionsPartiesPK) {
        this.electionsPartiesPK = electionsPartiesPK;
    }

    public ElectionsParties(ElectionsPartiesPK electionsPartiesPK, String createdAt, int deleted) {
        this.electionsPartiesPK = electionsPartiesPK;
        this.createdAt = createdAt;
        this.deleted = deleted;
    }

    public ElectionsPartiesPK getElectionsPartiesPK() {
        return electionsPartiesPK;
    }

    public void setElectionsPartiesPK(ElectionsPartiesPK electionsPartiesPK) {
        this.electionsPartiesPK = electionsPartiesPK;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public int getDeleted() {
        return deleted;
    }

    public void setDeleted(int deleted) {
        this.deleted = deleted;
    }

    public Parties getIdParty() {
        return idParty;
    }

    public void setIdParty(Parties idParty) {
        this.idParty = idParty;
    }

    public Elections getIdElection() {
        return idElection;
    }

    public void setIdElection(Elections idElection) {
        this.idElection = idElection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (electionsPartiesPK != null ? electionsPartiesPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ElectionsParties)) {
            return false;
        }
        ElectionsParties other = (ElectionsParties) object;
        if ((this.electionsPartiesPK == null && other.electionsPartiesPK != null) || (this.electionsPartiesPK != null && !this.electionsPartiesPK.equals(other.electionsPartiesPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ar.com.singleton.app.fiscalizar.database.ElectionsParties[ electionsPartiesPK=" + electionsPartiesPK + " ]";
    }
    
}
