/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ar.com.singleton.fiscalizar.libs.jpa.controllers.entities.relations;

import ar.com.singleton.fiscalizar.libs.jpa.controllers.exceptions.NonexistentEntityException;
import ar.com.singleton.fiscalizar.libs.jpa.controllers.exceptions.PreexistingEntityException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import ar.com.singleton.fiscalizar.libs.jpa.entities.Sections;
import ar.com.singleton.fiscalizar.libs.jpa.entities.Elections;
import ar.com.singleton.fiscalizar.libs.jpa.entities.keys.ElectionsSectionsPK;
import ar.com.singleton.fiscalizar.libs.jpa.entities.relations.ElectionsSections;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author pato
 */
public class ElectionsSectionsJpaController implements Serializable {

    public ElectionsSectionsJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(ElectionsSections electionsSections) throws PreexistingEntityException, Exception {
        if (electionsSections.getElectionsSectionsPK() == null) {
            electionsSections.setElectionsSectionsPK(new ElectionsSectionsPK());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Sections idSection = electionsSections.getIdSection();
            if (idSection != null) {
                idSection = em.getReference(idSection.getClass(), idSection.getId());
                electionsSections.setIdSection(idSection);
            }
            Elections idElection = electionsSections.getIdElection();
            if (idElection != null) {
                idElection = em.getReference(idElection.getClass(), idElection.getId());
                electionsSections.setIdElection(idElection);
            }
            em.persist(electionsSections);
            if (idSection != null) {
                idSection.getElectionsSectionsList().add(electionsSections);
                idSection = em.merge(idSection);
            }
            if (idElection != null) {
                idElection.getElectionsSectionsList().add(electionsSections);
                idElection = em.merge(idElection);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findElectionsSections(electionsSections.getElectionsSectionsPK()) != null) {
                throw new PreexistingEntityException("ElectionsSections " + electionsSections + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(ElectionsSections electionsSections) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            ElectionsSections persistentElectionsSections = em.find(ElectionsSections.class, electionsSections.getElectionsSectionsPK());
            Sections idSectionOld = persistentElectionsSections.getIdSection();
            Sections idSectionNew = electionsSections.getIdSection();
            Elections idElectionOld = persistentElectionsSections.getIdElection();
            Elections idElectionNew = electionsSections.getIdElection();
            if (idSectionNew != null) {
                idSectionNew = em.getReference(idSectionNew.getClass(), idSectionNew.getId());
                electionsSections.setIdSection(idSectionNew);
            }
            if (idElectionNew != null) {
                idElectionNew = em.getReference(idElectionNew.getClass(), idElectionNew.getId());
                electionsSections.setIdElection(idElectionNew);
            }
            electionsSections = em.merge(electionsSections);
            if (idSectionOld != null && !idSectionOld.equals(idSectionNew)) {
                idSectionOld.getElectionsSectionsList().remove(electionsSections);
                idSectionOld = em.merge(idSectionOld);
            }
            if (idSectionNew != null && !idSectionNew.equals(idSectionOld)) {
                idSectionNew.getElectionsSectionsList().add(electionsSections);
                idSectionNew = em.merge(idSectionNew);
            }
            if (idElectionOld != null && !idElectionOld.equals(idElectionNew)) {
                idElectionOld.getElectionsSectionsList().remove(electionsSections);
                idElectionOld = em.merge(idElectionOld);
            }
            if (idElectionNew != null && !idElectionNew.equals(idElectionOld)) {
                idElectionNew.getElectionsSectionsList().add(electionsSections);
                idElectionNew = em.merge(idElectionNew);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                ElectionsSectionsPK id = electionsSections.getElectionsSectionsPK();
                if (findElectionsSections(id) == null) {
                    throw new NonexistentEntityException("The electionsSections with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(ElectionsSectionsPK id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            ElectionsSections electionsSections;
            try {
                electionsSections = em.getReference(ElectionsSections.class, id);
                electionsSections.getElectionsSectionsPK();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The electionsSections with id " + id + " no longer exists.", enfe);
            }
            Sections idSection = electionsSections.getIdSection();
            if (idSection != null) {
                idSection.getElectionsSectionsList().remove(electionsSections);
                idSection = em.merge(idSection);
            }
            Elections idElection = electionsSections.getIdElection();
            if (idElection != null) {
                idElection.getElectionsSectionsList().remove(electionsSections);
                idElection = em.merge(idElection);
            }
            em.remove(electionsSections);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<ElectionsSections> findElectionsSectionsEntities() {
        return findElectionsSectionsEntities(true, -1, -1);
    }

    public List<ElectionsSections> findElectionsSectionsEntities(int maxResults, int firstResult) {
        return findElectionsSectionsEntities(false, maxResults, firstResult);
    }

    private List<ElectionsSections> findElectionsSectionsEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(ElectionsSections.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public ElectionsSections findElectionsSections(ElectionsSectionsPK id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(ElectionsSections.class, id);
        } finally {
            em.close();
        }
    }

    public int getElectionsSectionsCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<ElectionsSections> rt = cq.from(ElectionsSections.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
