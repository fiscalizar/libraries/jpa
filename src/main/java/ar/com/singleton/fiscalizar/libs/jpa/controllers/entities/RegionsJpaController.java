/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ar.com.singleton.fiscalizar.libs.jpa.controllers.entities;

import ar.com.singleton.fiscalizar.libs.jpa.controllers.exceptions.IllegalOrphanException;
import ar.com.singleton.fiscalizar.libs.jpa.controllers.exceptions.NonexistentEntityException;
import ar.com.singleton.fiscalizar.libs.jpa.controllers.exceptions.PreexistingEntityException;
import ar.com.singleton.fiscalizar.libs.jpa.entities.Regions;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import ar.com.singleton.fiscalizar.libs.jpa.entities.Sections;
import java.util.ArrayList;
import java.util.List;
import ar.com.singleton.fiscalizar.libs.jpa.entities.relations.UserRegions;
import ar.com.singleton.fiscalizar.libs.jpa.entities.relations.ElectionsRegions;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author pato
 */
public class RegionsJpaController implements Serializable {

    public RegionsJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Regions regions) throws PreexistingEntityException, Exception {
        if (regions.getSectionsList() == null) {
            regions.setSectionsList(new ArrayList<Sections>());
        }
        if (regions.getUserRegionsList() == null) {
            regions.setUserRegionsList(new ArrayList<UserRegions>());
        }
        if (regions.getElectionsRegionsList() == null) {
            regions.setElectionsRegionsList(new ArrayList<ElectionsRegions>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            List<Sections> attachedSectionsList = new ArrayList<Sections>();
            for (Sections sectionsListSectionsToAttach : regions.getSectionsList()) {
                sectionsListSectionsToAttach = em.getReference(sectionsListSectionsToAttach.getClass(), sectionsListSectionsToAttach.getId());
                attachedSectionsList.add(sectionsListSectionsToAttach);
            }
            regions.setSectionsList(attachedSectionsList);
            List<UserRegions> attachedUserRegionsList = new ArrayList<UserRegions>();
            for (UserRegions userRegionsListUserRegionsToAttach : regions.getUserRegionsList()) {
                userRegionsListUserRegionsToAttach = em.getReference(userRegionsListUserRegionsToAttach.getClass(), userRegionsListUserRegionsToAttach.getUserRegionsPK());
                attachedUserRegionsList.add(userRegionsListUserRegionsToAttach);
            }
            regions.setUserRegionsList(attachedUserRegionsList);
            List<ElectionsRegions> attachedElectionsRegionsList = new ArrayList<ElectionsRegions>();
            for (ElectionsRegions electionsRegionsListElectionsRegionsToAttach : regions.getElectionsRegionsList()) {
                electionsRegionsListElectionsRegionsToAttach = em.getReference(electionsRegionsListElectionsRegionsToAttach.getClass(), electionsRegionsListElectionsRegionsToAttach.getElectionsRegionsPK());
                attachedElectionsRegionsList.add(electionsRegionsListElectionsRegionsToAttach);
            }
            regions.setElectionsRegionsList(attachedElectionsRegionsList);
            em.persist(regions);
            for (Sections sectionsListSections : regions.getSectionsList()) {
                Regions oldIdRegionOfSectionsListSections = sectionsListSections.getIdRegion();
                sectionsListSections.setIdRegion(regions);
                sectionsListSections = em.merge(sectionsListSections);
                if (oldIdRegionOfSectionsListSections != null) {
                    oldIdRegionOfSectionsListSections.getSectionsList().remove(sectionsListSections);
                    oldIdRegionOfSectionsListSections = em.merge(oldIdRegionOfSectionsListSections);
                }
            }
            for (UserRegions userRegionsListUserRegions : regions.getUserRegionsList()) {
                Regions oldIdRegionOfUserRegionsListUserRegions = userRegionsListUserRegions.getIdRegion();
                userRegionsListUserRegions.setIdRegion(regions);
                userRegionsListUserRegions = em.merge(userRegionsListUserRegions);
                if (oldIdRegionOfUserRegionsListUserRegions != null) {
                    oldIdRegionOfUserRegionsListUserRegions.getUserRegionsList().remove(userRegionsListUserRegions);
                    oldIdRegionOfUserRegionsListUserRegions = em.merge(oldIdRegionOfUserRegionsListUserRegions);
                }
            }
            for (ElectionsRegions electionsRegionsListElectionsRegions : regions.getElectionsRegionsList()) {
                Regions oldIdRegionOfElectionsRegionsListElectionsRegions = electionsRegionsListElectionsRegions.getIdRegion();
                electionsRegionsListElectionsRegions.setIdRegion(regions);
                electionsRegionsListElectionsRegions = em.merge(electionsRegionsListElectionsRegions);
                if (oldIdRegionOfElectionsRegionsListElectionsRegions != null) {
                    oldIdRegionOfElectionsRegionsListElectionsRegions.getElectionsRegionsList().remove(electionsRegionsListElectionsRegions);
                    oldIdRegionOfElectionsRegionsListElectionsRegions = em.merge(oldIdRegionOfElectionsRegionsListElectionsRegions);
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findRegions(regions.getId()) != null) {
                throw new PreexistingEntityException("Regions " + regions + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Regions regions) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Regions persistentRegions = em.find(Regions.class, regions.getId());
            List<Sections> sectionsListOld = persistentRegions.getSectionsList();
            List<Sections> sectionsListNew = regions.getSectionsList();
            List<UserRegions> userRegionsListOld = persistentRegions.getUserRegionsList();
            List<UserRegions> userRegionsListNew = regions.getUserRegionsList();
            List<ElectionsRegions> electionsRegionsListOld = persistentRegions.getElectionsRegionsList();
            List<ElectionsRegions> electionsRegionsListNew = regions.getElectionsRegionsList();
            List<String> illegalOrphanMessages = null;
            for (Sections sectionsListOldSections : sectionsListOld) {
                if (!sectionsListNew.contains(sectionsListOldSections)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Sections " + sectionsListOldSections + " since its idRegion field is not nullable.");
                }
            }
            for (UserRegions userRegionsListOldUserRegions : userRegionsListOld) {
                if (!userRegionsListNew.contains(userRegionsListOldUserRegions)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain UserRegions " + userRegionsListOldUserRegions + " since its idRegion field is not nullable.");
                }
            }
            for (ElectionsRegions electionsRegionsListOldElectionsRegions : electionsRegionsListOld) {
                if (!electionsRegionsListNew.contains(electionsRegionsListOldElectionsRegions)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain ElectionsRegions " + electionsRegionsListOldElectionsRegions + " since its idRegion field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            List<Sections> attachedSectionsListNew = new ArrayList<Sections>();
            for (Sections sectionsListNewSectionsToAttach : sectionsListNew) {
                sectionsListNewSectionsToAttach = em.getReference(sectionsListNewSectionsToAttach.getClass(), sectionsListNewSectionsToAttach.getId());
                attachedSectionsListNew.add(sectionsListNewSectionsToAttach);
            }
            sectionsListNew = attachedSectionsListNew;
            regions.setSectionsList(sectionsListNew);
            List<UserRegions> attachedUserRegionsListNew = new ArrayList<UserRegions>();
            for (UserRegions userRegionsListNewUserRegionsToAttach : userRegionsListNew) {
                userRegionsListNewUserRegionsToAttach = em.getReference(userRegionsListNewUserRegionsToAttach.getClass(), userRegionsListNewUserRegionsToAttach.getUserRegionsPK());
                attachedUserRegionsListNew.add(userRegionsListNewUserRegionsToAttach);
            }
            userRegionsListNew = attachedUserRegionsListNew;
            regions.setUserRegionsList(userRegionsListNew);
            List<ElectionsRegions> attachedElectionsRegionsListNew = new ArrayList<ElectionsRegions>();
            for (ElectionsRegions electionsRegionsListNewElectionsRegionsToAttach : electionsRegionsListNew) {
                electionsRegionsListNewElectionsRegionsToAttach = em.getReference(electionsRegionsListNewElectionsRegionsToAttach.getClass(), electionsRegionsListNewElectionsRegionsToAttach.getElectionsRegionsPK());
                attachedElectionsRegionsListNew.add(electionsRegionsListNewElectionsRegionsToAttach);
            }
            electionsRegionsListNew = attachedElectionsRegionsListNew;
            regions.setElectionsRegionsList(electionsRegionsListNew);
            regions = em.merge(regions);
            for (Sections sectionsListNewSections : sectionsListNew) {
                if (!sectionsListOld.contains(sectionsListNewSections)) {
                    Regions oldIdRegionOfSectionsListNewSections = sectionsListNewSections.getIdRegion();
                    sectionsListNewSections.setIdRegion(regions);
                    sectionsListNewSections = em.merge(sectionsListNewSections);
                    if (oldIdRegionOfSectionsListNewSections != null && !oldIdRegionOfSectionsListNewSections.equals(regions)) {
                        oldIdRegionOfSectionsListNewSections.getSectionsList().remove(sectionsListNewSections);
                        oldIdRegionOfSectionsListNewSections = em.merge(oldIdRegionOfSectionsListNewSections);
                    }
                }
            }
            for (UserRegions userRegionsListNewUserRegions : userRegionsListNew) {
                if (!userRegionsListOld.contains(userRegionsListNewUserRegions)) {
                    Regions oldIdRegionOfUserRegionsListNewUserRegions = userRegionsListNewUserRegions.getIdRegion();
                    userRegionsListNewUserRegions.setIdRegion(regions);
                    userRegionsListNewUserRegions = em.merge(userRegionsListNewUserRegions);
                    if (oldIdRegionOfUserRegionsListNewUserRegions != null && !oldIdRegionOfUserRegionsListNewUserRegions.equals(regions)) {
                        oldIdRegionOfUserRegionsListNewUserRegions.getUserRegionsList().remove(userRegionsListNewUserRegions);
                        oldIdRegionOfUserRegionsListNewUserRegions = em.merge(oldIdRegionOfUserRegionsListNewUserRegions);
                    }
                }
            }
            for (ElectionsRegions electionsRegionsListNewElectionsRegions : electionsRegionsListNew) {
                if (!electionsRegionsListOld.contains(electionsRegionsListNewElectionsRegions)) {
                    Regions oldIdRegionOfElectionsRegionsListNewElectionsRegions = electionsRegionsListNewElectionsRegions.getIdRegion();
                    electionsRegionsListNewElectionsRegions.setIdRegion(regions);
                    electionsRegionsListNewElectionsRegions = em.merge(electionsRegionsListNewElectionsRegions);
                    if (oldIdRegionOfElectionsRegionsListNewElectionsRegions != null && !oldIdRegionOfElectionsRegionsListNewElectionsRegions.equals(regions)) {
                        oldIdRegionOfElectionsRegionsListNewElectionsRegions.getElectionsRegionsList().remove(electionsRegionsListNewElectionsRegions);
                        oldIdRegionOfElectionsRegionsListNewElectionsRegions = em.merge(oldIdRegionOfElectionsRegionsListNewElectionsRegions);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = regions.getId();
                if (findRegions(id) == null) {
                    throw new NonexistentEntityException("The regions with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Regions regions;
            try {
                regions = em.getReference(Regions.class, id);
                regions.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The regions with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            List<Sections> sectionsListOrphanCheck = regions.getSectionsList();
            for (Sections sectionsListOrphanCheckSections : sectionsListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Regions (" + regions + ") cannot be destroyed since the Sections " + sectionsListOrphanCheckSections + " in its sectionsList field has a non-nullable idRegion field.");
            }
            List<UserRegions> userRegionsListOrphanCheck = regions.getUserRegionsList();
            for (UserRegions userRegionsListOrphanCheckUserRegions : userRegionsListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Regions (" + regions + ") cannot be destroyed since the UserRegions " + userRegionsListOrphanCheckUserRegions + " in its userRegionsList field has a non-nullable idRegion field.");
            }
            List<ElectionsRegions> electionsRegionsListOrphanCheck = regions.getElectionsRegionsList();
            for (ElectionsRegions electionsRegionsListOrphanCheckElectionsRegions : electionsRegionsListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Regions (" + regions + ") cannot be destroyed since the ElectionsRegions " + electionsRegionsListOrphanCheckElectionsRegions + " in its electionsRegionsList field has a non-nullable idRegion field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            em.remove(regions);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Regions> findRegionsEntities() {
        return findRegionsEntities(true, -1, -1);
    }

    public List<Regions> findRegionsEntities(int maxResults, int firstResult) {
        return findRegionsEntities(false, maxResults, firstResult);
    }

    private List<Regions> findRegionsEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Regions.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Regions findRegions(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Regions.class, id);
        } finally {
            em.close();
        }
    }

    public int getRegionsCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Regions> rt = cq.from(Regions.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
