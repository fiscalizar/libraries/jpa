/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ar.com.singleton.fiscalizar.libs.jpa.entities;

import ar.com.singleton.fiscalizar.libs.jpa.entities.relations.UserCircuits;
import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author pato
 */
@Entity
@Table(name = "Circuits", catalog = "", schema = "")
@XmlRootElement
public class Circuits implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "name")
    private String name;
    @Basic(optional = false)
    @Column(name = "code")
    private String code;
    @Basic(optional = false)
    @Column(name = "createdAt")
    private String createdAt;
    @Column(name = "updatedAt")
    private String updatedAt;
    @Basic(optional = false)
    @Column(name = "deleted")
    private int deleted;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idCircuit")
    private List<Institutions> institutionsList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idCircuit")
    private List<UserCircuits> userCircuitsList;
    @JoinColumn(name = "idSection", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Sections idSection;

    public Circuits() {
    }

    public Circuits(Integer id) {
        this.id = id;
    }

    public Circuits(Integer id, String name, String code, String createdAt, int deleted) {
        this.id = id;
        this.name = name;
        this.code = code;
        this.createdAt = createdAt;
        this.deleted = deleted;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public int getDeleted() {
        return deleted;
    }

    public void setDeleted(int deleted) {
        this.deleted = deleted;
    }

    @XmlTransient
    public List<Institutions> getInstitutionsList() {
        return institutionsList;
    }

    public void setInstitutionsList(List<Institutions> institutionsList) {
        this.institutionsList = institutionsList;
    }

    @XmlTransient
    public List<UserCircuits> getUserCircuitsList() {
        return userCircuitsList;
    }

    public void setUserCircuitsList(List<UserCircuits> userCircuitsList) {
        this.userCircuitsList = userCircuitsList;
    }

    public Sections getIdSection() {
        return idSection;
    }

    public void setIdSection(Sections idSection) {
        this.idSection = idSection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Circuits)) {
            return false;
        }
        Circuits other = (Circuits) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ar.com.singleton.app.fiscalizar.database.Circuits[ id=" + id + " ]";
    }
    
}
