/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ar.com.singleton.fiscalizar.libs.jpa.entities;

import ar.com.singleton.fiscalizar.libs.jpa.entities.relations.UserVotingTables;
import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author pato
 */
@Entity
@Table(name = "VotingTables", catalog = "", schema = "")
@XmlRootElement
public class VotingTables implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Column(name = "name")
    private String name;
    @Basic(optional = false)
    @Column(name = "code")
    private String code;
    @Column(name = "electors")
    private Integer electors;
    @Basic(optional = false)
    @Column(name = "createdAt")
    private String createdAt;
    @Column(name = "updatedAt")
    private String updatedAt;
    @Basic(optional = false)
    @Column(name = "deleted")
    private int deleted;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idVotingTable")
    private List<UserVotingTables> userVotingTablesList;
    @JoinColumn(name = "idInstitution", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Institutions idInstitution;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idVotingTable")
    private List<Polls> pollsList;

    public VotingTables() {
    }

    public VotingTables(Integer id) {
        this.id = id;
    }

    public VotingTables(Integer id, String code, String createdAt, int deleted) {
        this.id = id;
        this.code = code;
        this.createdAt = createdAt;
        this.deleted = deleted;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Integer getElectors() {
        return electors;
    }

    public void setElectors(Integer electors) {
        this.electors = electors;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public int getDeleted() {
        return deleted;
    }

    public void setDeleted(int deleted) {
        this.deleted = deleted;
    }

    @XmlTransient
    public List<UserVotingTables> getUserVotingTablesList() {
        return userVotingTablesList;
    }

    public void setUserVotingTablesList(List<UserVotingTables> userVotingTablesList) {
        this.userVotingTablesList = userVotingTablesList;
    }

    public Institutions getIdInstitution() {
        return idInstitution;
    }

    public void setIdInstitution(Institutions idInstitution) {
        this.idInstitution = idInstitution;
    }

    @XmlTransient
    public List<Polls> getPollsList() {
        return pollsList;
    }

    public void setPollsList(List<Polls> pollsList) {
        this.pollsList = pollsList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof VotingTables)) {
            return false;
        }
        VotingTables other = (VotingTables) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ar.com.singleton.app.fiscalizar.database.VotingTables[ id=" + id + " ]";
    }
    
}
