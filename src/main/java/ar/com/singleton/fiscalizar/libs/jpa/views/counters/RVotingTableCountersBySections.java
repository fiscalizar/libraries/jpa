/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ar.com.singleton.fiscalizar.libs.jpa.views.counters;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author pato
 */
@MappedSuperclass
@Table(name = "R_VotingTableCountersBySections", catalog = "", schema = "")
@XmlRootElement
public class RVotingTableCountersBySections implements Serializable {

    private static final long serialVersionUID = 1L;
    @Column(name = "id")
    private Integer id;
    @Column(name = "votingTables")
    private String votingTables;
    @Column(name = "electors")
    private String electors;

    public RVotingTableCountersBySections() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getVotingTables() {
        return votingTables;
    }

    public void setVotingTables(String votingTables) {
        this.votingTables = votingTables;
    }

    public String getElectors() {
        return electors;
    }

    public void setElectors(String electors) {
        this.electors = electors;
    }
    
}
