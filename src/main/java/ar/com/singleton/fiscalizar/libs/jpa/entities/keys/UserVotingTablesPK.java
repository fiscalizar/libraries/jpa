/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ar.com.singleton.fiscalizar.libs.jpa.entities.keys;

import ar.com.singleton.fiscalizar.libs.jpa.entities.Users;
import ar.com.singleton.fiscalizar.libs.jpa.entities.VotingTables;
import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Basic;
import javax.persistence.Embeddable;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 *
 * @author pato
 */
@Embeddable
public class UserVotingTablesPK implements Serializable {

    @Basic
    @ManyToOne
    @JoinColumn(name = "idUser", insertable = false, updatable = false)
    private Users idUser;
    
    @Basic
    @ManyToOne
    @JoinColumn(name = "idVotingTable",insertable = false, updatable = false)
    private VotingTables idVotingTable;
    
    public UserVotingTablesPK() {
    }

    public UserVotingTablesPK(Users idUser, VotingTables idVotingTable) {
        this.idVotingTable = idVotingTable;
        this.idUser = idUser;
    }

    public Users getIdUser() {
        return idUser;
    }

    public void setIdUser(Users idUser) {
        this.idUser = idUser;
    }

    public VotingTables getIdVotingTable() {
        return idVotingTable;
    }

    public void setIdVotingTable(VotingTables idVotingTable) {
        this.idVotingTable = idVotingTable;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 67 * hash + Objects.hashCode(this.idUser);
        hash = 67 * hash + Objects.hashCode(this.idVotingTable);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final UserVotingTablesPK other = (UserVotingTablesPK) obj;
        if (!Objects.equals(this.idUser, other.idUser)) {
            return false;
        }
        return Objects.equals(this.idVotingTable, other.idVotingTable);
    }
    
}
