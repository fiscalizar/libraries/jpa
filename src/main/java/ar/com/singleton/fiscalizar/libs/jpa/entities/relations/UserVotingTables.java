/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ar.com.singleton.fiscalizar.libs.jpa.entities.relations;

import ar.com.singleton.fiscalizar.libs.jpa.entities.Users;
import ar.com.singleton.fiscalizar.libs.jpa.entities.VotingTables;
import ar.com.singleton.fiscalizar.libs.jpa.entities.keys.UserVotingTablesPK;
import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author pato
 */
@Entity
@Table(name = "UserVotingTables", catalog = "", schema = "")
@XmlRootElement
public class UserVotingTables implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected UserVotingTablesPK userVotingTablesPK;
    @Basic(optional = false)
    @Column(name = "createdAt")
    private String createdAt;
    @Column(name = "updatedAt")
    private String updatedAt;
    @Basic(optional = false)
    @Column(name = "deleted")
    private int deleted;
    @PrimaryKeyJoinColumn(name = "idUser", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Users idUser;
    @PrimaryKeyJoinColumn(name = "idVotingTable", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private VotingTables idVotingTable;

    public UserVotingTables() {
    }

    public UserVotingTables(UserVotingTablesPK userVotingTablesPK) {
        this.userVotingTablesPK = userVotingTablesPK;
    }

    public UserVotingTables(UserVotingTablesPK userVotingTablesPK, String createdAt, int deleted) {
        this.userVotingTablesPK = userVotingTablesPK;
        this.createdAt = createdAt;
        this.deleted = deleted;
    }

    public UserVotingTablesPK getUserVotingTablesPK() {
        return userVotingTablesPK;
    }

    public void setUserVotingTablesPK(UserVotingTablesPK userVotingTablesPK) {
        this.userVotingTablesPK = userVotingTablesPK;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public int getDeleted() {
        return deleted;
    }

    public void setDeleted(int deleted) {
        this.deleted = deleted;
    }

    public Users getIdUser() {
        return idUser;
    }

    public void setIdUser(Users idUser) {
        this.idUser = idUser;
    }

    public VotingTables getIdVotingTable() {
        return idVotingTable;
    }

    public void setIdVotingTable(VotingTables idVotingTable) {
        this.idVotingTable = idVotingTable;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (userVotingTablesPK != null ? userVotingTablesPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UserVotingTables)) {
            return false;
        }
        UserVotingTables other = (UserVotingTables) object;
        if ((this.userVotingTablesPK == null && other.userVotingTablesPK != null) || (this.userVotingTablesPK != null && !this.userVotingTablesPK.equals(other.userVotingTablesPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ar.com.singleton.app.fiscalizar.database.UserVotingTables[ userVotingTablesPK=" + userVotingTablesPK + " ]";
    }
    
}
