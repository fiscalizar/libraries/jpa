/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ar.com.singleton.fiscalizar.libs.jpa.controllers.entities;

import ar.com.singleton.fiscalizar.libs.jpa.controllers.exceptions.IllegalOrphanException;
import ar.com.singleton.fiscalizar.libs.jpa.controllers.exceptions.NonexistentEntityException;
import ar.com.singleton.fiscalizar.libs.jpa.controllers.exceptions.PreexistingEntityException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import ar.com.singleton.fiscalizar.libs.jpa.entities.Parties;
import java.util.ArrayList;
import java.util.List;
import ar.com.singleton.fiscalizar.libs.jpa.entities.Polls;
import ar.com.singleton.fiscalizar.libs.jpa.entities.relations.ElectionsParties;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author pato
 */
public class PartiesJpaController implements Serializable {

    public PartiesJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Parties parties) throws PreexistingEntityException, Exception {
        if (parties.getPartiesList() == null) {
            parties.setPartiesList(new ArrayList<Parties>());
        }
        if (parties.getPollsList() == null) {
            parties.setPollsList(new ArrayList<Polls>());
        }
        if (parties.getElectionsPartiesList() == null) {
            parties.setElectionsPartiesList(new ArrayList<ElectionsParties>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Parties supraPartyId = parties.getSupraPartyId();
            if (supraPartyId != null) {
                supraPartyId = em.getReference(supraPartyId.getClass(), supraPartyId.getId());
                parties.setSupraPartyId(supraPartyId);
            }
            List<Parties> attachedPartiesList = new ArrayList<Parties>();
            for (Parties partiesListPartiesToAttach : parties.getPartiesList()) {
                partiesListPartiesToAttach = em.getReference(partiesListPartiesToAttach.getClass(), partiesListPartiesToAttach.getId());
                attachedPartiesList.add(partiesListPartiesToAttach);
            }
            parties.setPartiesList(attachedPartiesList);
            List<Polls> attachedPollsList = new ArrayList<Polls>();
            for (Polls pollsListPollsToAttach : parties.getPollsList()) {
                pollsListPollsToAttach = em.getReference(pollsListPollsToAttach.getClass(), pollsListPollsToAttach.getId());
                attachedPollsList.add(pollsListPollsToAttach);
            }
            parties.setPollsList(attachedPollsList);
            List<ElectionsParties> attachedElectionsPartiesList = new ArrayList<ElectionsParties>();
            for (ElectionsParties electionsPartiesListElectionsPartiesToAttach : parties.getElectionsPartiesList()) {
                electionsPartiesListElectionsPartiesToAttach = em.getReference(electionsPartiesListElectionsPartiesToAttach.getClass(), electionsPartiesListElectionsPartiesToAttach.getElectionsPartiesPK());
                attachedElectionsPartiesList.add(electionsPartiesListElectionsPartiesToAttach);
            }
            parties.setElectionsPartiesList(attachedElectionsPartiesList);
            em.persist(parties);
            if (supraPartyId != null) {
                supraPartyId.getPartiesList().add(parties);
                supraPartyId = em.merge(supraPartyId);
            }
            for (Parties partiesListParties : parties.getPartiesList()) {
                Parties oldSupraPartyIdOfPartiesListParties = partiesListParties.getSupraPartyId();
                partiesListParties.setSupraPartyId(parties);
                partiesListParties = em.merge(partiesListParties);
                if (oldSupraPartyIdOfPartiesListParties != null) {
                    oldSupraPartyIdOfPartiesListParties.getPartiesList().remove(partiesListParties);
                    oldSupraPartyIdOfPartiesListParties = em.merge(oldSupraPartyIdOfPartiesListParties);
                }
            }
            for (Polls pollsListPolls : parties.getPollsList()) {
                Parties oldIdPartyOfPollsListPolls = pollsListPolls.getIdParty();
                pollsListPolls.setIdParty(parties);
                pollsListPolls = em.merge(pollsListPolls);
                if (oldIdPartyOfPollsListPolls != null) {
                    oldIdPartyOfPollsListPolls.getPollsList().remove(pollsListPolls);
                    oldIdPartyOfPollsListPolls = em.merge(oldIdPartyOfPollsListPolls);
                }
            }
            for (ElectionsParties electionsPartiesListElectionsParties : parties.getElectionsPartiesList()) {
                Parties oldIdPartyOfElectionsPartiesListElectionsParties = electionsPartiesListElectionsParties.getIdParty();
                electionsPartiesListElectionsParties.setIdParty(parties);
                electionsPartiesListElectionsParties = em.merge(electionsPartiesListElectionsParties);
                if (oldIdPartyOfElectionsPartiesListElectionsParties != null) {
                    oldIdPartyOfElectionsPartiesListElectionsParties.getElectionsPartiesList().remove(electionsPartiesListElectionsParties);
                    oldIdPartyOfElectionsPartiesListElectionsParties = em.merge(oldIdPartyOfElectionsPartiesListElectionsParties);
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findParties(parties.getId()) != null) {
                throw new PreexistingEntityException("Parties " + parties + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Parties parties) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Parties persistentParties = em.find(Parties.class, parties.getId());
            Parties supraPartyIdOld = persistentParties.getSupraPartyId();
            Parties supraPartyIdNew = parties.getSupraPartyId();
            List<Parties> partiesListOld = persistentParties.getPartiesList();
            List<Parties> partiesListNew = parties.getPartiesList();
            List<Polls> pollsListOld = persistentParties.getPollsList();
            List<Polls> pollsListNew = parties.getPollsList();
            List<ElectionsParties> electionsPartiesListOld = persistentParties.getElectionsPartiesList();
            List<ElectionsParties> electionsPartiesListNew = parties.getElectionsPartiesList();
            List<String> illegalOrphanMessages = null;
            for (Polls pollsListOldPolls : pollsListOld) {
                if (!pollsListNew.contains(pollsListOldPolls)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Polls " + pollsListOldPolls + " since its idParty field is not nullable.");
                }
            }
            for (ElectionsParties electionsPartiesListOldElectionsParties : electionsPartiesListOld) {
                if (!electionsPartiesListNew.contains(electionsPartiesListOldElectionsParties)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain ElectionsParties " + electionsPartiesListOldElectionsParties + " since its idParty field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            if (supraPartyIdNew != null) {
                supraPartyIdNew = em.getReference(supraPartyIdNew.getClass(), supraPartyIdNew.getId());
                parties.setSupraPartyId(supraPartyIdNew);
            }
            List<Parties> attachedPartiesListNew = new ArrayList<Parties>();
            for (Parties partiesListNewPartiesToAttach : partiesListNew) {
                partiesListNewPartiesToAttach = em.getReference(partiesListNewPartiesToAttach.getClass(), partiesListNewPartiesToAttach.getId());
                attachedPartiesListNew.add(partiesListNewPartiesToAttach);
            }
            partiesListNew = attachedPartiesListNew;
            parties.setPartiesList(partiesListNew);
            List<Polls> attachedPollsListNew = new ArrayList<Polls>();
            for (Polls pollsListNewPollsToAttach : pollsListNew) {
                pollsListNewPollsToAttach = em.getReference(pollsListNewPollsToAttach.getClass(), pollsListNewPollsToAttach.getId());
                attachedPollsListNew.add(pollsListNewPollsToAttach);
            }
            pollsListNew = attachedPollsListNew;
            parties.setPollsList(pollsListNew);
            List<ElectionsParties> attachedElectionsPartiesListNew = new ArrayList<ElectionsParties>();
            for (ElectionsParties electionsPartiesListNewElectionsPartiesToAttach : electionsPartiesListNew) {
                electionsPartiesListNewElectionsPartiesToAttach = em.getReference(electionsPartiesListNewElectionsPartiesToAttach.getClass(), electionsPartiesListNewElectionsPartiesToAttach.getElectionsPartiesPK());
                attachedElectionsPartiesListNew.add(electionsPartiesListNewElectionsPartiesToAttach);
            }
            electionsPartiesListNew = attachedElectionsPartiesListNew;
            parties.setElectionsPartiesList(electionsPartiesListNew);
            parties = em.merge(parties);
            if (supraPartyIdOld != null && !supraPartyIdOld.equals(supraPartyIdNew)) {
                supraPartyIdOld.getPartiesList().remove(parties);
                supraPartyIdOld = em.merge(supraPartyIdOld);
            }
            if (supraPartyIdNew != null && !supraPartyIdNew.equals(supraPartyIdOld)) {
                supraPartyIdNew.getPartiesList().add(parties);
                supraPartyIdNew = em.merge(supraPartyIdNew);
            }
            for (Parties partiesListOldParties : partiesListOld) {
                if (!partiesListNew.contains(partiesListOldParties)) {
                    partiesListOldParties.setSupraPartyId(null);
                    partiesListOldParties = em.merge(partiesListOldParties);
                }
            }
            for (Parties partiesListNewParties : partiesListNew) {
                if (!partiesListOld.contains(partiesListNewParties)) {
                    Parties oldSupraPartyIdOfPartiesListNewParties = partiesListNewParties.getSupraPartyId();
                    partiesListNewParties.setSupraPartyId(parties);
                    partiesListNewParties = em.merge(partiesListNewParties);
                    if (oldSupraPartyIdOfPartiesListNewParties != null && !oldSupraPartyIdOfPartiesListNewParties.equals(parties)) {
                        oldSupraPartyIdOfPartiesListNewParties.getPartiesList().remove(partiesListNewParties);
                        oldSupraPartyIdOfPartiesListNewParties = em.merge(oldSupraPartyIdOfPartiesListNewParties);
                    }
                }
            }
            for (Polls pollsListNewPolls : pollsListNew) {
                if (!pollsListOld.contains(pollsListNewPolls)) {
                    Parties oldIdPartyOfPollsListNewPolls = pollsListNewPolls.getIdParty();
                    pollsListNewPolls.setIdParty(parties);
                    pollsListNewPolls = em.merge(pollsListNewPolls);
                    if (oldIdPartyOfPollsListNewPolls != null && !oldIdPartyOfPollsListNewPolls.equals(parties)) {
                        oldIdPartyOfPollsListNewPolls.getPollsList().remove(pollsListNewPolls);
                        oldIdPartyOfPollsListNewPolls = em.merge(oldIdPartyOfPollsListNewPolls);
                    }
                }
            }
            for (ElectionsParties electionsPartiesListNewElectionsParties : electionsPartiesListNew) {
                if (!electionsPartiesListOld.contains(electionsPartiesListNewElectionsParties)) {
                    Parties oldIdPartyOfElectionsPartiesListNewElectionsParties = electionsPartiesListNewElectionsParties.getIdParty();
                    electionsPartiesListNewElectionsParties.setIdParty(parties);
                    electionsPartiesListNewElectionsParties = em.merge(electionsPartiesListNewElectionsParties);
                    if (oldIdPartyOfElectionsPartiesListNewElectionsParties != null && !oldIdPartyOfElectionsPartiesListNewElectionsParties.equals(parties)) {
                        oldIdPartyOfElectionsPartiesListNewElectionsParties.getElectionsPartiesList().remove(electionsPartiesListNewElectionsParties);
                        oldIdPartyOfElectionsPartiesListNewElectionsParties = em.merge(oldIdPartyOfElectionsPartiesListNewElectionsParties);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = parties.getId();
                if (findParties(id) == null) {
                    throw new NonexistentEntityException("The parties with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Parties parties;
            try {
                parties = em.getReference(Parties.class, id);
                parties.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The parties with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            List<Polls> pollsListOrphanCheck = parties.getPollsList();
            for (Polls pollsListOrphanCheckPolls : pollsListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Parties (" + parties + ") cannot be destroyed since the Polls " + pollsListOrphanCheckPolls + " in its pollsList field has a non-nullable idParty field.");
            }
            List<ElectionsParties> electionsPartiesListOrphanCheck = parties.getElectionsPartiesList();
            for (ElectionsParties electionsPartiesListOrphanCheckElectionsParties : electionsPartiesListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Parties (" + parties + ") cannot be destroyed since the ElectionsParties " + electionsPartiesListOrphanCheckElectionsParties + " in its electionsPartiesList field has a non-nullable idParty field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            Parties supraPartyId = parties.getSupraPartyId();
            if (supraPartyId != null) {
                supraPartyId.getPartiesList().remove(parties);
                supraPartyId = em.merge(supraPartyId);
            }
            List<Parties> partiesList = parties.getPartiesList();
            for (Parties partiesListParties : partiesList) {
                partiesListParties.setSupraPartyId(null);
                partiesListParties = em.merge(partiesListParties);
            }
            em.remove(parties);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Parties> findPartiesEntities() {
        return findPartiesEntities(true, -1, -1);
    }

    public List<Parties> findPartiesEntities(int maxResults, int firstResult) {
        return findPartiesEntities(false, maxResults, firstResult);
    }

    private List<Parties> findPartiesEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Parties.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Parties findParties(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Parties.class, id);
        } finally {
            em.close();
        }
    }

    public int getPartiesCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Parties> rt = cq.from(Parties.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
