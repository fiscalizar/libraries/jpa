/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ar.com.singleton.fiscalizar.libs.jpa.views.results;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author pato
 */
@MappedSuperclass
@Table(name = "R_ResultsByInstitutions", catalog = "", schema = "")
@XmlRootElement
public class RResultsByInstitutions implements Serializable {

    private static final long serialVersionUID = 1L;
    @Column(name = "idElection")
    private Integer idElection;
    @Column(name = "idCategory")
    private Integer idCategory;
    @Column(name = "idParty")
    private Integer idParty;
    @Column(name = "idInstitution")
    private Integer idInstitution;
    @Column(name = "votedTables")
    private String votedTables;
    @Column(name = "votedElectors")
    private String votedElectors;
    @Column(name = "votes")
    private String votes;
    @Column(name = "partyPercentage")
    private String partyPercentage;
    @Column(name = "percentageVotedByTable")
    private String percentageVotedByTable;
    @Column(name = "percentageVotedByElectors")
    private String percentageVotedByElectors;

    public RResultsByInstitutions() {
    }

    public Integer getIdElection() {
        return idElection;
    }

    public void setIdElection(Integer idElection) {
        this.idElection = idElection;
    }

    public Integer getIdCategory() {
        return idCategory;
    }

    public void setIdCategory(Integer idCategory) {
        this.idCategory = idCategory;
    }

    public Integer getIdParty() {
        return idParty;
    }

    public void setIdParty(Integer idParty) {
        this.idParty = idParty;
    }

    public Integer getIdInstitution() {
        return idInstitution;
    }

    public void setIdInstitution(Integer idInstitution) {
        this.idInstitution = idInstitution;
    }

    public String getVotedTables() {
        return votedTables;
    }

    public void setVotedTables(String votedTables) {
        this.votedTables = votedTables;
    }

    public String getVotedElectors() {
        return votedElectors;
    }

    public void setVotedElectors(String votedElectors) {
        this.votedElectors = votedElectors;
    }

    public String getVotes() {
        return votes;
    }

    public void setVotes(String votes) {
        this.votes = votes;
    }

    public String getPartyPercentage() {
        return partyPercentage;
    }

    public void setPartyPercentage(String partyPercentage) {
        this.partyPercentage = partyPercentage;
    }

    public String getPercentageVotedByTable() {
        return percentageVotedByTable;
    }

    public void setPercentageVotedByTable(String percentageVotedByTable) {
        this.percentageVotedByTable = percentageVotedByTable;
    }

    public String getPercentageVotedByElectors() {
        return percentageVotedByElectors;
    }

    public void setPercentageVotedByElectors(String percentageVotedByElectors) {
        this.percentageVotedByElectors = percentageVotedByElectors;
    }
    
}
