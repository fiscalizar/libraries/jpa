/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ar.com.singleton.fiscalizar.libs.jpa.entities.relations;

import ar.com.singleton.fiscalizar.libs.jpa.entities.Categories;
import ar.com.singleton.fiscalizar.libs.jpa.entities.Elections;
import ar.com.singleton.fiscalizar.libs.jpa.entities.keys.ElectionsCategoriesPK;
import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author pato
 */
@Entity
@Table(name = "ElectionsCategories", catalog = "", schema = "")
@XmlRootElement
public class ElectionsCategories implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected ElectionsCategoriesPK electionsCategoriesPK;
    @Basic(optional = false)
    @Column(name = "createdAt")
    private String createdAt;
    @Column(name = "updatedAt")
    private String updatedAt;
    @Basic(optional = false)
    @Column(name = "deleted")
    private int deleted;
    @PrimaryKeyJoinColumn(name = "idCategory", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Categories idCategory;
    @PrimaryKeyJoinColumn(name = "idElection", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Elections idElection;

    public ElectionsCategories() {
    }

    public ElectionsCategories(ElectionsCategoriesPK electionsCategoriesPK) {
        this.electionsCategoriesPK = electionsCategoriesPK;
    }

    public ElectionsCategories(ElectionsCategoriesPK electionsCategoriesPK, String createdAt, int deleted) {
        this.electionsCategoriesPK = electionsCategoriesPK;
        this.createdAt = createdAt;
        this.deleted = deleted;
    }

    public ElectionsCategoriesPK getElectionsCategoriesPK() {
        return electionsCategoriesPK;
    }

    public void setElectionsCategoriesPK(ElectionsCategoriesPK electionsCategoriesPK) {
        this.electionsCategoriesPK = electionsCategoriesPK;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public int getDeleted() {
        return deleted;
    }

    public void setDeleted(int deleted) {
        this.deleted = deleted;
    }

    public Categories getIdCategory() {
        return idCategory;
    }

    public void setIdCategory(Categories idCategory) {
        this.idCategory = idCategory;
    }

    public Elections getIdElection() {
        return idElection;
    }

    public void setIdElection(Elections idElection) {
        this.idElection = idElection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (electionsCategoriesPK != null ? electionsCategoriesPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ElectionsCategories)) {
            return false;
        }
        ElectionsCategories other = (ElectionsCategories) object;
        if ((this.electionsCategoriesPK == null && other.electionsCategoriesPK != null) || (this.electionsCategoriesPK != null && !this.electionsCategoriesPK.equals(other.electionsCategoriesPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ar.com.singleton.app.fiscalizar.database.ElectionsCategories[ electionsCategoriesPK=" + electionsCategoriesPK + " ]";
    }
    
}
