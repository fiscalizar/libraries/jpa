/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ar.com.singleton.fiscalizar.libs.jpa.entities.relations;

import ar.com.singleton.fiscalizar.libs.jpa.entities.Elections;
import ar.com.singleton.fiscalizar.libs.jpa.entities.Sections;
import ar.com.singleton.fiscalizar.libs.jpa.entities.keys.ElectionsSectionsPK;
import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author pato
 */
@Entity
@Table(name = "ElectionsSections", catalog = "", schema = "")
@XmlRootElement
public class ElectionsSections implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected ElectionsSectionsPK electionsSectionsPK;
    @Basic(optional = false)
    @Column(name = "createdAt")
    private String createdAt;
    @Column(name = "updatedAt")
    private String updatedAt;
    @Basic(optional = false)
    @Column(name = "deleted")
    private int deleted;
    @PrimaryKeyJoinColumn(name = "idSection", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Sections idSection;
    @PrimaryKeyJoinColumn(name = "idElection", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Elections idElection;

    public ElectionsSections() {
    }

    public ElectionsSections(ElectionsSectionsPK electionsSectionsPK) {
        this.electionsSectionsPK = electionsSectionsPK;
    }

    public ElectionsSections(ElectionsSectionsPK electionsSectionsPK, String createdAt, int deleted) {
        this.electionsSectionsPK = electionsSectionsPK;
        this.createdAt = createdAt;
        this.deleted = deleted;
    }

    public ElectionsSectionsPK getElectionsSectionsPK() {
        return electionsSectionsPK;
    }

    public void setElectionsSectionsPK(ElectionsSectionsPK electionsSectionsPK) {
        this.electionsSectionsPK = electionsSectionsPK;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public int getDeleted() {
        return deleted;
    }

    public void setDeleted(int deleted) {
        this.deleted = deleted;
    }

    public Sections getIdSection() {
        return idSection;
    }

    public void setIdSection(Sections idSection) {
        this.idSection = idSection;
    }

    public Elections getIdElection() {
        return idElection;
    }

    public void setIdElection(Elections idElection) {
        this.idElection = idElection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (electionsSectionsPK != null ? electionsSectionsPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ElectionsSections)) {
            return false;
        }
        ElectionsSections other = (ElectionsSections) object;
        if ((this.electionsSectionsPK == null && other.electionsSectionsPK != null) || (this.electionsSectionsPK != null && !this.electionsSectionsPK.equals(other.electionsSectionsPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ar.com.singleton.app.fiscalizar.database.ElectionsSections[ electionsSectionsPK=" + electionsSectionsPK + " ]";
    }
    
}
