/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ar.com.singleton.fiscalizar.libs.jpa.entities.keys;

import ar.com.singleton.fiscalizar.libs.jpa.entities.Elections;
import ar.com.singleton.fiscalizar.libs.jpa.entities.Parties;
import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Basic;
import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 *
 * @author pato
 */
@Embeddable
public class ElectionsPartiesPK implements Serializable {

    @Basic
    @ManyToOne
    @JoinColumn(name = "idElection", insertable = false, updatable = false)
    private Elections idElection;
    
    @Basic
    @ManyToOne
    @JoinColumn(name = "idParty", insertable = false, updatable = false)
    private Parties idParty;
    
    public ElectionsPartiesPK() {
    }

    public ElectionsPartiesPK(Elections idElection, Parties idParty) {
        this.idParty = idParty;
        this.idElection = idElection;
    }
    
    @Override
    public int hashCode() {
        int hash = 5;
        hash = 67 * hash + Objects.hashCode(this.idElection);
        hash = 67 * hash + Objects.hashCode(this.idParty);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ElectionsPartiesPK other = (ElectionsPartiesPK) obj;
        if (!Objects.equals(this.idElection, other.idElection)) {
            return false;
        }
        return Objects.equals(this.idParty, other.idParty);
    }

    public Elections getIdElection() {
        return idElection;
    }

    public void setIdElection(Elections idElection) {
        this.idElection = idElection;
    }

    public Parties getIdParty() {
        return idParty;
    }

    public void setIdParty(Parties idParty) {
        this.idParty = idParty;
    }
    
}
