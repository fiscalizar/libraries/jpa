/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ar.com.singleton.fiscalizar.libs.jpa.entities.keys;

import ar.com.singleton.fiscalizar.libs.jpa.entities.Circuits;
import ar.com.singleton.fiscalizar.libs.jpa.entities.Users;
import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Basic;
import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 *
 * @author pato
 */
@Embeddable
public class UserCircuitsPK implements Serializable {

    @Basic
    @ManyToOne
    @JoinColumn(name = "idUser", insertable = false, updatable = false)
    private Users idUser;
    
    @Basic
    @ManyToOne
    @JoinColumn(name = "idCircuit",insertable = false, updatable = false)
    private Circuits idCircuit;
    
    public UserCircuitsPK(Users idUser, Circuits idCircuit) {
        this.idCircuit = idCircuit;
        this.idUser = idUser;
    }

    public UserCircuitsPK() {
    }

    public Users getIdUser() {
        return idUser;
    }

    public void setIdUser(Users idUser) {
        this.idUser = idUser;
    }

    public Circuits getIdCircuit() {
        return idCircuit;
    }

    public void setIdCircuit(Circuits idCircuit) {
        this.idCircuit = idCircuit;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 67 * hash + Objects.hashCode(this.idUser);
        hash = 67 * hash + Objects.hashCode(this.idCircuit);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final UserCircuitsPK other = (UserCircuitsPK) obj;
        if (!Objects.equals(this.idUser, other.idUser)) {
            return false;
        }
        return Objects.equals(this.idCircuit, other.idCircuit);
    }
    
}
