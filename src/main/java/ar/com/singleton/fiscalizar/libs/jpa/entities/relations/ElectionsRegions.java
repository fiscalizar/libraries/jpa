/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ar.com.singleton.fiscalizar.libs.jpa.entities.relations;

import ar.com.singleton.fiscalizar.libs.jpa.entities.Elections;
import ar.com.singleton.fiscalizar.libs.jpa.entities.Regions;
import ar.com.singleton.fiscalizar.libs.jpa.entities.keys.ElectionsRegionsPK;
import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author pato
 */
@Entity
@Table(name = "ElectionsRegions", catalog = "", schema = "")
@XmlRootElement
public class ElectionsRegions implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected ElectionsRegionsPK electionsRegionsPK;
    @Basic(optional = false)
    @Column(name = "createdAt")
    private String createdAt;
    @Column(name = "updatedAt")
    private String updatedAt;
    @Basic(optional = false)
    @Column(name = "deleted")
    private int deleted;
    @PrimaryKeyJoinColumn(name = "idElection", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Elections idElection;
    @PrimaryKeyJoinColumn(name = "idRegion", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Regions idRegion;

    public ElectionsRegions() {
    }

    public ElectionsRegions(ElectionsRegionsPK electionsRegionsPK) {
        this.electionsRegionsPK = electionsRegionsPK;
    }

    public ElectionsRegions(ElectionsRegionsPK electionsRegionsPK, String createdAt, int deleted) {
        this.electionsRegionsPK = electionsRegionsPK;
        this.createdAt = createdAt;
        this.deleted = deleted;
    }

    public ElectionsRegionsPK getElectionsRegionsPK() {
        return electionsRegionsPK;
    }

    public void setElectionsRegionsPK(ElectionsRegionsPK electionsRegionsPK) {
        this.electionsRegionsPK = electionsRegionsPK;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public int getDeleted() {
        return deleted;
    }

    public void setDeleted(int deleted) {
        this.deleted = deleted;
    }

    public Elections getIdElection() {
        return idElection;
    }

    public void setIdElection(Elections idElection) {
        this.idElection = idElection;
    }

    public Regions getIdRegion() {
        return idRegion;
    }

    public void setIdRegion(Regions idRegion) {
        this.idRegion = idRegion;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (electionsRegionsPK != null ? electionsRegionsPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ElectionsRegions)) {
            return false;
        }
        ElectionsRegions other = (ElectionsRegions) object;
        if ((this.electionsRegionsPK == null && other.electionsRegionsPK != null) || (this.electionsRegionsPK != null && !this.electionsRegionsPK.equals(other.electionsRegionsPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ar.com.singleton.app.fiscalizar.database.ElectionsRegions[ electionsRegionsPK=" + electionsRegionsPK + " ]";
    }
    
}
