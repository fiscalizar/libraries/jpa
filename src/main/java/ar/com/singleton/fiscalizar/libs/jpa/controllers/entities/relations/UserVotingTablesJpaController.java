/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ar.com.singleton.fiscalizar.libs.jpa.controllers.entities.relations;

import ar.com.singleton.fiscalizar.libs.jpa.controllers.exceptions.NonexistentEntityException;
import ar.com.singleton.fiscalizar.libs.jpa.controllers.exceptions.PreexistingEntityException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import ar.com.singleton.fiscalizar.libs.jpa.entities.Users;
import ar.com.singleton.fiscalizar.libs.jpa.entities.VotingTables;
import ar.com.singleton.fiscalizar.libs.jpa.entities.keys.UserVotingTablesPK;
import ar.com.singleton.fiscalizar.libs.jpa.entities.relations.UserVotingTables;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author pato
 */
public class UserVotingTablesJpaController implements Serializable {

    public UserVotingTablesJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(UserVotingTables userVotingTables) throws PreexistingEntityException, Exception {
        if (userVotingTables.getUserVotingTablesPK() == null) {
            userVotingTables.setUserVotingTablesPK(new UserVotingTablesPK());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Users idUser = userVotingTables.getIdUser();
            if (idUser != null) {
                idUser = em.getReference(idUser.getClass(), idUser.getId());
                userVotingTables.setIdUser(idUser);
            }
            VotingTables idVotingTable = userVotingTables.getIdVotingTable();
            if (idVotingTable != null) {
                idVotingTable = em.getReference(idVotingTable.getClass(), idVotingTable.getId());
                userVotingTables.setIdVotingTable(idVotingTable);
            }
            em.persist(userVotingTables);
            if (idUser != null) {
                idUser.getUserVotingTablesList().add(userVotingTables);
                idUser = em.merge(idUser);
            }
            if (idVotingTable != null) {
                idVotingTable.getUserVotingTablesList().add(userVotingTables);
                idVotingTable = em.merge(idVotingTable);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findUserVotingTables(userVotingTables.getUserVotingTablesPK()) != null) {
                throw new PreexistingEntityException("UserVotingTables " + userVotingTables + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(UserVotingTables userVotingTables) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            UserVotingTables persistentUserVotingTables = em.find(UserVotingTables.class, userVotingTables.getUserVotingTablesPK());
            Users idUserOld = persistentUserVotingTables.getIdUser();
            Users idUserNew = userVotingTables.getIdUser();
            VotingTables idVotingTableOld = persistentUserVotingTables.getIdVotingTable();
            VotingTables idVotingTableNew = userVotingTables.getIdVotingTable();
            if (idUserNew != null) {
                idUserNew = em.getReference(idUserNew.getClass(), idUserNew.getId());
                userVotingTables.setIdUser(idUserNew);
            }
            if (idVotingTableNew != null) {
                idVotingTableNew = em.getReference(idVotingTableNew.getClass(), idVotingTableNew.getId());
                userVotingTables.setIdVotingTable(idVotingTableNew);
            }
            userVotingTables = em.merge(userVotingTables);
            if (idUserOld != null && !idUserOld.equals(idUserNew)) {
                idUserOld.getUserVotingTablesList().remove(userVotingTables);
                idUserOld = em.merge(idUserOld);
            }
            if (idUserNew != null && !idUserNew.equals(idUserOld)) {
                idUserNew.getUserVotingTablesList().add(userVotingTables);
                idUserNew = em.merge(idUserNew);
            }
            if (idVotingTableOld != null && !idVotingTableOld.equals(idVotingTableNew)) {
                idVotingTableOld.getUserVotingTablesList().remove(userVotingTables);
                idVotingTableOld = em.merge(idVotingTableOld);
            }
            if (idVotingTableNew != null && !idVotingTableNew.equals(idVotingTableOld)) {
                idVotingTableNew.getUserVotingTablesList().add(userVotingTables);
                idVotingTableNew = em.merge(idVotingTableNew);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                UserVotingTablesPK id = userVotingTables.getUserVotingTablesPK();
                if (findUserVotingTables(id) == null) {
                    throw new NonexistentEntityException("The userVotingTables with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(UserVotingTablesPK id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            UserVotingTables userVotingTables;
            try {
                userVotingTables = em.getReference(UserVotingTables.class, id);
                userVotingTables.getUserVotingTablesPK();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The userVotingTables with id " + id + " no longer exists.", enfe);
            }
            Users idUser = userVotingTables.getIdUser();
            if (idUser != null) {
                idUser.getUserVotingTablesList().remove(userVotingTables);
                idUser = em.merge(idUser);
            }
            VotingTables idVotingTable = userVotingTables.getIdVotingTable();
            if (idVotingTable != null) {
                idVotingTable.getUserVotingTablesList().remove(userVotingTables);
                idVotingTable = em.merge(idVotingTable);
            }
            em.remove(userVotingTables);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<UserVotingTables> findUserVotingTablesEntities() {
        return findUserVotingTablesEntities(true, -1, -1);
    }

    public List<UserVotingTables> findUserVotingTablesEntities(int maxResults, int firstResult) {
        return findUserVotingTablesEntities(false, maxResults, firstResult);
    }

    private List<UserVotingTables> findUserVotingTablesEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(UserVotingTables.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public UserVotingTables findUserVotingTables(UserVotingTablesPK id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(UserVotingTables.class, id);
        } finally {
            em.close();
        }
    }

    public int getUserVotingTablesCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<UserVotingTables> rt = cq.from(UserVotingTables.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
