/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ar.com.singleton.fiscalizar.libs.jpa.entities;

import ar.com.singleton.fiscalizar.libs.jpa.entities.relations.UserInstitutions;
import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author pato
 */
@Entity
@Table(name = "Institutions", catalog = "", schema = "")
@XmlRootElement
public class Institutions implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "name")
    private String name;
    @Basic(optional = false)
    @Column(name = "address")
    private String address;
    @Column(name = "locationLat")
    private String locationLat;
    @Column(name = "locationLong")
    private String locationLong;
    @Basic(optional = false)
    @Column(name = "createdAt")
    private String createdAt;
    @Column(name = "updatedAt")
    private String updatedAt;
    @Basic(optional = false)
    @Column(name = "deleted")
    private int deleted;
    @JoinColumn(name = "idCircuit", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Circuits idCircuit;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idInstitution")
    private List<VotingTables> votingTablesList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idUserInstitution")
    private List<UserInstitutions> userInstitutionsList;

    public Institutions() {
    }

    public Institutions(Integer id) {
        this.id = id;
    }

    public Institutions(Integer id, String name, String address, String createdAt, int deleted) {
        this.id = id;
        this.name = name;
        this.address = address;
        this.createdAt = createdAt;
        this.deleted = deleted;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getLocationLat() {
        return locationLat;
    }

    public void setLocationLat(String locationLat) {
        this.locationLat = locationLat;
    }

    public String getLocationLong() {
        return locationLong;
    }

    public void setLocationLong(String locationLong) {
        this.locationLong = locationLong;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public int getDeleted() {
        return deleted;
    }

    public void setDeleted(int deleted) {
        this.deleted = deleted;
    }

    public Circuits getIdCircuit() {
        return idCircuit;
    }

    public void setIdCircuit(Circuits idCircuit) {
        this.idCircuit = idCircuit;
    }

    @XmlTransient
    public List<VotingTables> getVotingTablesList() {
        return votingTablesList;
    }

    public void setVotingTablesList(List<VotingTables> votingTablesList) {
        this.votingTablesList = votingTablesList;
    }

    @XmlTransient
    public List<UserInstitutions> getUserInstitutionsList() {
        return userInstitutionsList;
    }

    public void setUserInstitutionsList(List<UserInstitutions> userInstitutionsList) {
        this.userInstitutionsList = userInstitutionsList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Institutions)) {
            return false;
        }
        Institutions other = (Institutions) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ar.com.singleton.app.fiscalizar.database.Institutions[ id=" + id + " ]";
    }
    
}
