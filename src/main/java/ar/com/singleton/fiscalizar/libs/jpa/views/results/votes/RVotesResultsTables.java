/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ar.com.singleton.fiscalizar.libs.jpa.views.results.votes;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author pato
 */
@MappedSuperclass
@Table(name = "R_VotesResultsTables", catalog = "", schema = "")
@XmlRootElement
public class RVotesResultsTables implements Serializable {

    private static final long serialVersionUID = 1L;
    @Column(name = "idElection")
    private Integer idElection;
    @Column(name = "idVotingTable")
    private Integer idVotingTable;

    public RVotesResultsTables() {
    }

    public Integer getIdElection() {
        return idElection;
    }

    public void setIdElection(Integer idElection) {
        this.idElection = idElection;
    }

    public Integer getIdVotingTable() {
        return idVotingTable;
    }

    public void setIdVotingTable(Integer idVotingTable) {
        this.idVotingTable = idVotingTable;
    }
    
}
