/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ar.com.singleton.fiscalizar.libs.jpa.entities.keys;

import ar.com.singleton.fiscalizar.libs.jpa.entities.Elections;
import ar.com.singleton.fiscalizar.libs.jpa.entities.Sections;
import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Basic;
import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 *
 * @author pato
 */
@Embeddable
public class ElectionsSectionsPK implements Serializable {

    @Basic
    @ManyToOne
    @JoinColumn(name = "idElection", insertable = false, updatable = false)
    private Elections idElection;
    
    @Basic
    @ManyToOne
    @JoinColumn(name = "idSection",insertable = false, updatable = false)
    private Sections idSection;
    
    public ElectionsSectionsPK() {
    }

    public ElectionsSectionsPK(Elections idElection, Sections idSection) {
        this.idSection = idSection;
        this.idElection = idElection;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 67 * hash + Objects.hashCode(this.idElection);
        hash = 67 * hash + Objects.hashCode(this.idSection);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ElectionsSectionsPK other = (ElectionsSectionsPK) obj;
        if (!Objects.equals(this.idElection, other.idElection)) {
            return false;
        }
        return Objects.equals(this.idSection, other.idSection);
    }
    
}
