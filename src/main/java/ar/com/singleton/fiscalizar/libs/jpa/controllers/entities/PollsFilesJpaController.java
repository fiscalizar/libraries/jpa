/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ar.com.singleton.fiscalizar.libs.jpa.controllers.entities;

import ar.com.singleton.fiscalizar.libs.jpa.controllers.exceptions.NonexistentEntityException;
import ar.com.singleton.fiscalizar.libs.jpa.controllers.exceptions.PreexistingEntityException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import ar.com.singleton.fiscalizar.libs.jpa.entities.Polls;
import ar.com.singleton.fiscalizar.libs.jpa.entities.PollsFiles;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author pato
 */
public class PollsFilesJpaController implements Serializable {

    public PollsFilesJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(PollsFiles pollsFiles) throws PreexistingEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Polls idPoll = pollsFiles.getIdPoll();
            if (idPoll != null) {
                idPoll = em.getReference(idPoll.getClass(), idPoll.getId());
                pollsFiles.setIdPoll(idPoll);
            }
            em.persist(pollsFiles);
            if (idPoll != null) {
                idPoll.getPollsFilesList().add(pollsFiles);
                idPoll = em.merge(idPoll);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findPollsFiles(pollsFiles.getId()) != null) {
                throw new PreexistingEntityException("PollsFiles " + pollsFiles + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(PollsFiles pollsFiles) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            PollsFiles persistentPollsFiles = em.find(PollsFiles.class, pollsFiles.getId());
            Polls idPollOld = persistentPollsFiles.getIdPoll();
            Polls idPollNew = pollsFiles.getIdPoll();
            if (idPollNew != null) {
                idPollNew = em.getReference(idPollNew.getClass(), idPollNew.getId());
                pollsFiles.setIdPoll(idPollNew);
            }
            pollsFiles = em.merge(pollsFiles);
            if (idPollOld != null && !idPollOld.equals(idPollNew)) {
                idPollOld.getPollsFilesList().remove(pollsFiles);
                idPollOld = em.merge(idPollOld);
            }
            if (idPollNew != null && !idPollNew.equals(idPollOld)) {
                idPollNew.getPollsFilesList().add(pollsFiles);
                idPollNew = em.merge(idPollNew);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = pollsFiles.getId();
                if (findPollsFiles(id) == null) {
                    throw new NonexistentEntityException("The pollsFiles with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            PollsFiles pollsFiles;
            try {
                pollsFiles = em.getReference(PollsFiles.class, id);
                pollsFiles.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The pollsFiles with id " + id + " no longer exists.", enfe);
            }
            Polls idPoll = pollsFiles.getIdPoll();
            if (idPoll != null) {
                idPoll.getPollsFilesList().remove(pollsFiles);
                idPoll = em.merge(idPoll);
            }
            em.remove(pollsFiles);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<PollsFiles> findPollsFilesEntities() {
        return findPollsFilesEntities(true, -1, -1);
    }

    public List<PollsFiles> findPollsFilesEntities(int maxResults, int firstResult) {
        return findPollsFilesEntities(false, maxResults, firstResult);
    }

    private List<PollsFiles> findPollsFilesEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(PollsFiles.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public PollsFiles findPollsFiles(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(PollsFiles.class, id);
        } finally {
            em.close();
        }
    }

    public int getPollsFilesCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<PollsFiles> rt = cq.from(PollsFiles.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
