/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ar.com.singleton.fiscalizar.libs.jpa.controllers.entities.relations;

import ar.com.singleton.fiscalizar.libs.jpa.controllers.exceptions.NonexistentEntityException;
import ar.com.singleton.fiscalizar.libs.jpa.controllers.exceptions.PreexistingEntityException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import ar.com.singleton.fiscalizar.libs.jpa.entities.Parties;
import ar.com.singleton.fiscalizar.libs.jpa.entities.Elections;
import ar.com.singleton.fiscalizar.libs.jpa.entities.keys.ElectionsPartiesPK;
import ar.com.singleton.fiscalizar.libs.jpa.entities.relations.ElectionsParties;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author pato
 */
public class ElectionsPartiesJpaController implements Serializable {

    public ElectionsPartiesJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(ElectionsParties electionsParties) throws PreexistingEntityException, Exception {
        if (electionsParties.getElectionsPartiesPK() == null) {
            electionsParties.setElectionsPartiesPK(new ElectionsPartiesPK());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Parties idParty = electionsParties.getIdParty();
            if (idParty != null) {
                idParty = em.getReference(idParty.getClass(), idParty.getId());
                electionsParties.setIdParty(idParty);
            }
            Elections idElection = electionsParties.getIdElection();
            if (idElection != null) {
                idElection = em.getReference(idElection.getClass(), idElection.getId());
                electionsParties.setIdElection(idElection);
            }
            em.persist(electionsParties);
            if (idParty != null) {
                idParty.getElectionsPartiesList().add(electionsParties);
                idParty = em.merge(idParty);
            }
            if (idElection != null) {
                idElection.getElectionsPartiesList().add(electionsParties);
                idElection = em.merge(idElection);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findElectionsParties(electionsParties.getElectionsPartiesPK()) != null) {
                throw new PreexistingEntityException("ElectionsParties " + electionsParties + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(ElectionsParties electionsParties) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            ElectionsParties persistentElectionsParties = em.find(ElectionsParties.class, electionsParties.getElectionsPartiesPK());
            Parties idPartyOld = persistentElectionsParties.getIdParty();
            Parties idPartyNew = electionsParties.getIdParty();
            Elections idElectionOld = persistentElectionsParties.getIdElection();
            Elections idElectionNew = electionsParties.getIdElection();
            if (idPartyNew != null) {
                idPartyNew = em.getReference(idPartyNew.getClass(), idPartyNew.getId());
                electionsParties.setIdParty(idPartyNew);
            }
            if (idElectionNew != null) {
                idElectionNew = em.getReference(idElectionNew.getClass(), idElectionNew.getId());
                electionsParties.setIdElection(idElectionNew);
            }
            electionsParties = em.merge(electionsParties);
            if (idPartyOld != null && !idPartyOld.equals(idPartyNew)) {
                idPartyOld.getElectionsPartiesList().remove(electionsParties);
                idPartyOld = em.merge(idPartyOld);
            }
            if (idPartyNew != null && !idPartyNew.equals(idPartyOld)) {
                idPartyNew.getElectionsPartiesList().add(electionsParties);
                idPartyNew = em.merge(idPartyNew);
            }
            if (idElectionOld != null && !idElectionOld.equals(idElectionNew)) {
                idElectionOld.getElectionsPartiesList().remove(electionsParties);
                idElectionOld = em.merge(idElectionOld);
            }
            if (idElectionNew != null && !idElectionNew.equals(idElectionOld)) {
                idElectionNew.getElectionsPartiesList().add(electionsParties);
                idElectionNew = em.merge(idElectionNew);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                ElectionsPartiesPK id = electionsParties.getElectionsPartiesPK();
                if (findElectionsParties(id) == null) {
                    throw new NonexistentEntityException("The electionsParties with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(ElectionsPartiesPK id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            ElectionsParties electionsParties;
            try {
                electionsParties = em.getReference(ElectionsParties.class, id);
                electionsParties.getElectionsPartiesPK();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The electionsParties with id " + id + " no longer exists.", enfe);
            }
            Parties idParty = electionsParties.getIdParty();
            if (idParty != null) {
                idParty.getElectionsPartiesList().remove(electionsParties);
                idParty = em.merge(idParty);
            }
            Elections idElection = electionsParties.getIdElection();
            if (idElection != null) {
                idElection.getElectionsPartiesList().remove(electionsParties);
                idElection = em.merge(idElection);
            }
            em.remove(electionsParties);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<ElectionsParties> findElectionsPartiesEntities() {
        return findElectionsPartiesEntities(true, -1, -1);
    }

    public List<ElectionsParties> findElectionsPartiesEntities(int maxResults, int firstResult) {
        return findElectionsPartiesEntities(false, maxResults, firstResult);
    }

    private List<ElectionsParties> findElectionsPartiesEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(ElectionsParties.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public ElectionsParties findElectionsParties(ElectionsPartiesPK id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(ElectionsParties.class, id);
        } finally {
            em.close();
        }
    }

    public int getElectionsPartiesCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<ElectionsParties> rt = cq.from(ElectionsParties.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
