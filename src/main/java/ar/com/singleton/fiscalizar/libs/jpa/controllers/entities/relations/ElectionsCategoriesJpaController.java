/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ar.com.singleton.fiscalizar.libs.jpa.controllers.entities.relations;

import ar.com.singleton.fiscalizar.libs.jpa.controllers.exceptions.NonexistentEntityException;
import ar.com.singleton.fiscalizar.libs.jpa.controllers.exceptions.PreexistingEntityException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import ar.com.singleton.fiscalizar.libs.jpa.entities.Categories;
import ar.com.singleton.fiscalizar.libs.jpa.entities.Elections;
import ar.com.singleton.fiscalizar.libs.jpa.entities.keys.ElectionsCategoriesPK;
import ar.com.singleton.fiscalizar.libs.jpa.entities.relations.ElectionsCategories;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author pato
 */
public class ElectionsCategoriesJpaController implements Serializable {

    public ElectionsCategoriesJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(ElectionsCategories electionsCategories) throws PreexistingEntityException, Exception {
        if (electionsCategories.getElectionsCategoriesPK() == null) {
            electionsCategories.setElectionsCategoriesPK(new ElectionsCategoriesPK());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Categories idCategory = electionsCategories.getIdCategory();
            if (idCategory != null) {
                idCategory = em.getReference(idCategory.getClass(), idCategory.getId());
                electionsCategories.setIdCategory(idCategory);
            }
            Elections idElection = electionsCategories.getIdElection();
            if (idElection != null) {
                idElection = em.getReference(idElection.getClass(), idElection.getId());
                electionsCategories.setIdElection(idElection);
            }
            em.persist(electionsCategories);
            if (idCategory != null) {
                idCategory.getElectionsCategoriesList().add(electionsCategories);
                idCategory = em.merge(idCategory);
            }
            if (idElection != null) {
                idElection.getElectionsCategoriesList().add(electionsCategories);
                idElection = em.merge(idElection);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findElectionsCategories(electionsCategories.getElectionsCategoriesPK()) != null) {
                throw new PreexistingEntityException("ElectionsCategories " + electionsCategories + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(ElectionsCategories electionsCategories) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            ElectionsCategories persistentElectionsCategories = em.find(ElectionsCategories.class, electionsCategories.getElectionsCategoriesPK());
            Categories idCategoryOld = persistentElectionsCategories.getIdCategory();
            Categories idCategoryNew = electionsCategories.getIdCategory();
            Elections idElectionOld = persistentElectionsCategories.getIdElection();
            Elections idElectionNew = electionsCategories.getIdElection();
            if (idCategoryNew != null) {
                idCategoryNew = em.getReference(idCategoryNew.getClass(), idCategoryNew.getId());
                electionsCategories.setIdCategory(idCategoryNew);
            }
            if (idElectionNew != null) {
                idElectionNew = em.getReference(idElectionNew.getClass(), idElectionNew.getId());
                electionsCategories.setIdElection(idElectionNew);
            }
            electionsCategories = em.merge(electionsCategories);
            if (idCategoryOld != null && !idCategoryOld.equals(idCategoryNew)) {
                idCategoryOld.getElectionsCategoriesList().remove(electionsCategories);
                idCategoryOld = em.merge(idCategoryOld);
            }
            if (idCategoryNew != null && !idCategoryNew.equals(idCategoryOld)) {
                idCategoryNew.getElectionsCategoriesList().add(electionsCategories);
                idCategoryNew = em.merge(idCategoryNew);
            }
            if (idElectionOld != null && !idElectionOld.equals(idElectionNew)) {
                idElectionOld.getElectionsCategoriesList().remove(electionsCategories);
                idElectionOld = em.merge(idElectionOld);
            }
            if (idElectionNew != null && !idElectionNew.equals(idElectionOld)) {
                idElectionNew.getElectionsCategoriesList().add(electionsCategories);
                idElectionNew = em.merge(idElectionNew);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                ElectionsCategoriesPK id = electionsCategories.getElectionsCategoriesPK();
                if (findElectionsCategories(id) == null) {
                    throw new NonexistentEntityException("The electionsCategories with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(ElectionsCategoriesPK id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            ElectionsCategories electionsCategories;
            try {
                electionsCategories = em.getReference(ElectionsCategories.class, id);
                electionsCategories.getElectionsCategoriesPK();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The electionsCategories with id " + id + " no longer exists.", enfe);
            }
            Categories idCategory = electionsCategories.getIdCategory();
            if (idCategory != null) {
                idCategory.getElectionsCategoriesList().remove(electionsCategories);
                idCategory = em.merge(idCategory);
            }
            Elections idElection = electionsCategories.getIdElection();
            if (idElection != null) {
                idElection.getElectionsCategoriesList().remove(electionsCategories);
                idElection = em.merge(idElection);
            }
            em.remove(electionsCategories);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<ElectionsCategories> findElectionsCategoriesEntities() {
        return findElectionsCategoriesEntities(true, -1, -1);
    }

    public List<ElectionsCategories> findElectionsCategoriesEntities(int maxResults, int firstResult) {
        return findElectionsCategoriesEntities(false, maxResults, firstResult);
    }

    private List<ElectionsCategories> findElectionsCategoriesEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(ElectionsCategories.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public ElectionsCategories findElectionsCategories(ElectionsCategoriesPK id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(ElectionsCategories.class, id);
        } finally {
            em.close();
        }
    }

    public int getElectionsCategoriesCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<ElectionsCategories> rt = cq.from(ElectionsCategories.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
