/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ar.com.singleton.fiscalizar.libs.jpa.controllers.entities;

import ar.com.singleton.fiscalizar.libs.jpa.controllers.exceptions.IllegalOrphanException;
import ar.com.singleton.fiscalizar.libs.jpa.controllers.exceptions.NonexistentEntityException;
import ar.com.singleton.fiscalizar.libs.jpa.controllers.exceptions.PreexistingEntityException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import ar.com.singleton.fiscalizar.libs.jpa.entities.relations.UserCircuits;
import java.util.ArrayList;
import java.util.List;
import ar.com.singleton.fiscalizar.libs.jpa.entities.relations.UserVotingTables;
import ar.com.singleton.fiscalizar.libs.jpa.entities.relations.UserRegions;
import ar.com.singleton.fiscalizar.libs.jpa.entities.relations.UserSections;
import ar.com.singleton.fiscalizar.libs.jpa.entities.relations.UserElections;
import ar.com.singleton.fiscalizar.libs.jpa.entities.relations.UserInstitutions;
import ar.com.singleton.fiscalizar.libs.jpa.entities.Polls;
import ar.com.singleton.fiscalizar.libs.jpa.entities.Users;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author pato
 */
public class UsersJpaController implements Serializable {

    public UsersJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Users users) throws PreexistingEntityException, Exception {
        if (users.getUserCircuitsList() == null) {
            users.setUserCircuitsList(new ArrayList<UserCircuits>());
        }
        if (users.getUserVotingTablesList() == null) {
            users.setUserVotingTablesList(new ArrayList<UserVotingTables>());
        }
        if (users.getUserRegionsList() == null) {
            users.setUserRegionsList(new ArrayList<UserRegions>());
        }
        if (users.getUserSectionsList() == null) {
            users.setUserSectionsList(new ArrayList<UserSections>());
        }
        if (users.getUserElectionsList() == null) {
            users.setUserElectionsList(new ArrayList<UserElections>());
        }
        if (users.getUserInstitutionsList() == null) {
            users.setUserInstitutionsList(new ArrayList<UserInstitutions>());
        }
        if (users.getPollsList() == null) {
            users.setPollsList(new ArrayList<Polls>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            List<UserCircuits> attachedUserCircuitsList = new ArrayList<UserCircuits>();
            for (UserCircuits userCircuitsListUserCircuitsToAttach : users.getUserCircuitsList()) {
                userCircuitsListUserCircuitsToAttach = em.getReference(userCircuitsListUserCircuitsToAttach.getClass(), userCircuitsListUserCircuitsToAttach.getUserCircuitsPK());
                attachedUserCircuitsList.add(userCircuitsListUserCircuitsToAttach);
            }
            users.setUserCircuitsList(attachedUserCircuitsList);
            List<UserVotingTables> attachedUserVotingTablesList = new ArrayList<UserVotingTables>();
            for (UserVotingTables userVotingTablesListUserVotingTablesToAttach : users.getUserVotingTablesList()) {
                userVotingTablesListUserVotingTablesToAttach = em.getReference(userVotingTablesListUserVotingTablesToAttach.getClass(), userVotingTablesListUserVotingTablesToAttach.getUserVotingTablesPK());
                attachedUserVotingTablesList.add(userVotingTablesListUserVotingTablesToAttach);
            }
            users.setUserVotingTablesList(attachedUserVotingTablesList);
            List<UserRegions> attachedUserRegionsList = new ArrayList<UserRegions>();
            for (UserRegions userRegionsListUserRegionsToAttach : users.getUserRegionsList()) {
                userRegionsListUserRegionsToAttach = em.getReference(userRegionsListUserRegionsToAttach.getClass(), userRegionsListUserRegionsToAttach.getUserRegionsPK());
                attachedUserRegionsList.add(userRegionsListUserRegionsToAttach);
            }
            users.setUserRegionsList(attachedUserRegionsList);
            List<UserSections> attachedUserSectionsList = new ArrayList<UserSections>();
            for (UserSections userSectionsListUserSectionsToAttach : users.getUserSectionsList()) {
                userSectionsListUserSectionsToAttach = em.getReference(userSectionsListUserSectionsToAttach.getClass(), userSectionsListUserSectionsToAttach.getUserSectionsPK());
                attachedUserSectionsList.add(userSectionsListUserSectionsToAttach);
            }
            users.setUserSectionsList(attachedUserSectionsList);
            List<UserElections> attachedUserElectionsList = new ArrayList<UserElections>();
            for (UserElections userElectionsListUserElectionsToAttach : users.getUserElectionsList()) {
                userElectionsListUserElectionsToAttach = em.getReference(userElectionsListUserElectionsToAttach.getClass(), userElectionsListUserElectionsToAttach.getUserElectionsPK());
                attachedUserElectionsList.add(userElectionsListUserElectionsToAttach);
            }
            users.setUserElectionsList(attachedUserElectionsList);
            List<UserInstitutions> attachedUserInstitutionsList = new ArrayList<UserInstitutions>();
            for (UserInstitutions userInstitutionsListUserInstitutionsToAttach : users.getUserInstitutionsList()) {
                userInstitutionsListUserInstitutionsToAttach = em.getReference(userInstitutionsListUserInstitutionsToAttach.getClass(), userInstitutionsListUserInstitutionsToAttach.getUserInstitutionsPK());
                attachedUserInstitutionsList.add(userInstitutionsListUserInstitutionsToAttach);
            }
            users.setUserInstitutionsList(attachedUserInstitutionsList);
            List<Polls> attachedPollsList = new ArrayList<Polls>();
            for (Polls pollsListPollsToAttach : users.getPollsList()) {
                pollsListPollsToAttach = em.getReference(pollsListPollsToAttach.getClass(), pollsListPollsToAttach.getId());
                attachedPollsList.add(pollsListPollsToAttach);
            }
            users.setPollsList(attachedPollsList);
            em.persist(users);
            for (UserCircuits userCircuitsListUserCircuits : users.getUserCircuitsList()) {
                Users oldIdUserOfUserCircuitsListUserCircuits = userCircuitsListUserCircuits.getIdUser();
                userCircuitsListUserCircuits.setIdUser(users);
                userCircuitsListUserCircuits = em.merge(userCircuitsListUserCircuits);
                if (oldIdUserOfUserCircuitsListUserCircuits != null) {
                    oldIdUserOfUserCircuitsListUserCircuits.getUserCircuitsList().remove(userCircuitsListUserCircuits);
                    oldIdUserOfUserCircuitsListUserCircuits = em.merge(oldIdUserOfUserCircuitsListUserCircuits);
                }
            }
            for (UserVotingTables userVotingTablesListUserVotingTables : users.getUserVotingTablesList()) {
                Users oldIdUserOfUserVotingTablesListUserVotingTables = userVotingTablesListUserVotingTables.getIdUser();
                userVotingTablesListUserVotingTables.setIdUser(users);
                userVotingTablesListUserVotingTables = em.merge(userVotingTablesListUserVotingTables);
                if (oldIdUserOfUserVotingTablesListUserVotingTables != null) {
                    oldIdUserOfUserVotingTablesListUserVotingTables.getUserVotingTablesList().remove(userVotingTablesListUserVotingTables);
                    oldIdUserOfUserVotingTablesListUserVotingTables = em.merge(oldIdUserOfUserVotingTablesListUserVotingTables);
                }
            }
            for (UserRegions userRegionsListUserRegions : users.getUserRegionsList()) {
                Users oldIdUserOfUserRegionsListUserRegions = userRegionsListUserRegions.getIdUser();
                userRegionsListUserRegions.setIdUser(users);
                userRegionsListUserRegions = em.merge(userRegionsListUserRegions);
                if (oldIdUserOfUserRegionsListUserRegions != null) {
                    oldIdUserOfUserRegionsListUserRegions.getUserRegionsList().remove(userRegionsListUserRegions);
                    oldIdUserOfUserRegionsListUserRegions = em.merge(oldIdUserOfUserRegionsListUserRegions);
                }
            }
            for (UserSections userSectionsListUserSections : users.getUserSectionsList()) {
                Users oldIdUserOfUserSectionsListUserSections = userSectionsListUserSections.getIdUser();
                userSectionsListUserSections.setIdUser(users);
                userSectionsListUserSections = em.merge(userSectionsListUserSections);
                if (oldIdUserOfUserSectionsListUserSections != null) {
                    oldIdUserOfUserSectionsListUserSections.getUserSectionsList().remove(userSectionsListUserSections);
                    oldIdUserOfUserSectionsListUserSections = em.merge(oldIdUserOfUserSectionsListUserSections);
                }
            }
            for (UserElections userElectionsListUserElections : users.getUserElectionsList()) {
                Users oldIdUserOfUserElectionsListUserElections = userElectionsListUserElections.getIdUser();
                userElectionsListUserElections.setIdUser(users);
                userElectionsListUserElections = em.merge(userElectionsListUserElections);
                if (oldIdUserOfUserElectionsListUserElections != null) {
                    oldIdUserOfUserElectionsListUserElections.getUserElectionsList().remove(userElectionsListUserElections);
                    oldIdUserOfUserElectionsListUserElections = em.merge(oldIdUserOfUserElectionsListUserElections);
                }
            }
            for (UserInstitutions userInstitutionsListUserInstitutions : users.getUserInstitutionsList()) {
                Users oldIdUserOfUserInstitutionsListUserInstitutions = userInstitutionsListUserInstitutions.getIdUser();
                userInstitutionsListUserInstitutions.setIdUser(users);
                userInstitutionsListUserInstitutions = em.merge(userInstitutionsListUserInstitutions);
                if (oldIdUserOfUserInstitutionsListUserInstitutions != null) {
                    oldIdUserOfUserInstitutionsListUserInstitutions.getUserInstitutionsList().remove(userInstitutionsListUserInstitutions);
                    oldIdUserOfUserInstitutionsListUserInstitutions = em.merge(oldIdUserOfUserInstitutionsListUserInstitutions);
                }
            }
            for (Polls pollsListPolls : users.getPollsList()) {
                Users oldIdUserOfPollsListPolls = pollsListPolls.getIdUser();
                pollsListPolls.setIdUser(users);
                pollsListPolls = em.merge(pollsListPolls);
                if (oldIdUserOfPollsListPolls != null) {
                    oldIdUserOfPollsListPolls.getPollsList().remove(pollsListPolls);
                    oldIdUserOfPollsListPolls = em.merge(oldIdUserOfPollsListPolls);
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findUsers(users.getId()) != null) {
                throw new PreexistingEntityException("Users " + users + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Users users) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Users persistentUsers = em.find(Users.class, users.getId());
            List<UserCircuits> userCircuitsListOld = persistentUsers.getUserCircuitsList();
            List<UserCircuits> userCircuitsListNew = users.getUserCircuitsList();
            List<UserVotingTables> userVotingTablesListOld = persistentUsers.getUserVotingTablesList();
            List<UserVotingTables> userVotingTablesListNew = users.getUserVotingTablesList();
            List<UserRegions> userRegionsListOld = persistentUsers.getUserRegionsList();
            List<UserRegions> userRegionsListNew = users.getUserRegionsList();
            List<UserSections> userSectionsListOld = persistentUsers.getUserSectionsList();
            List<UserSections> userSectionsListNew = users.getUserSectionsList();
            List<UserElections> userElectionsListOld = persistentUsers.getUserElectionsList();
            List<UserElections> userElectionsListNew = users.getUserElectionsList();
            List<UserInstitutions> userInstitutionsListOld = persistentUsers.getUserInstitutionsList();
            List<UserInstitutions> userInstitutionsListNew = users.getUserInstitutionsList();
            List<Polls> pollsListOld = persistentUsers.getPollsList();
            List<Polls> pollsListNew = users.getPollsList();
            List<String> illegalOrphanMessages = null;
            for (UserCircuits userCircuitsListOldUserCircuits : userCircuitsListOld) {
                if (!userCircuitsListNew.contains(userCircuitsListOldUserCircuits)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain UserCircuits " + userCircuitsListOldUserCircuits + " since its idUser field is not nullable.");
                }
            }
            for (UserVotingTables userVotingTablesListOldUserVotingTables : userVotingTablesListOld) {
                if (!userVotingTablesListNew.contains(userVotingTablesListOldUserVotingTables)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain UserVotingTables " + userVotingTablesListOldUserVotingTables + " since its idUser field is not nullable.");
                }
            }
            for (UserRegions userRegionsListOldUserRegions : userRegionsListOld) {
                if (!userRegionsListNew.contains(userRegionsListOldUserRegions)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain UserRegions " + userRegionsListOldUserRegions + " since its idUser field is not nullable.");
                }
            }
            for (UserSections userSectionsListOldUserSections : userSectionsListOld) {
                if (!userSectionsListNew.contains(userSectionsListOldUserSections)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain UserSections " + userSectionsListOldUserSections + " since its idUser field is not nullable.");
                }
            }
            for (UserElections userElectionsListOldUserElections : userElectionsListOld) {
                if (!userElectionsListNew.contains(userElectionsListOldUserElections)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain UserElections " + userElectionsListOldUserElections + " since its idUser field is not nullable.");
                }
            }
            for (UserInstitutions userInstitutionsListOldUserInstitutions : userInstitutionsListOld) {
                if (!userInstitutionsListNew.contains(userInstitutionsListOldUserInstitutions)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain UserInstitutions " + userInstitutionsListOldUserInstitutions + " since its idUser field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            List<UserCircuits> attachedUserCircuitsListNew = new ArrayList<UserCircuits>();
            for (UserCircuits userCircuitsListNewUserCircuitsToAttach : userCircuitsListNew) {
                userCircuitsListNewUserCircuitsToAttach = em.getReference(userCircuitsListNewUserCircuitsToAttach.getClass(), userCircuitsListNewUserCircuitsToAttach.getUserCircuitsPK());
                attachedUserCircuitsListNew.add(userCircuitsListNewUserCircuitsToAttach);
            }
            userCircuitsListNew = attachedUserCircuitsListNew;
            users.setUserCircuitsList(userCircuitsListNew);
            List<UserVotingTables> attachedUserVotingTablesListNew = new ArrayList<UserVotingTables>();
            for (UserVotingTables userVotingTablesListNewUserVotingTablesToAttach : userVotingTablesListNew) {
                userVotingTablesListNewUserVotingTablesToAttach = em.getReference(userVotingTablesListNewUserVotingTablesToAttach.getClass(), userVotingTablesListNewUserVotingTablesToAttach.getUserVotingTablesPK());
                attachedUserVotingTablesListNew.add(userVotingTablesListNewUserVotingTablesToAttach);
            }
            userVotingTablesListNew = attachedUserVotingTablesListNew;
            users.setUserVotingTablesList(userVotingTablesListNew);
            List<UserRegions> attachedUserRegionsListNew = new ArrayList<UserRegions>();
            for (UserRegions userRegionsListNewUserRegionsToAttach : userRegionsListNew) {
                userRegionsListNewUserRegionsToAttach = em.getReference(userRegionsListNewUserRegionsToAttach.getClass(), userRegionsListNewUserRegionsToAttach.getUserRegionsPK());
                attachedUserRegionsListNew.add(userRegionsListNewUserRegionsToAttach);
            }
            userRegionsListNew = attachedUserRegionsListNew;
            users.setUserRegionsList(userRegionsListNew);
            List<UserSections> attachedUserSectionsListNew = new ArrayList<UserSections>();
            for (UserSections userSectionsListNewUserSectionsToAttach : userSectionsListNew) {
                userSectionsListNewUserSectionsToAttach = em.getReference(userSectionsListNewUserSectionsToAttach.getClass(), userSectionsListNewUserSectionsToAttach.getUserSectionsPK());
                attachedUserSectionsListNew.add(userSectionsListNewUserSectionsToAttach);
            }
            userSectionsListNew = attachedUserSectionsListNew;
            users.setUserSectionsList(userSectionsListNew);
            List<UserElections> attachedUserElectionsListNew = new ArrayList<UserElections>();
            for (UserElections userElectionsListNewUserElectionsToAttach : userElectionsListNew) {
                userElectionsListNewUserElectionsToAttach = em.getReference(userElectionsListNewUserElectionsToAttach.getClass(), userElectionsListNewUserElectionsToAttach.getUserElectionsPK());
                attachedUserElectionsListNew.add(userElectionsListNewUserElectionsToAttach);
            }
            userElectionsListNew = attachedUserElectionsListNew;
            users.setUserElectionsList(userElectionsListNew);
            List<UserInstitutions> attachedUserInstitutionsListNew = new ArrayList<UserInstitutions>();
            for (UserInstitutions userInstitutionsListNewUserInstitutionsToAttach : userInstitutionsListNew) {
                userInstitutionsListNewUserInstitutionsToAttach = em.getReference(userInstitutionsListNewUserInstitutionsToAttach.getClass(), userInstitutionsListNewUserInstitutionsToAttach.getUserInstitutionsPK());
                attachedUserInstitutionsListNew.add(userInstitutionsListNewUserInstitutionsToAttach);
            }
            userInstitutionsListNew = attachedUserInstitutionsListNew;
            users.setUserInstitutionsList(userInstitutionsListNew);
            List<Polls> attachedPollsListNew = new ArrayList<Polls>();
            for (Polls pollsListNewPollsToAttach : pollsListNew) {
                pollsListNewPollsToAttach = em.getReference(pollsListNewPollsToAttach.getClass(), pollsListNewPollsToAttach.getId());
                attachedPollsListNew.add(pollsListNewPollsToAttach);
            }
            pollsListNew = attachedPollsListNew;
            users.setPollsList(pollsListNew);
            users = em.merge(users);
            for (UserCircuits userCircuitsListNewUserCircuits : userCircuitsListNew) {
                if (!userCircuitsListOld.contains(userCircuitsListNewUserCircuits)) {
                    Users oldIdUserOfUserCircuitsListNewUserCircuits = userCircuitsListNewUserCircuits.getIdUser();
                    userCircuitsListNewUserCircuits.setIdUser(users);
                    userCircuitsListNewUserCircuits = em.merge(userCircuitsListNewUserCircuits);
                    if (oldIdUserOfUserCircuitsListNewUserCircuits != null && !oldIdUserOfUserCircuitsListNewUserCircuits.equals(users)) {
                        oldIdUserOfUserCircuitsListNewUserCircuits.getUserCircuitsList().remove(userCircuitsListNewUserCircuits);
                        oldIdUserOfUserCircuitsListNewUserCircuits = em.merge(oldIdUserOfUserCircuitsListNewUserCircuits);
                    }
                }
            }
            for (UserVotingTables userVotingTablesListNewUserVotingTables : userVotingTablesListNew) {
                if (!userVotingTablesListOld.contains(userVotingTablesListNewUserVotingTables)) {
                    Users oldIdUserOfUserVotingTablesListNewUserVotingTables = userVotingTablesListNewUserVotingTables.getIdUser();
                    userVotingTablesListNewUserVotingTables.setIdUser(users);
                    userVotingTablesListNewUserVotingTables = em.merge(userVotingTablesListNewUserVotingTables);
                    if (oldIdUserOfUserVotingTablesListNewUserVotingTables != null && !oldIdUserOfUserVotingTablesListNewUserVotingTables.equals(users)) {
                        oldIdUserOfUserVotingTablesListNewUserVotingTables.getUserVotingTablesList().remove(userVotingTablesListNewUserVotingTables);
                        oldIdUserOfUserVotingTablesListNewUserVotingTables = em.merge(oldIdUserOfUserVotingTablesListNewUserVotingTables);
                    }
                }
            }
            for (UserRegions userRegionsListNewUserRegions : userRegionsListNew) {
                if (!userRegionsListOld.contains(userRegionsListNewUserRegions)) {
                    Users oldIdUserOfUserRegionsListNewUserRegions = userRegionsListNewUserRegions.getIdUser();
                    userRegionsListNewUserRegions.setIdUser(users);
                    userRegionsListNewUserRegions = em.merge(userRegionsListNewUserRegions);
                    if (oldIdUserOfUserRegionsListNewUserRegions != null && !oldIdUserOfUserRegionsListNewUserRegions.equals(users)) {
                        oldIdUserOfUserRegionsListNewUserRegions.getUserRegionsList().remove(userRegionsListNewUserRegions);
                        oldIdUserOfUserRegionsListNewUserRegions = em.merge(oldIdUserOfUserRegionsListNewUserRegions);
                    }
                }
            }
            for (UserSections userSectionsListNewUserSections : userSectionsListNew) {
                if (!userSectionsListOld.contains(userSectionsListNewUserSections)) {
                    Users oldIdUserOfUserSectionsListNewUserSections = userSectionsListNewUserSections.getIdUser();
                    userSectionsListNewUserSections.setIdUser(users);
                    userSectionsListNewUserSections = em.merge(userSectionsListNewUserSections);
                    if (oldIdUserOfUserSectionsListNewUserSections != null && !oldIdUserOfUserSectionsListNewUserSections.equals(users)) {
                        oldIdUserOfUserSectionsListNewUserSections.getUserSectionsList().remove(userSectionsListNewUserSections);
                        oldIdUserOfUserSectionsListNewUserSections = em.merge(oldIdUserOfUserSectionsListNewUserSections);
                    }
                }
            }
            for (UserElections userElectionsListNewUserElections : userElectionsListNew) {
                if (!userElectionsListOld.contains(userElectionsListNewUserElections)) {
                    Users oldIdUserOfUserElectionsListNewUserElections = userElectionsListNewUserElections.getIdUser();
                    userElectionsListNewUserElections.setIdUser(users);
                    userElectionsListNewUserElections = em.merge(userElectionsListNewUserElections);
                    if (oldIdUserOfUserElectionsListNewUserElections != null && !oldIdUserOfUserElectionsListNewUserElections.equals(users)) {
                        oldIdUserOfUserElectionsListNewUserElections.getUserElectionsList().remove(userElectionsListNewUserElections);
                        oldIdUserOfUserElectionsListNewUserElections = em.merge(oldIdUserOfUserElectionsListNewUserElections);
                    }
                }
            }
            for (UserInstitutions userInstitutionsListNewUserInstitutions : userInstitutionsListNew) {
                if (!userInstitutionsListOld.contains(userInstitutionsListNewUserInstitutions)) {
                    Users oldIdUserOfUserInstitutionsListNewUserInstitutions = userInstitutionsListNewUserInstitutions.getIdUser();
                    userInstitutionsListNewUserInstitutions.setIdUser(users);
                    userInstitutionsListNewUserInstitutions = em.merge(userInstitutionsListNewUserInstitutions);
                    if (oldIdUserOfUserInstitutionsListNewUserInstitutions != null && !oldIdUserOfUserInstitutionsListNewUserInstitutions.equals(users)) {
                        oldIdUserOfUserInstitutionsListNewUserInstitutions.getUserInstitutionsList().remove(userInstitutionsListNewUserInstitutions);
                        oldIdUserOfUserInstitutionsListNewUserInstitutions = em.merge(oldIdUserOfUserInstitutionsListNewUserInstitutions);
                    }
                }
            }
            for (Polls pollsListOldPolls : pollsListOld) {
                if (!pollsListNew.contains(pollsListOldPolls)) {
                    pollsListOldPolls.setIdUser(null);
                    pollsListOldPolls = em.merge(pollsListOldPolls);
                }
            }
            for (Polls pollsListNewPolls : pollsListNew) {
                if (!pollsListOld.contains(pollsListNewPolls)) {
                    Users oldIdUserOfPollsListNewPolls = pollsListNewPolls.getIdUser();
                    pollsListNewPolls.setIdUser(users);
                    pollsListNewPolls = em.merge(pollsListNewPolls);
                    if (oldIdUserOfPollsListNewPolls != null && !oldIdUserOfPollsListNewPolls.equals(users)) {
                        oldIdUserOfPollsListNewPolls.getPollsList().remove(pollsListNewPolls);
                        oldIdUserOfPollsListNewPolls = em.merge(oldIdUserOfPollsListNewPolls);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = users.getId();
                if (findUsers(id) == null) {
                    throw new NonexistentEntityException("The users with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Users users;
            try {
                users = em.getReference(Users.class, id);
                users.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The users with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            List<UserCircuits> userCircuitsListOrphanCheck = users.getUserCircuitsList();
            for (UserCircuits userCircuitsListOrphanCheckUserCircuits : userCircuitsListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Users (" + users + ") cannot be destroyed since the UserCircuits " + userCircuitsListOrphanCheckUserCircuits + " in its userCircuitsList field has a non-nullable idUser field.");
            }
            List<UserVotingTables> userVotingTablesListOrphanCheck = users.getUserVotingTablesList();
            for (UserVotingTables userVotingTablesListOrphanCheckUserVotingTables : userVotingTablesListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Users (" + users + ") cannot be destroyed since the UserVotingTables " + userVotingTablesListOrphanCheckUserVotingTables + " in its userVotingTablesList field has a non-nullable idUser field.");
            }
            List<UserRegions> userRegionsListOrphanCheck = users.getUserRegionsList();
            for (UserRegions userRegionsListOrphanCheckUserRegions : userRegionsListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Users (" + users + ") cannot be destroyed since the UserRegions " + userRegionsListOrphanCheckUserRegions + " in its userRegionsList field has a non-nullable idUser field.");
            }
            List<UserSections> userSectionsListOrphanCheck = users.getUserSectionsList();
            for (UserSections userSectionsListOrphanCheckUserSections : userSectionsListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Users (" + users + ") cannot be destroyed since the UserSections " + userSectionsListOrphanCheckUserSections + " in its userSectionsList field has a non-nullable idUser field.");
            }
            List<UserElections> userElectionsListOrphanCheck = users.getUserElectionsList();
            for (UserElections userElectionsListOrphanCheckUserElections : userElectionsListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Users (" + users + ") cannot be destroyed since the UserElections " + userElectionsListOrphanCheckUserElections + " in its userElectionsList field has a non-nullable idUser field.");
            }
            List<UserInstitutions> userInstitutionsListOrphanCheck = users.getUserInstitutionsList();
            for (UserInstitutions userInstitutionsListOrphanCheckUserInstitutions : userInstitutionsListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Users (" + users + ") cannot be destroyed since the UserInstitutions " + userInstitutionsListOrphanCheckUserInstitutions + " in its userInstitutionsList field has a non-nullable idUser field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            List<Polls> pollsList = users.getPollsList();
            for (Polls pollsListPolls : pollsList) {
                pollsListPolls.setIdUser(null);
                pollsListPolls = em.merge(pollsListPolls);
            }
            em.remove(users);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Users> findUsersEntities() {
        return findUsersEntities(true, -1, -1);
    }

    public List<Users> findUsersEntities(int maxResults, int firstResult) {
        return findUsersEntities(false, maxResults, firstResult);
    }

    private List<Users> findUsersEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Users.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Users findUsers(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Users.class, id);
        } finally {
            em.close();
        }
    }

    public int getUsersCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Users> rt = cq.from(Users.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
