/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ar.com.singleton.fiscalizar.libs.jpa.controllers.entities.relations;

import ar.com.singleton.fiscalizar.libs.jpa.controllers.exceptions.NonexistentEntityException;
import ar.com.singleton.fiscalizar.libs.jpa.controllers.exceptions.PreexistingEntityException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import ar.com.singleton.fiscalizar.libs.jpa.entities.Users;
import ar.com.singleton.fiscalizar.libs.jpa.entities.Sections;
import ar.com.singleton.fiscalizar.libs.jpa.entities.keys.UserSectionsPK;
import ar.com.singleton.fiscalizar.libs.jpa.entities.relations.UserSections;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author pato
 */
public class UserSectionsJpaController implements Serializable {

    public UserSectionsJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(UserSections userSections) throws PreexistingEntityException, Exception {
        if (userSections.getUserSectionsPK() == null) {
            userSections.setUserSectionsPK(new UserSectionsPK());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Users idUser = userSections.getIdUser();
            if (idUser != null) {
                idUser = em.getReference(idUser.getClass(), idUser.getId());
                userSections.setIdUser(idUser);
            }
            Sections idSection = userSections.getIdSection();
            if (idSection != null) {
                idSection = em.getReference(idSection.getClass(), idSection.getId());
                userSections.setIdSection(idSection);
            }
            em.persist(userSections);
            if (idUser != null) {
                idUser.getUserSectionsList().add(userSections);
                idUser = em.merge(idUser);
            }
            if (idSection != null) {
                idSection.getUserSectionsList().add(userSections);
                idSection = em.merge(idSection);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findUserSections(userSections.getUserSectionsPK()) != null) {
                throw new PreexistingEntityException("UserSections " + userSections + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(UserSections userSections) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            UserSections persistentUserSections = em.find(UserSections.class, userSections.getUserSectionsPK());
            Users idUserOld = persistentUserSections.getIdUser();
            Users idUserNew = userSections.getIdUser();
            Sections idSectionOld = persistentUserSections.getIdSection();
            Sections idSectionNew = userSections.getIdSection();
            if (idUserNew != null) {
                idUserNew = em.getReference(idUserNew.getClass(), idUserNew.getId());
                userSections.setIdUser(idUserNew);
            }
            if (idSectionNew != null) {
                idSectionNew = em.getReference(idSectionNew.getClass(), idSectionNew.getId());
                userSections.setIdSection(idSectionNew);
            }
            userSections = em.merge(userSections);
            if (idUserOld != null && !idUserOld.equals(idUserNew)) {
                idUserOld.getUserSectionsList().remove(userSections);
                idUserOld = em.merge(idUserOld);
            }
            if (idUserNew != null && !idUserNew.equals(idUserOld)) {
                idUserNew.getUserSectionsList().add(userSections);
                idUserNew = em.merge(idUserNew);
            }
            if (idSectionOld != null && !idSectionOld.equals(idSectionNew)) {
                idSectionOld.getUserSectionsList().remove(userSections);
                idSectionOld = em.merge(idSectionOld);
            }
            if (idSectionNew != null && !idSectionNew.equals(idSectionOld)) {
                idSectionNew.getUserSectionsList().add(userSections);
                idSectionNew = em.merge(idSectionNew);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                UserSectionsPK id = userSections.getUserSectionsPK();
                if (findUserSections(id) == null) {
                    throw new NonexistentEntityException("The userSections with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(UserSectionsPK id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            UserSections userSections;
            try {
                userSections = em.getReference(UserSections.class, id);
                userSections.getUserSectionsPK();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The userSections with id " + id + " no longer exists.", enfe);
            }
            Users idUser = userSections.getIdUser();
            if (idUser != null) {
                idUser.getUserSectionsList().remove(userSections);
                idUser = em.merge(idUser);
            }
            Sections idSection = userSections.getIdSection();
            if (idSection != null) {
                idSection.getUserSectionsList().remove(userSections);
                idSection = em.merge(idSection);
            }
            em.remove(userSections);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<UserSections> findUserSectionsEntities() {
        return findUserSectionsEntities(true, -1, -1);
    }

    public List<UserSections> findUserSectionsEntities(int maxResults, int firstResult) {
        return findUserSectionsEntities(false, maxResults, firstResult);
    }

    private List<UserSections> findUserSectionsEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(UserSections.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public UserSections findUserSections(UserSectionsPK id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(UserSections.class, id);
        } finally {
            em.close();
        }
    }

    public int getUserSectionsCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<UserSections> rt = cq.from(UserSections.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
