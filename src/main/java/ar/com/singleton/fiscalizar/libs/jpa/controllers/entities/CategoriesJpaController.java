/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ar.com.singleton.fiscalizar.libs.jpa.controllers.entities;

import ar.com.singleton.fiscalizar.libs.jpa.controllers.exceptions.IllegalOrphanException;
import ar.com.singleton.fiscalizar.libs.jpa.controllers.exceptions.NonexistentEntityException;
import ar.com.singleton.fiscalizar.libs.jpa.controllers.exceptions.PreexistingEntityException;
import ar.com.singleton.fiscalizar.libs.jpa.entities.Categories;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import ar.com.singleton.fiscalizar.libs.jpa.entities.relations.ElectionsCategories;
import java.util.ArrayList;
import java.util.List;
import ar.com.singleton.fiscalizar.libs.jpa.entities.Polls;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author pato
 */
public class CategoriesJpaController implements Serializable {

    public CategoriesJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Categories categories) throws PreexistingEntityException, Exception {
        if (categories.getElectionsCategoriesList() == null) {
            categories.setElectionsCategoriesList(new ArrayList<ElectionsCategories>());
        }
        if (categories.getPollsList() == null) {
            categories.setPollsList(new ArrayList<Polls>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            List<ElectionsCategories> attachedElectionsCategoriesList = new ArrayList<ElectionsCategories>();
            for (ElectionsCategories electionsCategoriesListElectionsCategoriesToAttach : categories.getElectionsCategoriesList()) {
                electionsCategoriesListElectionsCategoriesToAttach = em.getReference(electionsCategoriesListElectionsCategoriesToAttach.getClass(), electionsCategoriesListElectionsCategoriesToAttach.getElectionsCategoriesPK());
                attachedElectionsCategoriesList.add(electionsCategoriesListElectionsCategoriesToAttach);
            }
            categories.setElectionsCategoriesList(attachedElectionsCategoriesList);
            List<Polls> attachedPollsList = new ArrayList<Polls>();
            for (Polls pollsListPollsToAttach : categories.getPollsList()) {
                pollsListPollsToAttach = em.getReference(pollsListPollsToAttach.getClass(), pollsListPollsToAttach.getId());
                attachedPollsList.add(pollsListPollsToAttach);
            }
            categories.setPollsList(attachedPollsList);
            em.persist(categories);
            for (ElectionsCategories electionsCategoriesListElectionsCategories : categories.getElectionsCategoriesList()) {
                Categories oldIdCategoryOfElectionsCategoriesListElectionsCategories = electionsCategoriesListElectionsCategories.getIdCategory();
                electionsCategoriesListElectionsCategories.setIdCategory(categories);
                electionsCategoriesListElectionsCategories = em.merge(electionsCategoriesListElectionsCategories);
                if (oldIdCategoryOfElectionsCategoriesListElectionsCategories != null) {
                    oldIdCategoryOfElectionsCategoriesListElectionsCategories.getElectionsCategoriesList().remove(electionsCategoriesListElectionsCategories);
                    oldIdCategoryOfElectionsCategoriesListElectionsCategories = em.merge(oldIdCategoryOfElectionsCategoriesListElectionsCategories);
                }
            }
            for (Polls pollsListPolls : categories.getPollsList()) {
                Categories oldIdCategoryOfPollsListPolls = pollsListPolls.getIdCategory();
                pollsListPolls.setIdCategory(categories);
                pollsListPolls = em.merge(pollsListPolls);
                if (oldIdCategoryOfPollsListPolls != null) {
                    oldIdCategoryOfPollsListPolls.getPollsList().remove(pollsListPolls);
                    oldIdCategoryOfPollsListPolls = em.merge(oldIdCategoryOfPollsListPolls);
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findCategories(categories.getId()) != null) {
                throw new PreexistingEntityException("Categories " + categories + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Categories categories) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Categories persistentCategories = em.find(Categories.class, categories.getId());
            List<ElectionsCategories> electionsCategoriesListOld = persistentCategories.getElectionsCategoriesList();
            List<ElectionsCategories> electionsCategoriesListNew = categories.getElectionsCategoriesList();
            List<Polls> pollsListOld = persistentCategories.getPollsList();
            List<Polls> pollsListNew = categories.getPollsList();
            List<String> illegalOrphanMessages = null;
            for (ElectionsCategories electionsCategoriesListOldElectionsCategories : electionsCategoriesListOld) {
                if (!electionsCategoriesListNew.contains(electionsCategoriesListOldElectionsCategories)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain ElectionsCategories " + electionsCategoriesListOldElectionsCategories + " since its idCategory field is not nullable.");
                }
            }
            for (Polls pollsListOldPolls : pollsListOld) {
                if (!pollsListNew.contains(pollsListOldPolls)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Polls " + pollsListOldPolls + " since its idCategory field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            List<ElectionsCategories> attachedElectionsCategoriesListNew = new ArrayList<ElectionsCategories>();
            for (ElectionsCategories electionsCategoriesListNewElectionsCategoriesToAttach : electionsCategoriesListNew) {
                electionsCategoriesListNewElectionsCategoriesToAttach = em.getReference(electionsCategoriesListNewElectionsCategoriesToAttach.getClass(), electionsCategoriesListNewElectionsCategoriesToAttach.getElectionsCategoriesPK());
                attachedElectionsCategoriesListNew.add(electionsCategoriesListNewElectionsCategoriesToAttach);
            }
            electionsCategoriesListNew = attachedElectionsCategoriesListNew;
            categories.setElectionsCategoriesList(electionsCategoriesListNew);
            List<Polls> attachedPollsListNew = new ArrayList<Polls>();
            for (Polls pollsListNewPollsToAttach : pollsListNew) {
                pollsListNewPollsToAttach = em.getReference(pollsListNewPollsToAttach.getClass(), pollsListNewPollsToAttach.getId());
                attachedPollsListNew.add(pollsListNewPollsToAttach);
            }
            pollsListNew = attachedPollsListNew;
            categories.setPollsList(pollsListNew);
            categories = em.merge(categories);
            for (ElectionsCategories electionsCategoriesListNewElectionsCategories : electionsCategoriesListNew) {
                if (!electionsCategoriesListOld.contains(electionsCategoriesListNewElectionsCategories)) {
                    Categories oldIdCategoryOfElectionsCategoriesListNewElectionsCategories = electionsCategoriesListNewElectionsCategories.getIdCategory();
                    electionsCategoriesListNewElectionsCategories.setIdCategory(categories);
                    electionsCategoriesListNewElectionsCategories = em.merge(electionsCategoriesListNewElectionsCategories);
                    if (oldIdCategoryOfElectionsCategoriesListNewElectionsCategories != null && !oldIdCategoryOfElectionsCategoriesListNewElectionsCategories.equals(categories)) {
                        oldIdCategoryOfElectionsCategoriesListNewElectionsCategories.getElectionsCategoriesList().remove(electionsCategoriesListNewElectionsCategories);
                        oldIdCategoryOfElectionsCategoriesListNewElectionsCategories = em.merge(oldIdCategoryOfElectionsCategoriesListNewElectionsCategories);
                    }
                }
            }
            for (Polls pollsListNewPolls : pollsListNew) {
                if (!pollsListOld.contains(pollsListNewPolls)) {
                    Categories oldIdCategoryOfPollsListNewPolls = pollsListNewPolls.getIdCategory();
                    pollsListNewPolls.setIdCategory(categories);
                    pollsListNewPolls = em.merge(pollsListNewPolls);
                    if (oldIdCategoryOfPollsListNewPolls != null && !oldIdCategoryOfPollsListNewPolls.equals(categories)) {
                        oldIdCategoryOfPollsListNewPolls.getPollsList().remove(pollsListNewPolls);
                        oldIdCategoryOfPollsListNewPolls = em.merge(oldIdCategoryOfPollsListNewPolls);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = categories.getId();
                if (findCategories(id) == null) {
                    throw new NonexistentEntityException("The categories with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Categories categories;
            try {
                categories = em.getReference(Categories.class, id);
                categories.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The categories with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            List<ElectionsCategories> electionsCategoriesListOrphanCheck = categories.getElectionsCategoriesList();
            for (ElectionsCategories electionsCategoriesListOrphanCheckElectionsCategories : electionsCategoriesListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Categories (" + categories + ") cannot be destroyed since the ElectionsCategories " + electionsCategoriesListOrphanCheckElectionsCategories + " in its electionsCategoriesList field has a non-nullable idCategory field.");
            }
            List<Polls> pollsListOrphanCheck = categories.getPollsList();
            for (Polls pollsListOrphanCheckPolls : pollsListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Categories (" + categories + ") cannot be destroyed since the Polls " + pollsListOrphanCheckPolls + " in its pollsList field has a non-nullable idCategory field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            em.remove(categories);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Categories> findCategoriesEntities() {
        return findCategoriesEntities(true, -1, -1);
    }

    public List<Categories> findCategoriesEntities(int maxResults, int firstResult) {
        return findCategoriesEntities(false, maxResults, firstResult);
    }

    private List<Categories> findCategoriesEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Categories.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Categories findCategories(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Categories.class, id);
        } finally {
            em.close();
        }
    }

    public int getCategoriesCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Categories> rt = cq.from(Categories.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
