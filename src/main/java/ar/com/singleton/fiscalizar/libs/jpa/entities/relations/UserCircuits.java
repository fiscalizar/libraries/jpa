/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ar.com.singleton.fiscalizar.libs.jpa.entities.relations;

import ar.com.singleton.fiscalizar.libs.jpa.entities.Circuits;
import ar.com.singleton.fiscalizar.libs.jpa.entities.Users;
import ar.com.singleton.fiscalizar.libs.jpa.entities.keys.UserCircuitsPK;
import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author pato
 */
@Entity
@Table(name = "UserCircuits", catalog = "", schema = "")
@XmlRootElement
public class UserCircuits implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected UserCircuitsPK userCircuitsPK;
    @Basic(optional = false)
    @Column(name = "createdAt")
    private String createdAt;
    @Column(name = "updatedAt")
    private String updatedAt;
    @Basic(optional = false)
    @Column(name = "deleted")
    private int deleted;
    @PrimaryKeyJoinColumn(name = "idCircuit", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Circuits idCircuit;
    @PrimaryKeyJoinColumn(name = "idUser", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Users idUser;

    public UserCircuits() {
    }

    public UserCircuits(UserCircuitsPK userCircuitsPK) {
        this.userCircuitsPK = userCircuitsPK;
    }

    public UserCircuits(UserCircuitsPK userCircuitsPK, String createdAt, int deleted) {
        this.userCircuitsPK = userCircuitsPK;
        this.createdAt = createdAt;
        this.deleted = deleted;
    }

    public UserCircuitsPK getUserCircuitsPK() {
        return userCircuitsPK;
    }

    public void setUserCircuitsPK(UserCircuitsPK userCircuitsPK) {
        this.userCircuitsPK = userCircuitsPK;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public int getDeleted() {
        return deleted;
    }

    public void setDeleted(int deleted) {
        this.deleted = deleted;
    }

    public Circuits getIdCircuit() {
        return idCircuit;
    }

    public void setIdCircuit(Circuits idCircuit) {
        this.idCircuit = idCircuit;
    }

    public Users getIdUser() {
        return idUser;
    }

    public void setIdUser(Users idUser) {
        this.idUser = idUser;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (userCircuitsPK != null ? userCircuitsPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UserCircuits)) {
            return false;
        }
        UserCircuits other = (UserCircuits) object;
        if ((this.userCircuitsPK == null && other.userCircuitsPK != null) || (this.userCircuitsPK != null && !this.userCircuitsPK.equals(other.userCircuitsPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ar.com.singleton.app.fiscalizar.database.UserCircuits[ userCircuitsPK=" + userCircuitsPK + " ]";
    }
    
}
